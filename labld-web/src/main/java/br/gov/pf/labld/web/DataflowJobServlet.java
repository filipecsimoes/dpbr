package br.gov.pf.labld.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.gov.pf.labld.dataflow.DataflowService;
import br.gov.pf.labld.dataflow.DataflowServiceFactory;

@WebServlet(name = "DataflowJobServlet", value = "/df/view")
public class DataflowJobServlet extends HttpServlet {

	private static final long serialVersionUID = -389568605573793538L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String jobId = req.getParameter("job");
		String projectId = req.getParameter("project");

		DataflowService service = DataflowServiceFactory.getInstance();
		service.getDataflow().projects().jobs().get(projectId, jobId).executeAndDownloadTo(resp.getOutputStream());
	}
}

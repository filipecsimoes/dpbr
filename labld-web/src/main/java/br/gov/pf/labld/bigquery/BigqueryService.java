package br.gov.pf.labld.bigquery;

import java.util.Map;

import com.google.cloud.bigquery.BigQuery;
import com.google.cloud.bigquery.QueryParameterValue;
import com.google.cloud.bigquery.QueryResult;

public interface BigqueryService {

	BigQuery getBigQuery();

	QueryResult query(String query);

	QueryResult legacyQuery(String query);

	QueryResult query(String query, Map<String, QueryParameterValue> params);

	QueryResult legacyQuery(String query, Map<String, QueryParameterValue> params);
}

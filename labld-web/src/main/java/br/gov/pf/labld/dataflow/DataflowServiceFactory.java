package br.gov.pf.labld.dataflow;

public class DataflowServiceFactory {

	private static DataflowService instance;

	public static DataflowService getInstance() {
		if (instance == null) {
			instance = new DataflowServiceImpl();
		}
		return instance;
	}

}

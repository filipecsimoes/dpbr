package br.gov.pf.labld.gcs;

import com.google.appengine.tools.cloudstorage.GcsFilename;

public interface ListListener {

	void found(GcsFilename filename);

}

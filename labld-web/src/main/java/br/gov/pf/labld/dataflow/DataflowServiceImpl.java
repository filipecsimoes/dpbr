package br.gov.pf.labld.dataflow;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.google.api.client.extensions.appengine.http.UrlFetchTransport;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.extensions.appengine.auth.oauth2.AppIdentityCredential;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.dataflow.Dataflow;
import com.google.api.services.dataflow.Dataflow.Projects;
import com.google.api.services.dataflow.Dataflow.Projects.Jobs.Get;
import com.google.api.services.dataflow.Dataflow.Projects.Templates;
import com.google.api.services.dataflow.Dataflow.Projects.Templates.Create;
import com.google.api.services.dataflow.DataflowScopes;
import com.google.api.services.dataflow.model.CreateJobFromTemplateRequest;
import com.google.api.services.dataflow.model.Job;
import com.google.appengine.api.utils.SystemProperty;

public class DataflowServiceImpl implements DataflowService {

	private static final Logger LOGGER = Logger.getLogger(DataflowServiceImpl.class.getSimpleName());

	private static final JsonFactory JSON_FACTORY = new JacksonFactory();

	@Override
	public Dataflow getDataflow() {
		String applicationName = System.getProperty("application.name", "arctic-cursor-171520");
		HttpTransport transport = getTransport();
		HttpRequestInitializer httpRequestInitializer = getRequestInitializer(transport);
		Dataflow.Builder builder = new Dataflow.Builder(transport, JSON_FACTORY, httpRequestInitializer).setApplicationName(applicationName);
		return builder.build();
	}

	@Override
	public Job executeTemplate(String projectId, CreateJobFromTemplateRequest content) throws IOException {
		Dataflow dataflow = getDataflow();
		Projects projects = dataflow.projects();
		Templates templates = projects.templates();
		Create create = templates.create(projectId, content);
		return create.execute();
	}

	private HttpRequestInitializer getRequestInitializer(HttpTransport transport) {
		if (isProduction()) {
			return new AppIdentityCredential(DataflowScopes.all());
		} else {
			System.out.println(System.getProperties().entrySet());
			String serviceAccountId = System.getProperty("service.account.id");
			String serviceAccountP12 = System.getProperty("service.account.p12");
			LOGGER.info("service.account.p12 = " + serviceAccountP12);
			LOGGER.info("service.account.id = " + serviceAccountId);
			File serviceAccountP12File = new File(System.getProperty("user.home") + "/.gc-keys", serviceAccountP12);
			GoogleCredential credential;
			try {
				GoogleCredential.Builder builder = new GoogleCredential.Builder();
				builder.setTransport(transport).setJsonFactory(JSON_FACTORY).setServiceAccountId(serviceAccountId).setServiceAccountScopes(DataflowScopes.all())
						.setServiceAccountPrivateKeyFromP12File(serviceAccountP12File);
				credential = builder.build();
			} catch (GeneralSecurityException | IOException e) {
				throw new RuntimeException(e);
			}
			return credential;
		}
	}

	private HttpTransport getTransport() {
		if (isProduction()) {
			return new UrlFetchTransport();
		} else {
			return new NetHttpTransport();
		}
	}

	private boolean isProduction() {
		return SystemProperty.environment.value() == SystemProperty.Environment.Value.Production;
	}

	@Override
	public void monitor(String projectId, String jobId, Map<String, String> params, List<DataflowJobListener> listeners) throws IOException {
		Dataflow dataflow = getDataflow();
		Projects projects = dataflow.projects();
		Get get = projects.jobs().get(projectId, jobId);
		Job job = get.execute();
		for (DataflowJobListener listener : listeners) {
			listener.listen(job, params);
		}
	}

}

package br.gov.pf.labld.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.gov.pf.labld.dataflow.DataflowService;
import br.gov.pf.labld.dataflow.DataflowServiceFactory;

import com.google.api.services.dataflow.model.CreateJobFromTemplateRequest;
import com.google.api.services.dataflow.model.Job;
import com.google.api.services.dataflow.model.RuntimeEnvironment;

@WebServlet(name = "DataflowRunJobServlet", value = "/df/run")
public class DataflowRunJobServlet extends HttpServlet {

	private static final long serialVersionUID = 7469284995788666540L;

	private static final String DF_PARAM_PREFIX = "df-";

	@SuppressWarnings("unchecked")
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String project = request.getParameter("project");
		String jobName = request.getParameter("job");
		String bucket = request.getParameter("bucket");

		String template = request.getParameter("template");
		String gcsPath = "gs://" + bucket + "/templates/" + template;

		String zone = request.getParameter("zone");
		String location = request.getParameter("location");

		Map<String, String[]> parameterMap = request.getParameterMap();

		DataflowService dataflowService = DataflowServiceFactory.getInstance();
		CreateJobFromTemplateRequest content = new CreateJobFromTemplateRequest();
		Map<String, String> params = buildDataflowParams(parameterMap);
		content.setParameters(params);
		content.setGcsPath(gcsPath);

		jobName += "-" + UUID.randomUUID();
		content.setJobName(jobName);
		if (zone != null) {
			RuntimeEnvironment environment = new RuntimeEnvironment();
			environment.setZone(zone);
			content.setEnvironment(environment);
		}

		if (location != null) {
			content.setLocation(location);
		}

		Job job = dataflowService.executeTemplate(project, content);

		params.put("bucket", bucket);
		params.put("template", template);
		params.put("jobName", jobName);
		

		
		WebDataflowJobListeners.startMonitoring(job, params);

		job.put("X-job-params", params);
		
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		response.getWriter().write(job.toPrettyString());
	}

	private Map<String, String> buildDataflowParams(Map<String, String[]> parameterMap) {
		Map<String, String> params = new HashMap<String, String>();
		for (Entry<String, String[]> param : parameterMap.entrySet()) {
			String[] array = param.getValue();
			String key = param.getKey();
			boolean dfParam = key.startsWith(DF_PARAM_PREFIX);
			if (array.length > 0 && dfParam) {
				key = key.replaceFirst(DF_PARAM_PREFIX, "");
				String value = array[0];
				params.put(key, value);
			}
		}
		return params;
	}

}

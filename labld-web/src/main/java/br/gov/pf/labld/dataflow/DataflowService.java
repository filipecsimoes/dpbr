package br.gov.pf.labld.dataflow;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.google.api.services.dataflow.Dataflow;
import com.google.api.services.dataflow.model.CreateJobFromTemplateRequest;
import com.google.api.services.dataflow.model.Job;

public interface DataflowService {

	Dataflow getDataflow();

	Job executeTemplate(String projectId, CreateJobFromTemplateRequest content) throws IOException;

	void monitor(String projectId, String jobId, Map<String, String> params, List<DataflowJobListener> listeners) throws IOException;

}

package br.gov.pf.labld.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import br.gov.pf.labld.dataflow.DataflowJobListener;
import br.gov.pf.labld.gcs.GCStorageService;
import br.gov.pf.labld.gcs.GCStorageServiceFactory;

import com.google.api.services.dataflow.model.Job;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import com.google.appengine.tools.cloudstorage.ListOptions;

public class WebDataflowJobListeners {

	private static final int COUNTDOWN = 5 * 60 * 1000;

	private static final List<String> TERMINAL_STATES = Arrays.asList("JOB_STATE_CANCELLED", "JOB_STATE_DONE", "JOB_STATE_FAILED");
	private static List<DataflowJobListener> listeners = new ArrayList<>();

	static {
		listeners.add(new DeleteTempFilesListener());
		listeners.add(new RescheduleListerner());
	}

	public static List<DataflowJobListener> getListeners() {
		return listeners;
	}

	public static void startMonitoring(Job job, Map<String, String> params) {
		Queue monitorQueue = QueueFactory.getQueue("queue-df-monitor");
		TaskOptions taskOptions = TaskOptions.Builder.withUrl("/df/monitor").countdownMillis(COUNTDOWN).method(Method.POST);
		taskOptions.param("project", job.getProjectId()).param("job", job.getId());
		for (Entry<String, String> param : params.entrySet()) {
			taskOptions.param(param.getKey(), param.getValue());
		}
		monitorQueue.add(taskOptions);
	}

	private static class RescheduleListerner implements DataflowJobListener {

		@Override
		public void listen(Job job, Map<String, String> params) {
			String currentState = job.getCurrentState();
			if (!TERMINAL_STATES.contains(currentState)) {
				startMonitoring(job, params);
			}
		}

	}

	private static class DeleteTempFilesListener implements DataflowJobListener {

		@Override
		public void listen(Job job, Map<String, String> params) {
			String currentState = job.getCurrentState();
			if (TERMINAL_STATES.contains(currentState)) {
				String folder = params.get("folder");
				String bucket = params.get("bucket");
				if (folder != null && bucket != null) {
					ListOptions opts = new ListOptions.Builder().setRecursive(true).setPrefix(folder).build();
					GCStorageService service = GCStorageServiceFactory.getInstance();
					try {
						service.deleteTemp(bucket, opts);
					} catch (IOException e) {
						throw new RuntimeException(e);
					}
				}
			}
		}

	}

}

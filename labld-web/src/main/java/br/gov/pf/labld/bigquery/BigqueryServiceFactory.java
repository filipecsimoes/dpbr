package br.gov.pf.labld.bigquery;

import java.net.Authenticator;
import java.net.PasswordAuthentication;

public class BigqueryServiceFactory {

	private static BigqueryService instance;

	public static BigqueryService getInstance() {
		if (instance == null) {
			instance = createInstance();
		}
		return instance;
	}

	private static BigqueryService createInstance() {
		if (System.getProperty("http.proxyHost") != null || System.getProperty("https.proxyHost") != null || System.getProperty("socks.proxyHost") != null) {

			// Java ignores http.proxyUser. Here come's the workaround.
			Authenticator.setDefault(new Authenticator() {
				@Override
				protected PasswordAuthentication getPasswordAuthentication() {
					if (getRequestorType() == RequestorType.PROXY) {
						String prot = getRequestingProtocol().toLowerCase();
						String host = System.getProperty(prot + ".proxyHost", "");
						String port = System.getProperty(prot + ".proxyPort", "3128");
						String user = System.getProperty("proxyUser", "");
						String password = System.getProperty("proxyPwd", "");

						if (getRequestingHost().equalsIgnoreCase(host)) {
							if (Integer.parseInt(port) == getRequestingPort()) {
								return new PasswordAuthentication(user, password.toCharArray());
							}
						}
					}
					return null;
				}
			});
		}
		return new BigqueryServiceImpl();
	}

}

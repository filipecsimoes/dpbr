package br.gov.pf.labld.gcs;

import java.io.IOException;
import java.util.regex.Pattern;

import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.ListItem;
import com.google.appengine.tools.cloudstorage.ListOptions;
import com.google.appengine.tools.cloudstorage.ListResult;
import com.google.appengine.tools.cloudstorage.RetryParams;

public class GCStorageServiceImpl implements GCStorageService {

	private final GcsService gcsService = GcsServiceFactory.createGcsService(RetryParams.getDefaultInstance());

	@Override
	public GcsService getGCS() {
		return gcsService;
	}

	@Override
	public boolean delete(GcsFilename file) throws IOException {
		return getGCS().delete(file);
	}

	@Override
	public void deleteTemp(String bucket, ListOptions options) throws IOException {
		list(bucket, options, ".+\\.tmp$", new DeleteListener());
	}

	@Override
	public void list(String bucket, ListOptions options, String regex, ListListener listener) throws IOException {
		ListResult result = getGCS().list(bucket, options);
		Pattern patern = Pattern.compile(regex);
		while (result.hasNext()) {
			ListItem item = result.next();
			String name = item.getName();
			if (patern.matcher(name).matches()) {
				listener.found(new GcsFilename(bucket, name));
			}
		}
	}

	private class DeleteListener implements ListListener {

		@Override
		public void found(GcsFilename filename) {
			try {
				delete(filename);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}

	}

}

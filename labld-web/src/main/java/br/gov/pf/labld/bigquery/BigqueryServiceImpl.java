package br.gov.pf.labld.bigquery;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.bigquery.BigQuery;
import com.google.cloud.bigquery.BigQueryOptions;
import com.google.cloud.bigquery.QueryParameterValue;
import com.google.cloud.bigquery.QueryRequest;
import com.google.cloud.bigquery.QueryResponse;
import com.google.cloud.bigquery.QueryResult;

public class BigqueryServiceImpl implements BigqueryService {

	@Override
	public BigQuery getBigQuery() {
		BigQueryOptions queryOptions;
		String gcKey = System.getProperty("gc-key");
		if (gcKey != null) {
			try (InputStream in = this.getClass().getClassLoader().getResourceAsStream(gcKey)) {
				String projectId = System.getProperty("projectId");
				queryOptions = BigQueryOptions.newBuilder().setProjectId(projectId).setCredentials(GoogleCredentials.fromStream(in)).build();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		} else {
			queryOptions = BigQueryOptions.getDefaultInstance();
		}
		BigQuery bigQuery = queryOptions.getService();
		return bigQuery;
	}

	public QueryResult query(String query) {
		return query(query, false, null);
	}

	public QueryResult legacyQuery(String query) {
		return query(query, true, null);
	}

	public QueryResult query(String query, Map<String, QueryParameterValue> params) {
		return query(query, false, params);
	}

	public QueryResult legacyQuery(String query, Map<String, QueryParameterValue> params) {
		return query(query, true, params);
	}

	private QueryResult query(String query, boolean legacySql, Map<String, QueryParameterValue> params) {
		BigQuery bigQuery = getBigQuery();
		QueryRequest.Builder queryBuilder = QueryRequest.newBuilder(query);
		queryBuilder.setUseLegacySql(legacySql);
		if (params != null) {
			queryBuilder.setNamedParameters(params);
		}
		QueryRequest queryRequest = queryBuilder.build();
		QueryResponse response = bigQuery.query(queryRequest);
		return response.getResult();
	}
}

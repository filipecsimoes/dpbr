package br.gov.pf.labld.gcs;

import java.io.IOException;

import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.ListOptions;

public interface GCStorageService {

	GcsService getGCS();

	boolean delete(GcsFilename file) throws IOException;

	void deleteTemp(String bucket, ListOptions options) throws IOException;

	void list(String bucket, ListOptions options, String regex, ListListener listener) throws IOException;
}

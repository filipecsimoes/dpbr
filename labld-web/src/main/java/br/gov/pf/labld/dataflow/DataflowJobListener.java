package br.gov.pf.labld.dataflow;

import java.util.Map;

import com.google.api.services.dataflow.model.Job;

public interface DataflowJobListener {

	void listen(Job job, Map<String, String> params);

}

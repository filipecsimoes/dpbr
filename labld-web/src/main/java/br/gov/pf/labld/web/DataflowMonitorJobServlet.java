package br.gov.pf.labld.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.gov.pf.labld.dataflow.DataflowService;
import br.gov.pf.labld.dataflow.DataflowServiceFactory;

@WebServlet(name = "DataflowMonitorJobServlet", value = "/df/monitor")
public class DataflowMonitorJobServlet extends HttpServlet {

	private static final long serialVersionUID = -389568605573793538L;

	@SuppressWarnings("unchecked")
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String jobId = req.getParameter("job");
		String projectId = req.getParameter("project");
		Map<String, String[]> parameterMap = req.getParameterMap();
		Map<String, String> params = buildParams(parameterMap);
		DataflowService service = DataflowServiceFactory.getInstance();
		service.monitor(projectId, jobId, params, WebDataflowJobListeners.getListeners());
	}

	private Map<String, String> buildParams(Map<String, String[]> parameterMap) {
		Map<String, String> params = new HashMap<String, String>();
		for (Entry<String, String[]> param : parameterMap.entrySet()) {
			String[] array = param.getValue();
			String key = param.getKey();
			if (array.length > 0) {
				String value = array[0];
				params.put(key, value);
			}
		}
		return params;
	}

}

package br.gov.pf.labld.dataflow.fn;

import java.io.BufferedInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.cloud.dataflow.sdk.options.ValueProvider;
import com.google.cloud.dataflow.sdk.transforms.DoFn;

public class ZipExtractDoFn extends DoFn<String, String> {

	private static final Logger LOGGER = LoggerFactory.getLogger(ZipExtractDoFn.class);

	private static final long serialVersionUID = -959529779120314409L;

	private FileHandler handler;
	private Pattern pattern = Pattern.compile(".");

	private String folder;
	private ValueProvider<String> folderProvider;

	public ZipExtractDoFn(ValueProvider<String> folder, FileHandler handler) {
		super();
		this.handler = handler;
		this.folderProvider = folder;
	}

	public ZipExtractDoFn(ValueProvider<String> folder, FileHandler handler, String pattern) {
		this(folder, handler);
		this.pattern = Pattern.compile(pattern);
	}

	public ZipExtractDoFn(String folder, FileHandler handler) {
		super();
		this.handler = handler;
		if (!folder.isEmpty() && !folder.endsWith("/")) {
			folder += "/";
		}
		this.folder = folder;
	}

	public ZipExtractDoFn(String folder, FileHandler handler, String pattern) {
		this(folder, handler);
		this.pattern = Pattern.compile(pattern);
	}

	@Override
	public void processElement(DoFn<String, String>.ProcessContext ctx) throws Exception {
		String path = ctx.element();
		LOGGER.info("Unzipping {}.", path);
		List<String> outs = new ArrayList<String>();
		try (ZipInputStream zipIn = new ZipInputStream(new BufferedInputStream(handler.open(path)))) {
			for (ZipEntry entry = zipIn.getNextEntry(); entry != null; entry = zipIn.getNextEntry()) {
				if (!entry.isDirectory()) {
					String name = entry.getName();
					LOGGER.debug("Zip content found {}.", name);
					Matcher matcher = pattern.matcher(name);
					if (matcher.matches()) {
						name = name.substring(name.lastIndexOf("/") + 1);
						String filePath = getFolder() + name + "." + UUID.randomUUID().toString() + ".tmp";
						LOGGER.info("Found {}. Writing to {}.", name, filePath);
						String out = handler.write(zipIn, filePath);
						outs.add(out);
					}
				}
				zipIn.closeEntry();
			}
		}
		for (String out : outs) {
			ctx.output(out);
		}
	}

	private String getFolder() {
		setFolder();
		return folder;
	}

	private void setFolder() {
		if (folder == null) {
			folder = folderProvider.get();
			if (!folder.isEmpty() && !folder.endsWith("/")) {
				folder += "/";
			}
		}
	}
}

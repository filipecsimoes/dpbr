package br.gov.pf.labld.dataflow.util;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.api.services.bigquery.model.TableFieldSchema;
import com.google.api.services.bigquery.model.TableRow;
import com.google.api.services.bigquery.model.TableSchema;

public class JSONUtil implements Serializable {

	private static final long serialVersionUID = 4710452274133247763L;

	private static final Locale DEFAULT_LOCALE = new Locale("en", "US");

	private final String inputFormat;
	private final String zoneId;
	private transient SimpleDateFormat inputFormatter;
	private transient SimpleDateFormat outputFormatter;

	public JSONUtil(String inputFormat, String zoneId) {
		super();
		this.inputFormat = inputFormat;
		this.zoneId = zoneId;
	}

	public TableRow toTableRow(Map<String, String> schema, String prefix, TableRow tableRow, ObjectNode json) {
		Set<Entry<String, String>> fields = schema.entrySet();
		for (Entry<String, String> field : fields) {
			String name = field.getKey();
			String key;
			if (prefix != null) {
				key = name.replaceFirst(prefix + ".", "");
			} else {
				key = name;
			}
			String type = field.getValue();
			JsonNode jsonValue = json.get(key);

			if (!tableRow.containsKey(key)) {
				Object newValue = convertValue(jsonValue, type);
				tableRow.set(name, newValue);
			}
		}

		return tableRow;
	}

	public TableRow toTableRow(Map<String, String> schema, TableRow tableRow, ObjectNode json) {
		return toTableRow(schema, null, tableRow, json);
	}

	public TableRow toTableRow(Map<String, String> schema, ObjectNode json) {
		return toTableRow(schema, new TableRow(), json);
	}

	public Object convertValue(JsonNode jsonValue, String type) {
		Object value;

		if (jsonValue == null || jsonValue instanceof NullNode) {
			value = null;
		} else {
			switch (type) {
			case "INTEGER":
				value = jsonValue.asLong();
				break;
			case "FLOAT":
				Double doubleValue = jsonValue.asDouble();
				value = doubleValue.floatValue();
				break;
			case "BOOLEAN":
				value = jsonValue.asBoolean();
				break;
			case "TIMESTAMP":
				String timestampValue = jsonValue.asText();
				try {
					Date parsed = getInputDateTimeFormatter().parse(timestampValue);
					value = getOutputDateTimeFormatter().format(parsed);
				} catch (ParseException e) {
					throw new RuntimeException(e);
				}
				break;
			default:
				value = jsonValue.asText();
				break;
			}
		}
		return value;
	}

	public static Map<String, String> schemaAsMap(TableSchema schema) {
		List<TableFieldSchema> fields = schema.getFields();
		Map<String, String> map = new HashMap<>(fields.size());

		for (TableFieldSchema field : fields) {
			map.put(field.getName(), field.getType());
		}

		return map;
	}

	private SimpleDateFormat getInputDateTimeFormatter() {
		if (inputFormatter == null) {
			inputFormatter = new SimpleDateFormat(inputFormat, DEFAULT_LOCALE);
			inputFormatter.setTimeZone(TimeZone.getTimeZone(ZoneId.of(zoneId)));
		}
		return inputFormatter;
	}

	private SimpleDateFormat getOutputDateTimeFormatter() {
		if (outputFormatter == null) {
			outputFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS z", DEFAULT_LOCALE);
		}
		return outputFormatter;
	}

}

package br.gov.pf.labld.dataflow.fn;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexFix implements Serializable {

	private static final long serialVersionUID = -7517985725472783598L;

	private final String regex;
	private final String fix;

	private Pattern pattern;

	public RegexFix(String regex, String fix) {
		super();
		this.regex = regex;
		this.fix = fix;
	}

	public String fixIfMatch(String line) {
		Matcher matcher = getPattern().matcher(line);
		StringBuffer buff = new StringBuffer(line.length());
		while (matcher.find()) {
			matcher.appendReplacement(buff, fix);
		}
		matcher.appendTail(buff);
		return buff.toString();
	}

	private Pattern getPattern() {
		if (pattern == null) {
			pattern = Pattern.compile(regex);
		}
		return pattern;
	}

	public static void main(String[] args) {
		String str = "\"2011\",\"01000\",\"ASSEMBLEIA LEGISLATIVA\",\"01001\",\"ASSEMBLEIA LEGISLATIVA\",\"010101\",\"ASSEMBLEIA LEGISLATIVA\",\"3\",\"DESPESAS CORRENTES\",\"33\",\"OUTRAS DESPESAS CORRENTES\",\"3390\",\"APLICACOES DIRETAS\",\"339039\",\"OUTROS SERVICOS DE TERCEIROS-PESSOA JURIDICA\",\"33903980\",\"CONSERV.MANUTENC.DE BENS MOVEIS E EQUIPAMENT\",\"01\",\"LEGISLATIVA\",\"031\",\"ACAO LEGISLATIVA\",\"0150\",\"PROCESSO LEGISLATIVO\",\"01031015048170000\",\"FUNCIONAMENTO DO PROCESSO LEGISLATIVO\",\"001\",\"TESOURO-DOT.INICIAL E CRED.SUPLEMENTAR\",\"04209/09\",\"2010NE01592\",\"08720770000110\",\"MARIA JOSINETE PEREIRA ARAUJO - ME\",\"48170000\",\"FUNCIONAMENTO DO PROCESSO LEGISLATIVO\",\"7 - PREGÃO\",\"\",\"\",\"\",\"5800\"";
		RegexFix fix = new RegexFix("([^,]{1})\"\",\"", "$1'\",\"");
		System.out.println(fix.fixIfMatch(str));
	}
}

package br.gov.pf.labld.dataflow.specific.tcesp;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLDecoder;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.pf.labld.dataflow.fn.JSoupProcessorDoFn;

import com.google.cloud.dataflow.sdk.options.ValueProvider;
import com.google.cloud.dataflow.sdk.transforms.DoFn;

public class TCESPDespesasMunicipiosListPageURLDoFn extends JSoupProcessorDoFn<String, String> {

	private static final Logger LOGGER = LoggerFactory.getLogger(TCESPDespesasMunicipiosListPageURLDoFn.class);

	private static final long serialVersionUID = -4801724717993158946L;

	private final MessageFormat fmt = new MessageFormat("http://transparencia.tce.sp.gov.br/municipios-csv?ano_exercicio={0}&page={1}");

	private ValueProvider<String> exercicioProvider;
	private String exercicio;

	public TCESPDespesasMunicipiosListPageURLDoFn(ValueProvider<String> exercicioProvider) {
		super();
		this.exercicioProvider = exercicioProvider;
	}

	public TCESPDespesasMunicipiosListPageURLDoFn(String exercicio) {
		this.exercicio = exercicio;
	}

	@Override
	public void process(DoFn<String, String>.ProcessContext ctx, Document doc) throws Exception {
		Element lastPageEl = doc.select(".pager__item.pager__item--last > a").first();
		String lastPageUrl = lastPageEl.attr("href");
		URI uri = new URI(lastPageUrl);
		String query = uri.getQuery();
		Map<String, List<String>> params = getParams(query);
		String lastPageStr = params.get("page").get(0);
		Integer lastPage = Integer.parseInt(lastPageStr.trim());

		LOGGER.info("Found {} pages.", lastPage);
		for (int page = 0; page <= lastPage; page++) {
			String exercicio = getExercicioOption();
			String url = fmt.format(new Object[] { exercicio, page });
			ctx.output(url);
		}
	}

	private String getExercicio() {
		if (exercicioProvider != null) {
			return exercicioProvider.get();
		} else {
			return exercicio;
		}
	}

	private String getExercicioOption() {
		String exercicioStr = getExercicio();
		int actual = Calendar.getInstance().get(Calendar.YEAR);
		Integer exercicioAsInt = Integer.parseInt(exercicioStr);
		String exercicioOpt = Integer.toString((actual - exercicioAsInt) + 1);
		return exercicioOpt;
	}

	private Map<String, List<String>> getParams(String query) {
		try {
			Map<String, List<String>> params = new HashMap<String, List<String>>();
			for (String param : query.split("&")) {
				String[] pair = param.split("=");
				String key = URLDecoder.decode(pair[0], "UTF-8");
				String value = "";
				if (pair.length > 1) {
					value = URLDecoder.decode(pair[1], "UTF-8");
				}

				List<String> values = params.get(key);
				if (values == null) {
					values = new ArrayList<String>();
					params.put(key, values);
				}
				values.add(value);
			}
			return params;
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	protected URL getURL(DoFn<String, String>.ProcessContext ctx) throws MalformedURLException {
		String exercicio = getExercicioOption();
		String url = fmt.format(new Object[] { exercicio, 0 });
		return new URL(url);
	}

}

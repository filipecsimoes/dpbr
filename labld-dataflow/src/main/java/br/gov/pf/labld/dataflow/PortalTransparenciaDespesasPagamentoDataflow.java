package br.gov.pf.labld.dataflow;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.csv.CSVFormat;

import com.google.api.services.bigquery.model.TableRow;
import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.coders.StringUtf8Coder;
import com.google.cloud.dataflow.sdk.io.BigQueryIO;
import com.google.cloud.dataflow.sdk.io.BigQueryIO.Write.CreateDisposition;
import com.google.cloud.dataflow.sdk.io.BigQueryIO.Write.WriteDisposition;
import com.google.cloud.dataflow.sdk.options.Default;
import com.google.cloud.dataflow.sdk.options.Description;
import com.google.cloud.dataflow.sdk.options.PipelineOptions;
import com.google.cloud.dataflow.sdk.options.PipelineOptionsFactory;
import com.google.cloud.dataflow.sdk.options.ValueProvider;
import com.google.cloud.dataflow.sdk.transforms.Create;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.values.PCollection;
import com.google.cloud.storage.Acl;
import com.google.cloud.storage.Acl.Role;
import com.google.cloud.storage.Acl.User;

import br.gov.pf.labld.dataflow.fn.CSVParseDoFn;
import br.gov.pf.labld.dataflow.fn.CSVRecordLine;
import br.gov.pf.labld.dataflow.fn.CSVRecordToTableRowDoFn;
import br.gov.pf.labld.dataflow.fn.FileHandler;
import br.gov.pf.labld.dataflow.fn.Line;
import br.gov.pf.labld.dataflow.fn.LineReadDoFn;
import br.gov.pf.labld.dataflow.fn.URLDownloadDoFn;
import br.gov.pf.labld.dataflow.fn.ZipExtractDoFn;
import br.gov.pf.labld.dataflow.util.CSVBigQueryConverter;
import br.gov.pf.labld.dataflow.util.CSVBigQueryFieldConverter;
import br.gov.pf.labld.dataflow.util.CSVBigQueryFieldConverter.FloatConverter;

public class PortalTransparenciaDespesasPagamentoDataflow {

    public static final CSVFormat CSV_FORMAT = CSVFormat.EXCEL.withSkipHeaderRecord(true).withDelimiter(';')
            .withRecordSeparator("\r\n");

    private static final String URL_FMT = "'http://www.portaldatransparencia.gov.br/download-de-dados/despesas/'yyyyMMdd";

    public static final CSVBigQueryConverter CONVERTER;

    static {
        CONVERTER = new CSVBigQueryConverter();
        
        CSVBigQueryFieldConverter.TimestampConverter dataFieldConverter = new CSVBigQueryFieldConverter.TimestampConverter("dd/MM/yyyy", "America/Sao_Paulo", true);
        FloatConverter floatConverter = new FloatConverter(new Locale("pt", "BR"), "0.##");

        CONVERTER.addForPosition("codigo_pagamento", 0).setDescription("Código Pagamento");
        CONVERTER.addForPosition("codigo_pagamento_resumido", 1).setDescription("Código Pagamento Resumido");
        CONVERTER.addForPosition("data_emissao", 2).setDescription("Data Emissão").setConverter(dataFieldConverter);
        CONVERTER.addForPosition("codigo_tipo_pagamento", 3).setDescription("Código Tipo Documento");
        CONVERTER.addForPosition("tipo_pagamento", 4).setDescription("Tipo Documento");
        CONVERTER.addForPosition("tipo_ob", 5).setDescription("Tipo OB");
        CONVERTER.addForPosition("extraorcamentario", 6).setDescription("Extraorçamentário");
        CONVERTER.addForPosition("codigo_orgao_superior", 7).setDescription("Código Órgão Superior");
        CONVERTER.addForPosition("orgao_superior", 8).setDescription("Órgão Superior");
        CONVERTER.addForPosition("codigo_orgao", 9).setDescription("Código Órgão");
        CONVERTER.addForPosition("orgao", 10).setDescription("Órgão");
        CONVERTER.addForPosition("codigo_unidade_gestora", 11).setDescription("Código Unidade Gestora");
        CONVERTER.addForPosition("unidade_gestora", 12).setDescription("Unidade Gestora");
        CONVERTER.addForPosition("codigo_gestao", 13).setDescription("Código Gestão");
        CONVERTER.addForPosition("gestao", 14).setDescription("Gestão");
        CONVERTER.addForPosition("codigo_favorecido", 15).setDescription("Código Favorecido");
        CONVERTER.addForPosition("favorecido", 16).setDescription("Favorecido");
        CONVERTER.addForPosition("observacao", 17).setDescription("Observação");
        CONVERTER.addForPosition("processo", 18).setDescription("Processo");
        CONVERTER.addForPosition("codigo_categoria_despesa", 19).setDescription("Código Categoria de Despesa");
        CONVERTER.addForPosition("categoria_despesa", 20).setDescription("Categoria de Despesa");
        CONVERTER.addForPosition("codigo_grupo_despesa", 21).setDescription("Código Grupo de Despesa");
        CONVERTER.addForPosition("grupo_despesa", 22).setDescription("Grupo de Despesa");
        CONVERTER.addForPosition("codigo_modalidade_aplicacao", 23).setDescription("Código Modalidade de Aplicação");
        CONVERTER.addForPosition("modalidade_aplicacao", 24).setDescription("Modalidade de Aplicação");
        CONVERTER.addForPosition("codigo_elemento_despesa", 25).setDescription("Código Elemento de Despesa");
        CONVERTER.addForPosition("elemento_despesa", 26).setDescription("Elemento de Despesa");
        CONVERTER.addForPosition("valor_original", 27).setDescription("Valor Original do Pagamento").setConverter(floatConverter);
        CONVERTER.addForPosition("valor_convertido", 28).setDescription("Valor do Pagamento Convertido pra R$").setConverter(floatConverter);
        CONVERTER.addForPosition("valor_conversao", 29).setDescription("Valor Utilizado na Conversão").setConverter(floatConverter);

    }

    public Pipeline buildPipeline(Options options) throws ParseException {
        Pipeline p = Pipeline.create(options);

        String project = options.getProject();
        String dataset = options.getDataset();
        String table = options.getTable();
        ValueProvider<String> folder = options.getFolder();
        String bucket = options.getBucket();
        List<Acl> acls = new ArrayList<Acl>(Arrays.asList(Acl.of(User.ofAllUsers(), Role.READER)));
        FileHandler.GCSFileHandler fileHandler = new FileHandler.GCSFileHandler(bucket, acls);

        CSVBigQueryConverter converter = getConverter();

        Set<String> urlsSet = new HashSet<>();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat outSdf = new SimpleDateFormat(getURLFormat());

        Calendar calStart = Calendar.getInstance();
        calStart.setTime(sdf.parse(options.getDate()));

        Calendar calEnd = Calendar.getInstance();
        calEnd.setTime(sdf.parse(options.getFinalDate()));

        while (calStart.before(calEnd)) {
            String url = outSdf.format(calStart.getTime());
            urlsSet.add(url);
            calStart.add(Calendar.DAY_OF_YEAR, 1);
        }

        PCollection<String> urls = p.apply(Create.of(urlsSet).withCoder(StringUtf8Coder.of()));
        PCollection<String> zips = urls.apply(ParDo.of(new URLDownloadDoFn(folder, fileHandler, true)).named("DownloadZIP"));
        PCollection<String> csvs = zips
                .apply(ParDo.of(new ZipExtractDoFn(folder, fileHandler, getFileRegex())).named("UnzipCSV"));
        PCollection<Line> lines = csvs.apply(ParDo.of(new LineReadDoFn(fileHandler, 1, "ISO-8859-1")).named("ReadCSV"));
        PCollection<CSVRecordLine> csv = lines.apply(ParDo.of(new CSVParseDoFn(getCSVFormat())).named("ParseCSV"));
        PCollection<TableRow> rows = csv
                .apply(ParDo.of(new CSVRecordToTableRowDoFn(converter)).named("ConvertBigQuery"));

        String tableSpec = project + ":" + dataset + "." + table;
        rows.apply(BigQueryIO.Write.named("WriteBigQuery").to(tableSpec).withSchema(converter.getTableFieldSchema())
                .withWriteDisposition(WriteDisposition.WRITE_TRUNCATE)
                .withCreateDisposition(CreateDisposition.CREATE_IF_NEEDED).withoutValidation());

        return p;
    }

    public static void main(String[] args) throws Exception {
        PortalTransparenciaDespesasPagamentoDataflow dataflow = new PortalTransparenciaDespesasPagamentoDataflow();

        Options options = PipelineOptionsFactory.fromArgs(args).withValidation().as(Options.class);
        Pipeline p = dataflow.buildPipeline(options);
        p.run();
    }

    public CSVFormat getCSVFormat() {
        return CSV_FORMAT;
    }

    public CSVBigQueryConverter getConverter() {
        return CONVERTER;
    }

    public String getFileRegex() {
        return "\\d{8}_Despesas_Pagamento\\.csv";
    }

    public String getURLFormat() {
        return URL_FMT;
    }

    public static interface Options extends PipelineOptions {

        @Description("Bucket for temp data")
        @Default.String("dataflow-uige-temp-data")
        String getBucket();

        void setBucket(String bucket);

        @Description("Folder for temp data")
        @Default.String("portal_transparencia")
        ValueProvider<String> getFolder();

        void setFolder(ValueProvider<String> folder);

        @Description("Dataflow project")
        @Default.String("uige-bigdata")
        String getProject();

        void setProject(String project);

        @Description("Bigquery dataset")
        @Default.String("portal_transparencia")
        String getDataset();

        void setDataset(String project);

        @Description("Bigquery table")
        String getTable();

        void setTable(String value);

        @Description("Date in the format yyyy-MM-dd")
        String getDate();

        void setDate(String date);

        @Description("Date in the format yyyy-MM-dd")
        String getFinalDate();

        void setFinalDate(String date);

    }

}

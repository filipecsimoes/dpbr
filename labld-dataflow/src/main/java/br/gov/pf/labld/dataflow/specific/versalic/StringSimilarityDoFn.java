package br.gov.pf.labld.dataflow.specific.versalic;

import info.debatty.java.stringsimilarity.Cosine;
import info.debatty.java.stringsimilarity.Jaccard;
import info.debatty.java.stringsimilarity.JaroWinkler;
import info.debatty.java.stringsimilarity.NormalizedLevenshtein;
import info.debatty.java.stringsimilarity.SorensenDice;

import java.util.ArrayList;
import java.util.List;

import com.google.api.services.bigquery.model.TableRow;
import com.google.cloud.dataflow.sdk.transforms.DoFn;
import com.google.cloud.dataflow.sdk.values.KV;

public class StringSimilarityDoFn extends DoFn<TableRow, KV<StringsKey, TableRow>> {

	private static final long serialVersionUID = -8487442384824875299L;

	private static List<StringSimilarityCalculator> calculators;

	private String refKey1;
	private String refKey2;
	private String key1;
	private String key2;

	public StringSimilarityDoFn(String refKey1, String refKey2, String key1, String key2) {
		super();
		this.refKey1 = refKey1;
		this.refKey2 = refKey2;
		this.key1 = key1;
		this.key2 = key2;
	}

	@Override
	public void processElement(DoFn<TableRow, KV<StringsKey, TableRow>>.ProcessContext ctx) throws Exception {
		TableRow row = ctx.element();
		String value1 = (String) row.get(key1);
		String value2 = (String) row.get(key2);

		String ref1 = (String) row.get(refKey1);
		String ref2 = (String) row.get(refKey2);

		TableRow out = new TableRow();

		out.set(refKey1, ref1);
		out.set(refKey2, ref2);

		for (StringSimilarityCalculator calculator : getCalculators()) {
			Double similarity = calculator.similarity(value1, value2);
			String name = calculator.getName();
			out.set(name, similarity.floatValue());
		}

		StringsKey key = new StringsKey(ref1, ref2);

		ctx.output(KV.of(key, out));
	}

	public static List<StringSimilarityCalculator> getCalculators() {
		if (calculators == null) {
			calculators = new ArrayList<>();

			calculators.add(new StringSimilarityCalculator(new Cosine()));
			calculators.add(new StringSimilarityCalculator(new Jaccard()));
			calculators.add(new StringSimilarityCalculator(new JaroWinkler()));
			calculators.add(new StringSimilarityCalculator(new NormalizedLevenshtein()));
			calculators.add(new StringSimilarityCalculator(new SorensenDice()));
		}
		return calculators;
	}

}

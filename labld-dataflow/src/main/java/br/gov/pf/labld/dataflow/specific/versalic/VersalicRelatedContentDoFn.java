package br.gov.pf.labld.dataflow.specific.versalic;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.api.services.bigquery.model.TableRow;
import com.google.cloud.dataflow.sdk.transforms.DoFn;

import br.gov.pf.labld.dataflow.fn.JSONContentDoFn;
import br.gov.pf.labld.dataflow.util.JSONUtil;

public class VersalicRelatedContentDoFn extends JSONContentDoFn<TableRow, TableRow> {

	private static final long serialVersionUID = -2861058628469637070L;

	private static final String EMBEDDED = "_embedded";

	private Map<String, String> schema;
	private String fieldName;
	private String relatedEntityName;

	private JSONUtil jsonUtil;

	public VersalicRelatedContentDoFn(Map<String, String> schema, String fieldName, String relatedEntityName) {
		super(true);
		this.schema = schema;
		this.fieldName = fieldName;
		this.relatedEntityName = relatedEntityName;
	}

	@Override
	public void process(DoFn<TableRow, TableRow>.ProcessContext ctx, JsonNode jsonNode) throws Exception {
		TableRow row = ctx.element();
		Object fieldValue = row.get(fieldName);

		ObjectNode embedded = (ObjectNode) jsonNode.get(EMBEDDED);
		if (embedded != null && !(jsonNode.get(EMBEDDED) instanceof NullNode)) {
			ArrayNode arrayNode = (ArrayNode) embedded.get(relatedEntityName);
			Iterator<JsonNode> iterator = arrayNode.elements();
			while (iterator.hasNext()) {
				ObjectNode related = (ObjectNode) iterator.next();
				TableRow tableRow = getJSONUtil().toTableRow(schema, related);
				tableRow.set(fieldName, fieldValue);
				ctx.output(tableRow);
			}
		} else {
			TableRow tableRow = getJSONUtil().toTableRow(schema, (ObjectNode) jsonNode);
			tableRow.set(fieldName, fieldValue);
			ctx.output(tableRow);
		}
	}

	@Override
	public void processElement(DoFn<TableRow, TableRow>.ProcessContext ctx) throws Exception {
		TableRow row = ctx.element();
		Object fieldValue = row.get(relatedEntityName);
		URL url = getRelatedContentURL(fieldValue.toString());

		while (url != null) {
			JsonNode jsonNode = readJsonNode(url);
			if (jsonNode != null) {
				process(ctx, jsonNode);
				url = getNext(jsonNode);
			} else {
				url = null;
			}
		}
	}

	private URL getRelatedContentURL(String fieldValue) {
		try {
			return new URL(fieldValue);
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
	}

	private URL getNext(JsonNode jsonNode) {
		JsonNode linksNode = jsonNode.get("_links");
		if (linksNode == null) {
			return null;
		}

		JsonNode nextNode = linksNode.get("next");
		if (nextNode == null || nextNode instanceof NullNode) {
			return null;
		}

		String url = nextNode.asText();
		try {
			return new URL(url);
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
	}

	private JSONUtil getJSONUtil() {
		if (jsonUtil == null) {
			jsonUtil = new JSONUtil("yyyy-MM-dd", "America/Sao_Paulo");
		}
		return jsonUtil;
	}

}

package br.gov.pf.labld.dataflow.fn;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.client.util.IOUtils;
import com.google.cloud.ReadChannel;
import com.google.cloud.storage.Acl;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageException;
import com.google.cloud.storage.StorageOptions;

import br.gov.pf.labld.dataflow.util.UncloseableInputStream;

public interface FileHandler extends Serializable {

	String write(InputStream in, String filePath) throws IOException;

	InputStream open(String path) throws IOException;

	public static class GCSFileHandler implements FileHandler {

		private static final Logger LOGGER = LoggerFactory.getLogger(GCSFileHandler.class);

		private static final long serialVersionUID = -2435534731977072323L;

		private transient Storage storage;
		private final List<Acl> acls;
		private final String bucket;

		private File tempDir;

		public GCSFileHandler(String bucket, List<Acl> acls) {
			super();
			this.bucket = bucket;
			this.acls = acls;
		}

		@Override
		public String write(InputStream in, String filePath) throws IOException {
			try {
				InputStream uncloseable = new UncloseableInputStream(in);
				BlobInfo blobInfo = BlobInfo.newBuilder(bucket, filePath).setAcl(acls).build();
				BlobId blobId = blobInfo.getBlobId();
				Storage storage = getStorage();
				LOGGER.info("Writing {} to {}.", blobId, bucket);
				storage.create(blobInfo, uncloseable);
				for (Acl acl : acls) {
					storage.createAcl(blobId, acl);
				}
			} catch (StorageException e) {
				throw new IOException(e);
			}
			String gsUrl = "gs://" + bucket + "/" + filePath;
			return gsUrl;
		}

		private BlobId getBlobId(String path) {
			URI uri;
			try {
				uri = new URI(path);
			} catch (URISyntaxException e) {
				throw new RuntimeException(e);
			}
			String bucket = uri.getHost();
			String filePath = uri.getPath().substring(1);
			return BlobId.of(bucket, filePath);
		}

		@Override
		public InputStream open(String path) throws IOException {
			BlobId blob = getBlobId(path);
			String outputName = blob.getName() + "." + UUID.randomUUID().toString() + ".tmp";
			File output = new File(getRoot(), outputName);
			writeToLocalFS(output, blob);
			return new FileInputStream(output);
		}

		private File getRoot() {
			if (tempDir == null) {
				tempDir = com.google.common.io.Files.createTempDir();
			}
			return tempDir;
		}

		private void writeToLocalFS(File output, BlobId blobId) throws IOException {
			LOGGER.info("Writing {} to local fs at {}.", blobId, output);
			output.getParentFile().mkdirs();
			output.createNewFile();
			BufferedOutputStream out = null;
			ReadChannel reader = null;
			WritableByteChannel channel = null;
			try {
				out = new BufferedOutputStream(new FileOutputStream(output));
				reader = getStorage().reader(blobId);
				channel = Channels.newChannel(out);
				ByteBuffer bytes = ByteBuffer.allocate(64 * 1024);
				while (reader.read(bytes) > 0) {
					((Buffer) bytes).flip();
					channel.write(bytes);
					((Buffer) bytes).clear();
				}
			} finally {
				if (out != null) {
					out.close();
				}
				if (reader != null) {
					reader.close();
				}
				if (channel != null) {
					channel.close();
				}
			}
		}

		public Storage getStorage() {
			if (storage == null) {
				storage = StorageOptions.getDefaultInstance().getService();
			}
			return storage;
		}

	}

	public static class LocalFSHandler implements FileHandler {

		private static final long serialVersionUID = 1103344722959634108L;

		private final File root;

		public LocalFSHandler(File root) {
			super();
			this.root = root;
		}

		@Override
		public String write(InputStream in, String filePath) throws IOException {
			File file = new File(root, filePath);
			file.getParentFile().mkdirs();
			try (OutputStream out = new FileOutputStream(file)) {
				InputStream uncloseable = new UncloseableInputStream(in);
				IOUtils.copy(uncloseable, out);
				return "file://" + file.getAbsolutePath();
			}
		}

		@Override
		public InputStream open(String path) throws IOException {
			File file = new File(root, path);
			return new FileInputStream(file);
		}

		public File getRoot() {
			return root;
		}

	}

}
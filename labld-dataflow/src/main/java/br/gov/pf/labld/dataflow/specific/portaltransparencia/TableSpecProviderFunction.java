package br.gov.pf.labld.dataflow.specific.portaltransparencia;

public class TableSpecProviderFunction extends DateFormatProviderFunction {

	private static final long serialVersionUID = -3061140725623993509L;

	public TableSpecProviderFunction(String project, String dataset, String tablePrefix) {
		super("'" + project + ":" + dataset + "." + tablePrefix + "_'yyyy'_'MM");
	}

}

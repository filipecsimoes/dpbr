package br.gov.pf.labld.dataflow.fn;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.cloud.dataflow.sdk.transforms.DoFn;

public class ReadCKANPackageJsonDoFn extends DoFn<String, String> {

	private static final long serialVersionUID = -1414229678623086663L;

	private final ObjectMapper mapper;

	private final FileHandler fileHandler;

	public ReadCKANPackageJsonDoFn(FileHandler fileHandler) {
		super();
		this.fileHandler = fileHandler;
		mapper = new ObjectMapper();
	}

	@Override
	public void processElement(DoFn<String, String>.ProcessContext ctx) throws Exception {
		String path = ctx.element();
		JsonNode jsonNode = null;
		try (InputStream in = new BufferedInputStream(fileHandler.open(path))) {
			ObjectReader reader = mapper.reader();
			jsonNode = reader.readTree(in);
		}
		List<String> outs = new ArrayList<String>();
		JsonNode result = jsonNode.get("result");
		if (result != null) {
			ArrayNode resources = (ArrayNode) result.get("resources");
			Iterator<JsonNode> iterator = resources.iterator();
			while (iterator.hasNext()) {
				JsonNode resource = iterator.next();
				String url = resource.get("url").asText();
				outs.add(url);
			}
		}

		for (String out : outs) {
			ctx.output(out);
		}
	}

}
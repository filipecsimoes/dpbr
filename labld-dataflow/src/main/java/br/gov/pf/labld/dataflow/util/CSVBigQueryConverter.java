package br.gov.pf.labld.dataflow.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.google.api.services.bigquery.model.TableFieldSchema;
import com.google.api.services.bigquery.model.TableSchema;

public class CSVBigQueryConverter implements Serializable {

	private static final long serialVersionUID = -1769649898786730071L;

	private Map<Integer, CSVBigQueryField> fields = new TreeMap<Integer, CSVBigQueryField>();
	private Map<Integer, List<ExtendedCSVBigQueryField>> extendedFields = new TreeMap<Integer, List<ExtendedCSVBigQueryField>>();

	public CSVBigQueryConverter addForPosition(CSVBigQueryField field, int index) {
		fields.put(index, field);
		return this;
	}

	public CSVBigQueryField addForPosition(String name, int index) {
		CSVBigQueryField field = new CSVBigQueryField();
		fields.put(index, field);
		return field.setName(name);
	}

	public ExtendedCSVBigQueryField extendPosition(String name, int index) {
		ExtendedCSVBigQueryField field = new ExtendedCSVBigQueryField();
		List<ExtendedCSVBigQueryField> exFields = extendedFields.get(index);
		if (exFields == null) {
			exFields = new ArrayList<ExtendedCSVBigQueryField>();
			extendedFields.put(index, exFields);
		}
		exFields.add(field);
		field.setName(name);
		return field;
	}

	public CSVBigQueryField get(int index) {
		return fields.get(index);
	}

	public List<ExtendedCSVBigQueryField> getExtensions(int index) {
		return extendedFields.get(index);
	}

	public TableSchema getTableFieldSchema() {
		List<TableFieldSchema> schemaFields = new ArrayList<>();
		for (CSVBigQueryField field : this.fields.values()) {
			schemaFields.add(field.getTableFieldSchema());
		}
		for (List<ExtendedCSVBigQueryField> fields : this.extendedFields.values()) {
			for (ExtendedCSVBigQueryField field : fields) {
				schemaFields.add(field.getTableFieldSchema());
			}
		}
		TableSchema schema = new TableSchema().setFields(schemaFields);
		return schema;
	}

}

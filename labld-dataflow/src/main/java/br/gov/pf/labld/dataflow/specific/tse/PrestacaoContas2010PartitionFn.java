package br.gov.pf.labld.dataflow.specific.tse;

import com.google.cloud.dataflow.sdk.transforms.Partition.PartitionFn;

public class PrestacaoContas2010PartitionFn implements PartitionFn<String> {

	private static final long serialVersionUID = 6932221664347386467L;

	private static final String DESPESAS_CANDIDATOS = "DespesasCandidatos";
	private static final String RECEITAS_CANDIDATOS = "ReceitasCandidatos";
	private static final String DESPESAS_COMITES = "DespesasComites";
	private static final String RECEITAS_COMITES = "ReceitasComites";
	private static final String DESPESAS_PARTIDOS = "DespesasPartidos";
	private static final String RECEITAS_PARTIDOS = "ReceitasPartidos";

	public static final String[] ELEM_NAMES = new String[] { DESPESAS_CANDIDATOS, RECEITAS_CANDIDATOS, DESPESAS_COMITES, RECEITAS_COMITES, DESPESAS_PARTIDOS, RECEITAS_PARTIDOS };

	@Override
	public int partitionFor(String elem, int numPartitions) {
		for (int index = 0; index < numPartitions; index++) {
			if (elem.contains(ELEM_NAMES[index])) {
				return index;
			}
		}
		throw new RuntimeException("Unknown Element: " + elem);
	}

}

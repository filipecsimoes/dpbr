package br.gov.pf.labld.dataflow;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.csv.CSVFormat;

import br.gov.pf.labld.dataflow.fn.CSVParseDoFn;
import br.gov.pf.labld.dataflow.fn.CSVRecordLine;
import br.gov.pf.labld.dataflow.fn.CSVRecordToTableRowDoFn;
import br.gov.pf.labld.dataflow.fn.FileHandler;
import br.gov.pf.labld.dataflow.fn.Line;
import br.gov.pf.labld.dataflow.fn.LineReadDoFn;
import br.gov.pf.labld.dataflow.fn.ValueProviderURLDownloadDoFn;
import br.gov.pf.labld.dataflow.fn.ZipExtractDoFn;
import br.gov.pf.labld.dataflow.specific.portaltransparencia.DateFormatProviderFunction;
import br.gov.pf.labld.dataflow.specific.portaltransparencia.TableSpecProviderFunction;
import br.gov.pf.labld.dataflow.util.CSVBigQueryConverter;

import com.google.api.services.bigquery.model.TableRow;
import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.coders.StringUtf8Coder;
import com.google.cloud.dataflow.sdk.io.BigQueryIO;
import com.google.cloud.dataflow.sdk.io.BigQueryIO.Write.CreateDisposition;
import com.google.cloud.dataflow.sdk.io.BigQueryIO.Write.WriteDisposition;
import com.google.cloud.dataflow.sdk.options.Default;
import com.google.cloud.dataflow.sdk.options.Description;
import com.google.cloud.dataflow.sdk.options.PipelineOptions;
import com.google.cloud.dataflow.sdk.options.ValueProvider;
import com.google.cloud.dataflow.sdk.options.ValueProvider.NestedValueProvider;
import com.google.cloud.dataflow.sdk.transforms.Create;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.values.PCollection;
import com.google.cloud.storage.Acl;
import com.google.cloud.storage.Acl.Role;
import com.google.cloud.storage.Acl.User;

public abstract class PortalTransparenciaDataflow {

	public abstract String getURLFormat();

	public abstract CSVFormat getCSVFormat();

	public abstract CSVBigQueryConverter getConverter();

	public abstract String getFileRegex();

	public abstract String getTablePrefix();

	public Pipeline buildPipeline(Options options) {
		Pipeline p = Pipeline.create(options);

		String project = options.getProject();
		String dataset = options.getDataset();
		ValueProvider<String> folder = options.getFolder();
		String bucket = options.getBucket();
		List<Acl> acls = new ArrayList<Acl>(Arrays.asList(Acl.of(User.ofAllUsers(), Role.READER)));
		FileHandler.GCSFileHandler fileHandler = new FileHandler.GCSFileHandler(bucket, acls);

		CSVBigQueryConverter converter = getConverter();

		ValueProvider<String> urlProvider = NestedValueProvider.of(options.getDate(), new DateFormatProviderFunction(getURLFormat()));

		PCollection<String> voidCollection = p.apply(Create.of("").withCoder(StringUtf8Coder.of()));
		PCollection<String> zips = voidCollection.apply(ParDo.of(new ValueProviderURLDownloadDoFn(urlProvider, folder, fileHandler)).named("DownloadZIP"));
		PCollection<String> csvs = zips.apply(ParDo.of(new ZipExtractDoFn(folder, fileHandler, getFileRegex())).named("UnzipCSV"));
		PCollection<Line> lines = csvs.apply(ParDo.of(new LineReadDoFn(fileHandler, 1, "ISO-8859-1")).named("ReadCSV"));
		PCollection<CSVRecordLine> csv = lines.apply(ParDo.of(new CSVParseDoFn(getCSVFormat())).named("ParseCSV"));
		PCollection<TableRow> rows = csv.apply(ParDo.of(new CSVRecordToTableRowDoFn(converter)).named("ConvertBigQuery"));

		ValueProvider<String> tableSpecProvider = NestedValueProvider.of(options.getDate(), new TableSpecProviderFunction(project, dataset, getTablePrefix()));

		rows.apply(BigQueryIO.Write.named("WriteBigQuery").to(tableSpecProvider).withSchema(converter.getTableFieldSchema()).withWriteDisposition(WriteDisposition.WRITE_TRUNCATE)
				.withCreateDisposition(CreateDisposition.CREATE_IF_NEEDED).withoutValidation());

		return p;
	}

	public static interface Options extends PipelineOptions {

		@Description("Bucket for temp data")
		@Default.String("dataflow-uige-temp-data")
		String getBucket();

		void setBucket(String bucket);

		@Description("Folder for temp data")
		@Default.String("portal_transparencia")
		ValueProvider<String> getFolder();

		void setFolder(ValueProvider<String> folder);

		@Description("Dataflow project")
		@Default.String("arctic-cursor-171520")
		String getProject();

		void setProject(String project);

		@Description("Bigquery dataset")
		@Default.String("portal_transparencia")
		String getDataset();

		void setDataset(String project);

		@Description("Date in the format yyyy-MM-dd")
		ValueProvider<String> getDate();

		void setDate(ValueProvider<String> date);

	}

}

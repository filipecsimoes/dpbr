package br.gov.pf.labld.dataflow.specific.tse;

import com.google.cloud.dataflow.sdk.transforms.Partition.PartitionFn;

public class PrestacaoContas2014PartitionFn implements PartitionFn<String> {

	private static final long serialVersionUID = -7430602074358571450L;

	private static final String DESPESAS_CANDIDATOS = "despesas_candidatos";
	private static final String DESPESAS_COMITES = "despesas_comites";
	private static final String DESPESAS_PARTIDOS = "despesas_partidos";
	private static final String RECEITAS_CANDIDATOS = "receitas_candidatos";
	private static final String RECEITAS_COMITES = "receitas_comites";
	private static final String RECEITAS_PARTIDOS = "receitas_partidos";

	public static final String[] ELEM_NAMES = new String[] { DESPESAS_CANDIDATOS, DESPESAS_COMITES, DESPESAS_PARTIDOS, RECEITAS_CANDIDATOS, RECEITAS_COMITES, RECEITAS_PARTIDOS };

	@Override
	public int partitionFor(String elem, int numPartitions) {
		for (int index = 0; index < ELEM_NAMES.length; index++) {
			if (elem.contains(ELEM_NAMES[index])) {
				return index;
			}
		}
		throw new RuntimeException("Unknown Element: " + elem);
	}

}
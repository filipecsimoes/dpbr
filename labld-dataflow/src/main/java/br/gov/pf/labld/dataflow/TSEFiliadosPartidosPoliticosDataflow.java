package br.gov.pf.labld.dataflow;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.csv.CSVFormat;

import br.gov.pf.labld.dataflow.fn.CSVParseDoFn;
import br.gov.pf.labld.dataflow.fn.CSVRecordLine;
import br.gov.pf.labld.dataflow.fn.CSVRecordToTableRowDoFn;
import br.gov.pf.labld.dataflow.fn.FileHandler;
import br.gov.pf.labld.dataflow.fn.Line;
import br.gov.pf.labld.dataflow.fn.LineReadDoFn;
import br.gov.pf.labld.dataflow.fn.ReadCKANPackageJsonDoFn;
import br.gov.pf.labld.dataflow.fn.URLDownloadDoFn;
import br.gov.pf.labld.dataflow.fn.ZipExtractDoFn;
import br.gov.pf.labld.dataflow.util.CSVBigQueryConverter;
import br.gov.pf.labld.dataflow.util.CSVBigQueryField;
import br.gov.pf.labld.dataflow.util.CSVBigQueryFieldConverter;

import com.google.api.services.bigquery.model.TableRow;
import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.coders.StringUtf8Coder;
import com.google.cloud.dataflow.sdk.io.BigQueryIO;
import com.google.cloud.dataflow.sdk.io.BigQueryIO.Write.CreateDisposition;
import com.google.cloud.dataflow.sdk.io.BigQueryIO.Write.WriteDisposition;
import com.google.cloud.dataflow.sdk.options.Default;
import com.google.cloud.dataflow.sdk.options.Description;
import com.google.cloud.dataflow.sdk.options.PipelineOptions;
import com.google.cloud.dataflow.sdk.options.PipelineOptionsFactory;
import com.google.cloud.dataflow.sdk.transforms.Create;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.values.PCollection;
import com.google.cloud.storage.Acl;
import com.google.cloud.storage.Acl.Role;
import com.google.cloud.storage.Acl.User;

public class TSEFiliadosPartidosPoliticosDataflow {

	public static final String FILIADOS_URL = "http://dados.gov.br/api/3/action/package_show?id=fb75f496-923a-4fd1-badd-70c9fc614b0c";
	public static final CSVFormat CSV_FORMAT = CSVFormat.EXCEL.withSkipHeaderRecord(true).withQuote('"').withDelimiter(';').withRecordSeparator("\r\n");
	public static final CSVBigQueryConverter CONVERTER;

	static {
		CONVERTER = new CSVBigQueryConverter();

		CSVBigQueryFieldConverter.TimestampConverter dataFieldConverter = new CSVBigQueryFieldConverter.TimestampConverter("dd/MM/yyyy", "America/Sao_Paulo");

		CSVBigQueryField dataField = new CSVBigQueryField().setConverter(dataFieldConverter).setName("data_extracao").setType("TIMESTAMP");
		dataField.setDescription("Data da extra\u00e7\u00e3o das informa\u00e7\u00f5es do banco de dados");
		CONVERTER.addForPosition(dataField, 0);

		CSVBigQueryField numeroInscricaoField = new CSVBigQueryField().setConverter(CSVBigQueryFieldConverter.STRING_CONVERTER).setName("numero_inscricao").setType("STRING");
		numeroInscricaoField.setDescription("N\u00famero da inscri\u00e7\u00e3o eleitoral");
		CONVERTER.addForPosition(numeroInscricaoField, 2);

		CSVBigQueryField nomeField = new CSVBigQueryField().setConverter(CSVBigQueryFieldConverter.STRING_CONVERTER).setName("nome_filiado").setType("STRING");
		nomeField.setDescription("Nome do filiado");
		CONVERTER.addForPosition(nomeField, 3);

		CSVBigQueryField siglaField = new CSVBigQueryField().setConverter(CSVBigQueryFieldConverter.STRING_CONVERTER).setName("sigla_partido").setType("STRING");
		siglaField.setDescription("Sigla do partido pol\u00edtico");
		CONVERTER.addForPosition(siglaField, 4);

		CSVBigQueryField partidoField = new CSVBigQueryField().setConverter(CSVBigQueryFieldConverter.STRING_CONVERTER).setName("nome_partido").setType("STRING");
		partidoField.setDescription("Nome do partido pol\u00edtico");
		CONVERTER.addForPosition(partidoField, 5);

		CSVBigQueryField ufField = new CSVBigQueryField().setConverter(CSVBigQueryFieldConverter.STRING_CONVERTER).setName("uf").setType("STRING");
		ufField.setDescription("Unidade da federa\u00e7\u00e3o");
		CONVERTER.addForPosition(ufField, 6);

		CSVBigQueryField codigoMunicipioField = new CSVBigQueryField().setConverter(CSVBigQueryFieldConverter.STRING_CONVERTER).setName("codigo_municipio").setType("STRING");
		codigoMunicipioField.setDescription("C\u00f3digo do munic\u00edpio");
		CONVERTER.addForPosition(codigoMunicipioField, 7);

		CSVBigQueryField nomeMunicipioField = new CSVBigQueryField().setConverter(CSVBigQueryFieldConverter.STRING_CONVERTER).setName("nome_municipio").setType("STRING");
		nomeMunicipioField.setDescription("Nome do munic\u00edpio");
		CONVERTER.addForPosition(nomeMunicipioField, 8);

		CSVBigQueryField zonaEleitoralField = new CSVBigQueryField().setConverter(CSVBigQueryFieldConverter.STRING_CONVERTER).setName("zona_eleitoral").setType("STRING");
		zonaEleitoralField.setDescription("Zona eleitoral");
		CONVERTER.addForPosition(zonaEleitoralField, 9);

		CSVBigQueryField secaoEleitoralField = new CSVBigQueryField().setConverter(CSVBigQueryFieldConverter.STRING_CONVERTER).setName("secao_eleitoral").setType("STRING");
		secaoEleitoralField.setDescription("Se\u00e7\u00e3o eleitoral");
		CONVERTER.addForPosition(secaoEleitoralField, 10);

		CSVBigQueryField dataFiliacaoField = new CSVBigQueryField().setConverter(dataFieldConverter).setName("data_filiacao").setType("TIMESTAMP");
		dataFiliacaoField.setDescription("Data da filia\u00e7\u00e3o");
		CONVERTER.addForPosition(dataFiliacaoField, 11);

		CSVBigQueryField situacaoRegistroField = new CSVBigQueryField().setConverter(CSVBigQueryFieldConverter.STRING_CONVERTER).setName("situacao_registro").setType("STRING");
		situacaoRegistroField.setDescription("Situa\u00e7\u00e3o do registro de filia\u00e7\u00e3o");
		CONVERTER.addForPosition(situacaoRegistroField, 12);

		CSVBigQueryField tipoRegistroField = new CSVBigQueryField().setConverter(CSVBigQueryFieldConverter.STRING_CONVERTER).setName("tipo_registro").setType("STRING");
		tipoRegistroField.setDescription("Tipo do registro de filia\u00e7\u00e3o");
		CONVERTER.addForPosition(tipoRegistroField, 13);

		CSVBigQueryField dataProcessamentoField = new CSVBigQueryField().setConverter(dataFieldConverter).setName("data_processamento").setType("TIMESTAMP");
		dataProcessamentoField.setDescription("Data do processamento do registro de filia\u00e7\u00e3o");
		CONVERTER.addForPosition(dataProcessamentoField, 14);

		CSVBigQueryField dataDesfiliacaoField = new CSVBigQueryField().setConverter(dataFieldConverter).setName("data_desfiliacao").setType("TIMESTAMP");
		dataDesfiliacaoField.setDescription("Data da desfilia\u00e7\u00e3o");
		CONVERTER.addForPosition(dataDesfiliacaoField, 15);

		CSVBigQueryField dataCancelamentoField = new CSVBigQueryField().setConverter(dataFieldConverter).setName("data_cancelamento").setType("TIMESTAMP");
		dataCancelamentoField.setDescription("Data do cancelamento do registro de filia\u00e7\u00e3o");
		CONVERTER.addForPosition(dataCancelamentoField, 16);

		CSVBigQueryField dataRegularizacaoField = new CSVBigQueryField().setConverter(dataFieldConverter).setName("data_regularizacao").setType("TIMESTAMP");
		dataRegularizacaoField.setDescription("Data da regulariza\u00e7\u00e3o do registro de filia\u00e7\u00e3o");
		CONVERTER.addForPosition(dataRegularizacaoField, 17);

		CSVBigQueryField motivoCancelamentoField = new CSVBigQueryField().setConverter(CSVBigQueryFieldConverter.STRING_CONVERTER).setName("motivo_cancelamento").setType("STRING");
		motivoCancelamentoField.setDescription("Motivo do cancelamento do registro de filia\u00e7\u00e3o");
		CONVERTER.addForPosition(motivoCancelamentoField, 18);

	}

	public static interface Options extends PipelineOptions {

		@Description("Bucket for temp data")
		@Default.String("dataflow-temp-data")
		String getBucket();

		void setBucket(String bucket);

		@Description("Folder for temp data")
		@Default.String("")
		String getFolder();

		void setFolder(String folder);

		@Description("BigQuery table to write to.")
		@Default.String("arctic-cursor-171520:dados_publicos.filiados_partidos_politicos")
		String getTableSpec();

		void setTableSpec(String tableSpec);

	}

	public static void main(String[] args) {
		Options options = PipelineOptionsFactory.fromArgs(args).withValidation().as(Options.class);
		Pipeline p = Pipeline.create(options);

		String folder = options.getFolder();
		String bucket = options.getBucket();
		String tableSpec = options.getTableSpec();
		List<Acl> acls = new ArrayList<Acl>(Arrays.asList(Acl.of(User.ofAllUsers(), Role.READER)));
		FileHandler.GCSFileHandler fileHandler = new FileHandler.GCSFileHandler(bucket, acls);

		PCollection<String> url = p.apply(Create.of(FILIADOS_URL).withCoder(StringUtf8Coder.of()));
		PCollection<String> json = url.apply(ParDo.of(new URLDownloadDoFn(folder, fileHandler)).named("DownloadJSON"));
		PCollection<String> zipUrls = json.apply(ParDo.of(new ReadCKANPackageJsonDoFn(fileHandler)).named("ParseJSON"));
		PCollection<String> zips = zipUrls.apply(ParDo.of(new URLDownloadDoFn(folder, fileHandler, true)).named("DownloadZIP"));
		PCollection<String> csvs = zips.apply(ParDo.of(new ZipExtractDoFn(folder, fileHandler, ".+\\.csv$")).named("UnzipCSV"));
		PCollection<Line> lines = csvs.apply(ParDo.of(new LineReadDoFn(fileHandler, 1, "ISO-8859-1")).named("ReadCSV"));
		PCollection<CSVRecordLine> csv = lines.apply(ParDo.of(new CSVParseDoFn(CSV_FORMAT, true)).named("ParseCSV"));
		PCollection<TableRow> rows = csv.apply(ParDo.of(new CSVRecordToTableRowDoFn(CONVERTER)).named("ConvertBigQuery"));
		rows.apply(BigQueryIO.Write.named("WriteBigQuery").to(tableSpec).withSchema(CONVERTER.getTableFieldSchema()).withWriteDisposition(WriteDisposition.WRITE_TRUNCATE)
				.withCreateDisposition(CreateDisposition.CREATE_IF_NEEDED).withoutValidation());

		p.run();
	}

}
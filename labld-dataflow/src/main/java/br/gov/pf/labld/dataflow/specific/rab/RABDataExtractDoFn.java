package br.gov.pf.labld.dataflow.specific.rab;

import java.net.MalformedURLException;
import java.net.URL;

import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.pf.labld.dataflow.fn.JSoupProcessorDoFn;
import br.gov.pf.labld.dataflow.util.CSVBigQueryFieldConverter.TimestampConverter;

import com.google.api.services.bigquery.model.TableRow;
import com.google.cloud.dataflow.sdk.transforms.DoFn;

public class RABDataExtractDoFn extends JSoupProcessorDoFn<String, TableRow> {

	private static final Logger LOGGER = LoggerFactory.getLogger(RABDataExtractDoFn.class);

	private static final long serialVersionUID = 3000444860013010529L;

	private transient TimestampConverter converterSimple;
	private transient TimestampConverter converterComplete;

	@Override
	public void process(DoFn<String, TableRow>.ProcessContext ctx, Document doc) throws Exception {
		try {
			RABData rab = new RABData(doc, getConverterSimple(), getConverterComplete());
			if (rab.isAeronaveCadastrada()) {
				String prefixo = rab.getPrefixo();

				LOGGER.info("Extracting data of prefix {} at {}", new Object[] { prefixo, ctx.element() });
				TableRow row = new TableRow();

				row.set("prefixo", prefixo);
				row.set("proprietario", rab.getProprietario());
				row.set("cpf_cnpj_proprietario", rab.getCpfCnpjProprietario());
				row.set("operador", rab.getOperador());
				row.set("cpf_cnpj_operador", rab.getCpfCnpjOperador());
				row.set("fabricante", rab.getFabricante());
				row.set("modelo", rab.getModelo());
				row.set("numero_serie", rab.getNumeroSerie());
				row.set("tipo_icao", rab.getTipoIcao());
				row.set("tipo_habilitacao_pilotos", rab.getTipoHabilitacaoPilotos());
				row.set("classe_aeronave", rab.getClasseAeronave());
				row.set("peso_maximo_decolagem", rab.getPesoMaximoDecolagem());
				row.set("numero_maximo_passageiros", rab.getNumeroMaximoPassageiros());
				row.set("tipo_voo_autorizado", rab.getTipoVooAutorizado());
				row.set("categoria_registro", rab.getCategoriaRegistro());
				row.set("numero_certificados", rab.getNumeroCertificados());
				row.set("situacao_rab", rab.getSituacaoRab());
				row.set("data_compra_transferencia", rab.getDataCompraTransferencia());
				row.set("data_validade_ca", rab.getDataValidadeCa());
				row.set("data_validade_iam", rab.getDataValidadeIam());
				row.set("situacao_aeronavegabilidade", rab.getSituacaoAeronavegabilidade());
				row.set("motivos", rab.getMotivos());

				ctx.output(row);
			} else {
				LOGGER.info("Aircraft not found at {}", new Object[] { ctx.element() });
			}
		} catch (Exception e) {
			LOGGER.error("Error extracting data from {}", new Object[] { ctx.element() });
			throw e;
		}
	}

	@Override
	protected URL getURL(DoFn<String, TableRow>.ProcessContext ctx) throws MalformedURLException {
		URL url = new URL(ctx.element());
		return url;
	}

	@Override
	protected String getMethod() {
		return "POST";
	}

	@Override
	protected boolean ignoreCerticates() {
		return true;
	}

	public TimestampConverter getConverterSimple() {
		if (converterSimple == null) {
			converterSimple = new TimestampConverter("ddMMyy", "America/Sao_Paulo");
		}
		return converterSimple;
	}

	public TimestampConverter getConverterComplete() {
		if (converterComplete == null) {
			converterComplete = new TimestampConverter("dd/MM/yy", "America/Sao_Paulo");
		}
		return converterComplete;
	}

	@Override
	protected String getEncoding() {
		return "windows-1252";
	}

}

package br.gov.pf.labld.dataflow;

import static br.gov.pf.labld.dataflow.specific.btc.BCoinBlockDataSyncDoFn.BLOCK_RECORD_TAG;
import static br.gov.pf.labld.dataflow.specific.btc.BCoinBlockDataSyncDoFn.TX_HASH_TAG;
import static br.gov.pf.labld.dataflow.specific.btc.BCoinTransactionDataSyncDoFn.TX_DATA_RECORD_TAG;
import static br.gov.pf.labld.dataflow.specific.btc.BCoinTransactionDataSyncDoFn.TX_INPUT_RECORD_TAG;
import static br.gov.pf.labld.dataflow.specific.btc.BCoinTransactionDataSyncDoFn.TX_OUTPUT_RECORD_TAG;

import java.io.File;
import java.text.DecimalFormat;

import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.coders.AvroCoder;
import com.google.cloud.dataflow.sdk.coders.BigEndianIntegerCoder;
import com.google.cloud.dataflow.sdk.coders.CoderRegistry;
import com.google.cloud.dataflow.sdk.io.AvroIO;
import com.google.cloud.dataflow.sdk.options.DataflowPipelineOptions;
import com.google.cloud.dataflow.sdk.options.Default;
import com.google.cloud.dataflow.sdk.options.Description;
import com.google.cloud.dataflow.sdk.options.PipelineOptionsFactory;
import com.google.cloud.dataflow.sdk.transforms.Create;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.values.PCollection;
import com.google.cloud.dataflow.sdk.values.PCollectionTuple;
import com.google.cloud.dataflow.sdk.values.TupleTagList;

import br.gov.pf.labld.bcoin.Block;
import br.gov.pf.labld.bcoin.Transaction;
import br.gov.pf.labld.bcoin.TransactionInput;
import br.gov.pf.labld.bcoin.TransactionOutput;
import br.gov.pf.labld.dataflow.specific.btc.BCoinBlockDataSyncDoFn;
import br.gov.pf.labld.dataflow.specific.btc.BCoinSyncDoFn;
import br.gov.pf.labld.dataflow.specific.btc.BCoinTransactionDataSyncDoFn;

public class BCoinSyncDataflow {

	private static final String TRANSACTIONS_OUTPUTS_FILE_NAME = "transactions_outputs";
	private static final String TRANSACTIONS_INPUTS_FILE_NAME = "transactions_inputs";
	private static final String BLOCKS_FILE_NAME = "blocks";
	private static final String TRANSACTIONS_FILE_NAME = "transactions";

	private static final String FILE_NAME_SUFFIX = ".avro";;

	public static interface Options extends DataflowPipelineOptions {

		@Description("Initial index")
		@Default.Integer(1)
		Integer getIterations();

		void setIterations(Integer iterations);

		@Description("Initial index")
		@Default.Integer(0)
		Integer getInitialIndex();

		void setInitialIndex(Integer initialIndex);

		@Description("Max index interval")
		@Default.Integer(10)
		Integer getMaxIndexInterval();

		void setMaxIndexInterval(Integer maxIndex);

		@Description("BCoin url")
		@Default.String("http://localhost:8332")
		String getBCoinUrl();

		void setBCoinUrl(String bcoinUrl);

		@Description("Output Path")
		String getOutputPath();

		void setOutputPath(String outputPath);

		@Description("File Prefix")
		@Default.String("")
		String getFilePrefix();

		void setFilePrefix(String file);

	}

	private static final DecimalFormat INDEX_FMT = new DecimalFormat("0000000");

	public static void main(String[] args) {
		Options options = PipelineOptionsFactory.fromArgs(args).withValidation().as(Options.class);

		int initialIndex = options.getInitialIndex();
		int maxIndexInterval = options.getMaxIndexInterval();
		int iterations = options.getIterations();

		String outputPath = options.getOutputPath();

		for (int iteration = 0; iteration < iterations; iteration++) {
			String filePrefix = INDEX_FMT.format(initialIndex) + "-"
					+ INDEX_FMT.format(initialIndex + maxIndexInterval - 1) + "-";
			options.setInitialIndex(initialIndex);
			options.setFilePrefix(filePrefix);
			if (filesExists(outputPath, filePrefix)) {
				System.out.println("DONE " + filePrefix);
			} else {
				System.out.println("DOING... " + filePrefix);
				run(options);
			}
			initialIndex = initialIndex + maxIndexInterval;
		}
	}

	private static boolean filesExists(String outputPath, String prefix) {
		File blocks = new File(outputPath, prefix + BLOCKS_FILE_NAME + FILE_NAME_SUFFIX);
		File transactions = new File(outputPath, prefix + TRANSACTIONS_FILE_NAME + FILE_NAME_SUFFIX);
		File transactionsInputs = new File(outputPath, prefix + TRANSACTIONS_INPUTS_FILE_NAME + FILE_NAME_SUFFIX);
		File transactionsOutputs = new File(outputPath, prefix + TRANSACTIONS_OUTPUTS_FILE_NAME + FILE_NAME_SUFFIX);

		return blocks.exists() && transactions.exists() && transactionsInputs.exists() && transactionsOutputs.exists();
	}

	protected static void run(Options options) {
		Pipeline pipeline = Pipeline.create(options);

		CoderRegistry cr = pipeline.getCoderRegistry();
		cr.registerCoder(Block.class, AvroCoder.of(Block.class));
		cr.registerCoder(Transaction.class, AvroCoder.of(Transaction.class));
		cr.registerCoder(TransactionInput.class, AvroCoder.of(TransactionInput.class));
		cr.registerCoder(TransactionOutput.class, AvroCoder.of(TransactionOutput.class));

		Integer initialIndex = options.getInitialIndex();
		Integer maxIndexInterval = options.getMaxIndexInterval();
		String bcoinUrl = options.getBCoinUrl();
		String outputPath = options.getOutputPath();
		String filePrefix = options.getFilePrefix();

		PCollection<Integer> initial = pipeline.apply(Create.of(initialIndex).withCoder(BigEndianIntegerCoder.of()));
		PCollection<Integer> indexes = initial.apply(ParDo.of(new BCoinSyncDoFn(bcoinUrl, maxIndexInterval)));
		PCollectionTuple blockData = indexes.apply(ParDo.withOutputTags(BLOCK_RECORD_TAG, TupleTagList.of(TX_HASH_TAG))
				.of(new BCoinBlockDataSyncDoFn(bcoinUrl)));

		PCollection<Block> blockRecords = blockData.get(BLOCK_RECORD_TAG);

		blockRecords.apply(AvroIO.Write.to(outputPath + "/" + filePrefix + BLOCKS_FILE_NAME).withoutSharding()
				.withSuffix(FILE_NAME_SUFFIX).withSchema(Block.class));

		PCollection<String> txHashes = blockData.get(TX_HASH_TAG);

		PCollectionTuple txData = txHashes.apply(
				ParDo.withOutputTags(TX_DATA_RECORD_TAG, TupleTagList.of(TX_INPUT_RECORD_TAG).and(TX_OUTPUT_RECORD_TAG))
						.of(new BCoinTransactionDataSyncDoFn(bcoinUrl)));

		PCollection<Transaction> txRecords = txData.get(TX_DATA_RECORD_TAG);

		txRecords.apply(AvroIO.Write.to(outputPath + "/" + filePrefix + TRANSACTIONS_FILE_NAME).withoutSharding()
				.withSuffix(FILE_NAME_SUFFIX).withSchema(Transaction.class));

		PCollection<TransactionInput> txInputRecords = txData.get(TX_INPUT_RECORD_TAG);

		txInputRecords.apply(AvroIO.Write.to(outputPath + "/" + filePrefix + TRANSACTIONS_INPUTS_FILE_NAME)
				.withoutSharding().withSuffix(FILE_NAME_SUFFIX).withSchema(TransactionInput.class));

		PCollection<TransactionOutput> txOutputRecords = txData.get(TX_OUTPUT_RECORD_TAG);
		txOutputRecords.apply(AvroIO.Write.to(outputPath + "/" + filePrefix + TRANSACTIONS_OUTPUTS_FILE_NAME)
				.withoutSharding().withSuffix(FILE_NAME_SUFFIX).withSchema(TransactionOutput.class));

		pipeline.run();
	}

}

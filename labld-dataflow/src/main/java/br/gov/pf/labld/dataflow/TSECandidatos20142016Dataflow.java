package br.gov.pf.labld.dataflow;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.csv.CSVFormat;

import br.gov.pf.labld.dataflow.fn.CSVParseDoFn;
import br.gov.pf.labld.dataflow.fn.CSVRecordLine;
import br.gov.pf.labld.dataflow.fn.CSVRecordToTableRowDoFn;
import br.gov.pf.labld.dataflow.fn.FileHandler;
import br.gov.pf.labld.dataflow.fn.Line;
import br.gov.pf.labld.dataflow.fn.LineReadDoFn;
import br.gov.pf.labld.dataflow.fn.URLDownloadDoFn;
import br.gov.pf.labld.dataflow.fn.ZipExtractDoFn;
import br.gov.pf.labld.dataflow.util.CSVBigQueryConverter;
import br.gov.pf.labld.dataflow.util.CSVBigQueryFieldConverter;

import com.google.api.services.bigquery.model.TableRow;
import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.coders.StringUtf8Coder;
import com.google.cloud.dataflow.sdk.io.BigQueryIO;
import com.google.cloud.dataflow.sdk.io.BigQueryIO.Write.CreateDisposition;
import com.google.cloud.dataflow.sdk.io.BigQueryIO.Write.WriteDisposition;
import com.google.cloud.dataflow.sdk.options.Default;
import com.google.cloud.dataflow.sdk.options.Description;
import com.google.cloud.dataflow.sdk.options.PipelineOptions;
import com.google.cloud.dataflow.sdk.options.PipelineOptionsFactory;
import com.google.cloud.dataflow.sdk.transforms.Create;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.values.PCollection;
import com.google.cloud.storage.Acl;
import com.google.cloud.storage.Acl.Role;
import com.google.cloud.storage.Acl.User;

public class TSECandidatos20142016Dataflow {

	private static final MessageFormat URL_FMT = new MessageFormat("http://agencia.tse.jus.br/estatistica/sead/odsele/consulta_cand/consulta_cand_{0}.zip");

	public static final CSVFormat CSV_FORMAT = CSVFormat.EXCEL.withSkipHeaderRecord(true).withQuote('"').withDelimiter(';').withRecordSeparator("\r\n");
	public static final CSVBigQueryConverter CONVERTER;

	static {
		CONVERTER = new CSVBigQueryConverter();

		CSVBigQueryFieldConverter.TimestampConverter dataFieldConverter = new CSVBigQueryFieldConverter.TimestampConverter("dd/MM/yyyy", "America/Sao_Paulo");

		CONVERTER.addForPosition("ano_eleicao", 2).setDescription("Ano da elei\u00e7\u00e3o");
		CONVERTER.addForPosition("num_turno", 3).setDescription("N\u00famero do turno");
		CONVERTER.addForPosition("descricao_eleicao", 4).setDescription("Descri\u00e7\u00e3o da elei\u00e7\u00e3o");
		CONVERTER.addForPosition("sigla_uf", 5).setDescription("Sigla da Unidade da Federa\u00e7\u00e3o em que ocorreu a elei\u00e7\u00e3o");
		CONVERTER
				.addForPosition("sigla_ue", 6)
				.setDescription(
						"Sigla da Unidade Eleitoral (Em caso de elei\u00e7\u00e3o majorit\u00e1ria \u00e9 a sigla da UF que o candidato concorre (texto) e em caso de elei\u00e7\u00e3o municipal \u00e9 o c\u00f3digo TSE do munic\u00edpio (n\u00famero)). Assume os valores especiais BR, ZZ e VT para designar, respectivamente, o Brasil, Exterior e Voto em Tr\u00e2nsito");
		CONVERTER.addForPosition("descricao_ue", 7).setDescription("Descri\u00e7\u00e3o da Unidade Eleitoral");
		CONVERTER.addForPosition("codigo_cargo", 8).setDescription("C\u00f3digo do cargo a que o candidato concorre");
		CONVERTER.addForPosition("descricao_cargo", 9).setDescription("Descri\u00e7\u00e3o do cargo a que o candidato concorre");
		CONVERTER.addForPosition("nome_candidato", 10).setDescription("Nome completo do candidato");
		CONVERTER.addForPosition("sequencial_candidato", 11).setDescription(
				"N\u00famero sequencial do candidato gerado internamente pelos sistemas eleitorais. N\u00e3o \u00e9 o n\u00famero de campanha do candidato.");
		CONVERTER.addForPosition("numero_candidato", 12).setDescription("N\u00famero do candidato na urna");
		CONVERTER.addForPosition("cpf_candidato", 13).setDescription("CPF do candidato");
		CONVERTER.addForPosition("nome_urna_candidato", 14).setDescription("Nome de urna do candidato");
		CONVERTER.addForPosition("cod_situacao_candidatura", 15).setDescription("C\u00f3digo da situa\u00e7\u00e3o de candidatura");
		CONVERTER.addForPosition("des_situacao_candidatura", 16).setDescription("Descri\u00e7\u00e3o da situa\u00e7\u00e3o de candidatura");
		CONVERTER.addForPosition("numero_partido", 17).setDescription("N\u00famero do partido");
		CONVERTER.addForPosition("sigla_partido", 18).setDescription("Sigla do partido");
		CONVERTER.addForPosition("nome_partido", 19).setDescription("Nome do partido");
		CONVERTER.addForPosition("codigo_legenda", 20).setDescription("C\u00f3digo sequencial da legenda gerado pela Justi\u00e7a Eleitoral");
		CONVERTER.addForPosition("sigla_legenda", 21).setDescription("Sigla da legenda");
		CONVERTER.addForPosition("composicao_legenda", 22).setDescription("Composi\u00e7\u00e3o da legenda");
		CONVERTER.addForPosition("nome_legenda", 23).setDescription("Nome da legenda");
		CONVERTER.addForPosition("codigo_ocupacao", 24).setDescription("C\u00f3digo da ocupa\u00e7\u00e3o do candidato");
		CONVERTER.addForPosition("descricao_ocupacao", 25).setDescription("Descri\u00e7\u00e3o da ocupa\u00e7\u00e3o do candidato");
		CONVERTER.addForPosition("data_nascimento", 26).setDescription("Data de nascimento do candidato").setConverter(dataFieldConverter);
		CONVERTER.addForPosition("num_titulo_eleitoral_candidato", 27).setDescription("N\u00famero do t\u00edtulo eleitoral do candidato");
		CONVERTER.addForPosition("idade_data_eleicao", 28).setDescription("Idade do candidato da data da elei\u00e7\u00e3o");
		CONVERTER.addForPosition("codigo_sexo", 29).setDescription("C\u00f3digo do sexo do candidato");
		CONVERTER.addForPosition("descricao_sexo", 30).setDescription("Descri\u00e7\u00e3o do sexo do candidato");
		CONVERTER.addForPosition("cod_grau_instrucao", 31).setDescription("C\u00f3digo do grau de instru\u00e7\u00e3o do candidato.");
		CONVERTER.addForPosition("descricao_grau_instrucao", 32).setDescription("Descri\u00e7\u00e3o do grau de instru\u00e7\u00e3o do candidato");
		CONVERTER.addForPosition("codigo_estado_civil", 33).setDescription("C\u00f3digo do estado civil do candidato");
		CONVERTER.addForPosition("descricao_estado_civil", 34).setDescription("Descri\u00e7\u00e3o do estado civil do candidato");
		CONVERTER.addForPosition("codigo_cor_raca", 35).setDescription("C\u00f3digo da cor/ra\u00e7a do candidato (auto declara\u00e7\u00e3o)");
		CONVERTER.addForPosition("descricao_cor_raca", 36).setDescription("Descri\u00e7\u00e3o da cor/ra\u00e7a do candidato (auto declara\u00e7\u00e3o)");
		CONVERTER.addForPosition("codigo_nacionalidade", 37).setDescription("C\u00f3digo da nacionalidade do candidato");
		CONVERTER.addForPosition("descricao_nacionalidade", 38).setDescription("Descri\u00e7\u00e3o da nacionalidade do candidato");
		CONVERTER.addForPosition("sigla_uf_nascimento", 39).setDescription("Sigla da UF de nascimento do candidato");
		CONVERTER.addForPosition("codigo_municipio_nascimento", 40).setDescription("C\u00f3digo TSE do munic\u00edpio da nascimento do candidato");
		CONVERTER.addForPosition("nome_municipio_nascimento", 41).setDescription("Nome do munic\u00edpio de nascimento do candidato");
		CONVERTER.addForPosition("despesa_max_campanha", 42).setDescription("Despesa m\u00e1xima de campanha declarada pelo partido para aquele cargo. Valores em Reais.");
		CONVERTER.addForPosition("cod_sit_tot_turno", 43).setDescription("C\u00f3digo da situa\u00e7\u00e3o de totaliza\u00e7\u00e3o do candidato naquele turno");
		CONVERTER.addForPosition("desc_sit_tot_turno", 44).setDescription("Descri\u00e7\u00e3o da situa\u00e7\u00e3o de totaliza\u00e7\u00e3o do candidato naquele turno");
		CONVERTER.addForPosition("nm_email", 45).setDescription("E-mail para comunica\u00e7\u00e3o com o candidato")
				.setConverter(new CSVBigQueryFieldConverter.LowerCaseStringConverter());

	}

	public static interface Options extends PipelineOptions {

		@Description("Bucket for temp data")
		@Default.String("dataflow-temp-data")
		String getBucket();

		void setBucket(String bucket);

		@Description("Folder for temp data")
		@Default.String("tse")
		String getFolder();

		void setFolder(String folder);

		@Description("Dataflow project")
		@Default.String("arctic-cursor-171520")
		String getProject();

		void setProject(String project);

		@Description("Bigquery dataset")
		@Default.String("tse")
		String getDataset();

		void setDataset(String project);

		@Description("Year")
		@Default.Integer(2016)
		Integer getYear();

		void setYear(Integer year);

	}

	public static void main(String[] args) {
		Options options = PipelineOptionsFactory.fromArgs(args).withValidation().as(Options.class);
		Pipeline p = Pipeline.create(options);

		Integer year = options.getYear();
		if (!year.equals(2014) && !year.equals(2016)) {
			throw new IllegalArgumentException("Years allowed: 2014 or 2016.");
		}

		String folder = options.getFolder();
		String bucket = options.getBucket();
		String tableSpec = options.getProject() + ":" + options.getDataset() + ".tse_candidatos_" + year;
		List<Acl> acls = new ArrayList<Acl>(Arrays.asList(Acl.of(User.ofAllUsers(), Role.READER)));
		FileHandler.GCSFileHandler fileHandler = new FileHandler.GCSFileHandler(bucket, acls);

		String url = URL_FMT.format(new Object[] { year.toString() });

		PCollection<String> urls = p.apply(Create.of(url).withCoder(StringUtf8Coder.of()));
		PCollection<String> zips = urls.apply(ParDo.of(new URLDownloadDoFn(folder, fileHandler)).named("DownloadZIP"));
		PCollection<String> csvs = zips.apply(ParDo.of(new ZipExtractDoFn(folder, fileHandler, ".+\\.txt$")).named("UnzipCSV"));
		PCollection<Line> lines = csvs.apply(ParDo.of(new LineReadDoFn(fileHandler, 1, "iso-8859-1")).named("ReadCSV"));
		PCollection<CSVRecordLine> csv = lines.apply(ParDo.of(new CSVParseDoFn(CSV_FORMAT)).named("ParseCSV"));
		PCollection<TableRow> rows = csv.apply(ParDo.of(new CSVRecordToTableRowDoFn(CONVERTER)).named("ConvertBigQuery"));

		rows.apply(BigQueryIO.Write.named("WriteBigQuery").to(tableSpec).withSchema(CONVERTER.getTableFieldSchema()).withWriteDisposition(WriteDisposition.WRITE_TRUNCATE)
				.withCreateDisposition(CreateDisposition.CREATE_IF_NEEDED).withoutValidation());

		p.run();
	}
}

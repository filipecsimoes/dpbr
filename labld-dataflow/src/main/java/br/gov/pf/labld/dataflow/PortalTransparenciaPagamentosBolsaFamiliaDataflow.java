package br.gov.pf.labld.dataflow;

import java.util.Locale;

import org.apache.commons.csv.CSVFormat;

import br.gov.pf.labld.dataflow.util.CSVBigQueryConverter;
import br.gov.pf.labld.dataflow.util.CSVBigQueryFieldConverter;
import br.gov.pf.labld.dataflow.util.CSVBigQueryFieldConverter.FloatConverter;

import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.options.PipelineOptionsFactory;

public class PortalTransparenciaPagamentosBolsaFamiliaDataflow extends PortalTransparenciaDataflow {

	public static final CSVFormat CSV_FORMAT = CSVFormat.EXCEL.withSkipHeaderRecord(true).withDelimiter('\t').withRecordSeparator("\r\n");

	private static final String URL_FMT = "'http://arquivos.portaldatransparencia.gov.br/downloads.asp?a='yyyy'&m='MM'&consulta=BolsaFamiliaFolhaPagamento'";

	public static final CSVBigQueryConverter CONVERTER;

	static {
		CONVERTER = new CSVBigQueryConverter();

		CSVBigQueryFieldConverter.TimestampConverter dataFieldConverter = new CSVBigQueryFieldConverter.TimestampConverter("MM/yyyy", "America/Sao_Paulo", true);
		FloatConverter floatConverter = new FloatConverter(new Locale("en", "US"), "0.##");

		CONVERTER.addForPosition("uf", 0).setDescription("UF");
		CONVERTER.addForPosition("codigo_siafi_municipio", 1).setDescription("C\u00f3digo SIAFI Munic\u00edpio");
		CONVERTER.addForPosition("nome_municipio", 2).setDescription("Nome Munic\u00edpio");
		CONVERTER.addForPosition("codigo_funcao", 3).setDescription("C\u00f3digo Fun\u00e7\u00e3o");
		CONVERTER.addForPosition("codigo_subfuncao", 4).setDescription("C\u00f3digo Subfun\u00e7\u00e3o");
		CONVERTER.addForPosition("codigo_programa", 5).setDescription("C\u00f3digo Programa");
		CONVERTER.addForPosition("codigo_acao", 6).setDescription("C\u00f3digo A\u00e7\u00e3o");
		CONVERTER.addForPosition("nis_favorecido", 7).setDescription("NIS Favorecido");
		CONVERTER.addForPosition("nome_favorecido", 8).setDescription("Nome Favorecido");
		CONVERTER.addForPosition("fonte_finalidade", 9).setDescription("Fonte-Finalidade");
		CONVERTER.addForPosition("valor_parcela", 10).setDescription("Valor Parcela").setConverter(floatConverter);
		CONVERTER.addForPosition("mes_competencia", 11).setDescription("M\u00eas Compet\u00eancia").setConverter(dataFieldConverter);
	}

	public static void main(String[] args) {
		PortalTransparenciaPagamentosBolsaFamiliaDataflow dataflow = new PortalTransparenciaPagamentosBolsaFamiliaDataflow();

		Options options = PipelineOptionsFactory.fromArgs(args).withValidation().as(Options.class);
		Pipeline p = dataflow.buildPipeline(options);

		p.run();
	}

	@Override
	public CSVFormat getCSVFormat() {
		return CSV_FORMAT;
	}

	@Override
	public CSVBigQueryConverter getConverter() {
		return CONVERTER;
	}

	@Override
	public String getFileRegex() {
		return "\\d{6}_BolsaFamiliaFolhaPagamento\\.csv$";
	}

	@Override
	public String getTablePrefix() {
		return "bolsa_familia_pagamentos";
	}

	@Override
	public String getURLFormat() {
		return URL_FMT;
	}

}

package br.gov.pf.labld.dataflow.fn;

import java.util.Iterator;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.cloud.dataflow.sdk.transforms.DoFn;

public class CSVParseDoFn extends DoFn<Line, CSVRecordLine> {

	private static final long serialVersionUID = -2603135514691251133L;

	private static final Logger LOGGER = LoggerFactory
			.getLogger(CSVParseDoFn.class);

	private final CSVFormat format;
	private boolean failSafe = false;

	public CSVParseDoFn(boolean failSafe) {
		super();
		this.failSafe = failSafe;
		this.format = CSVFormat.DEFAULT;
	}

	public CSVParseDoFn() {
		super();
		this.format = CSVFormat.DEFAULT;
	}

	public CSVParseDoFn(CSVFormat format) {
		super();
		this.format = format;
	}

	public CSVParseDoFn(CSVFormat format, boolean failSafe) {
		super();
		this.format = format;
		this.failSafe = failSafe;
	}

	@Override
	public void processElement(DoFn<Line, CSVRecordLine>.ProcessContext ctx)
			throws Exception {
		Line line = ctx.element();
		String content = line.getContent();
		try (CSVParser parser = CSVParser.parse(content, format)) {
			Iterator<CSVRecord> iterator = parser.iterator();
			boolean hasNext = false;
			try {
				hasNext = iterator.hasNext();
			} catch (Exception e) {
				if (failSafe) {
					LOGGER.info("Error parsing line " + line.getNumber() + " ("
							+ content + ") of file " + line.getFile());
				} else {
					throw new RuntimeException(
							"Error parsing line " + line.getNumber() + " ("
									+ content + ") of file " + line.getFile(),
							e);
				}
			}

			while (hasNext) {
				try {
					CSVRecord csvRecord = (CSVRecord) iterator.next();
					CSVRecordLine csvLine = new CSVRecordLine(line.getNumber(),
							line.getFile(), csvRecord);
					ctx.output(csvLine);
					hasNext = iterator.hasNext();
				} catch (Exception e) {
					if (failSafe) {
						LOGGER.info("Error parsing line " + line.getNumber()
								+ " (" + content + ") of file "
								+ line.getFile());
					} else {
						throw new RuntimeException("Error parsing line "
								+ line.getNumber() + " (" + content
								+ ") of file " + line.getFile(), e);
					}
				}
			}
		} catch (Exception e) {
			throw new RuntimeException("Error parsing line " + line.getNumber()
					+ " (" + content + ") of file " + line.getFile(), e);
		}
	}
}
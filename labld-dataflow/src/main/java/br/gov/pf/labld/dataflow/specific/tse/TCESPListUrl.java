package br.gov.pf.labld.dataflow.specific.tse;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class TCESPListUrl {

	private static final MessageFormat FMT = new MessageFormat("http://transparencia.tce.sp.gov.br/municipios-csv?ano_exercicio={0}&page={1}");

	public static void main(String[] args) throws Exception {
		File out = Files.createTempFile("tce-sp-downloads", ".txt").toFile();

		int exercicio = 1;
		int page = 0;

		Document doc = openDoc(FMT.format(new Object[] { Integer.toString(exercicio), page }));
		int lastPage = readLastPage(doc);

		try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(out)))) {
			for (page = 0; page <= lastPage; page++) {
				doc = openDoc(FMT.format(new Object[] { Integer.toString(exercicio), page }));
				List<String> links = readLinks(doc);
				for (String link : links) {
					writer.write(link);
					writer.write("\r\n");
				}
			}
		}
		System.out.println("Result at: " + out);
	}

	protected static List<String> readLinks(Document doc) {
		Elements links = doc.select("table tr td:nth-child(4) > a");
		List<String> outs = new ArrayList<String>(links.size());
		Iterator<Element> iterator = links.iterator();
		while (iterator.hasNext()) {
			Element el = iterator.next();
			outs.add(el.attr("href"));
		}
		return outs;
	}

	protected static int readLastPage(Document doc) throws URISyntaxException {
		Element lastPageEl = doc.select(".pager__item.pager__item--last > a").first();
		String lastPageUrl = lastPageEl.attr("href");
		URI uri = new URI(lastPageUrl);
		String query = uri.getQuery();
		Map<String, List<String>> params = getParams(query);
		String lastPageStr = params.get("page").get(0);
		return Integer.parseInt(lastPageStr.trim());
	}

	private static Map<String, List<String>> getParams(String query) {
		try {
			Map<String, List<String>> params = new HashMap<String, List<String>>();
			for (String param : query.split("&")) {
				String[] pair = param.split("=");
				String key = URLDecoder.decode(pair[0], "UTF-8");
				String value = "";
				if (pair.length > 1) {
					value = URLDecoder.decode(pair[1], "UTF-8");
				}

				List<String> values = params.get(key);
				if (values == null) {
					values = new ArrayList<String>();
					params.put(key, values);
				}
				values.add(value);
			}
			return params;
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}

	protected static HttpURLConnection openConnection(URL url) throws IOException {
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		System.out.println("Opening " + url);
		return conn;
	}

	protected static Document openDoc(URL url) throws IOException {
		return openDoc(url, "utf-8");
	}

	protected static Document openDoc(String url) throws IOException {
		return openDoc(new URL(url), "utf-8");
	}

	protected static Document openDoc(URL url, String enconding) throws IOException {
		HttpURLConnection conn = openConnection(url);
		conn.connect();
		Document doc;
		try (InputStream in = new BufferedInputStream(conn.getInputStream())) {
			doc = Jsoup.parse(in, enconding, url.toString());
		}
		return doc;
	}

}

package br.gov.pf.labld.dataflow;

import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.PipelineResult;
import com.google.cloud.dataflow.sdk.options.DataflowPipelineOptions;
import com.google.cloud.dataflow.sdk.options.PipelineOptionsFactory;

public abstract class LabLDDataflow<O extends DataflowPipelineOptions> {

	public abstract Class<O> getOptionType();

	public abstract Pipeline buildPipeline(O options);

	public PipelineResult run(String[] args) {
		O options = PipelineOptionsFactory.fromArgs(args).withValidation().as(getOptionType());
		Pipeline pipeline = buildPipeline(options);
		return pipeline.run();
	}

}

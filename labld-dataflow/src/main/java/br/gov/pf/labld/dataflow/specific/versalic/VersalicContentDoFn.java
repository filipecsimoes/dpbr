package br.gov.pf.labld.dataflow.specific.versalic;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.api.services.bigquery.model.TableRow;
import com.google.cloud.dataflow.sdk.transforms.DoFn;

import br.gov.pf.labld.dataflow.fn.JSONContentDoFn;
import br.gov.pf.labld.dataflow.util.JSONUtil;

public class VersalicContentDoFn extends JSONContentDoFn<String, TableRow> {

	private static final String EMBEDDED = "_embedded";
	private static final long serialVersionUID = -364130431412203734L;

	private String entityName;
	private Map<String, String> schema;

	private JSONUtil jsonUtil;

	public VersalicContentDoFn(String entityName, Map<String, String> schema) {
		super(false);
		this.entityName = entityName;
		this.schema = schema;
	}

	@Override
	public void processElement(DoFn<String, TableRow>.ProcessContext ctx) throws Exception {
		URL url = new URL(ctx.element());
		while (url != null) {
			JsonNode jsonNode = readJsonNode(url);
			process(ctx, jsonNode);
			url = getNext(jsonNode);
			// url = null;
		}
	}

	@Override
	public void process(DoFn<String, TableRow>.ProcessContext ctx, JsonNode jsonNode) throws Exception {
		JSONUtil jsonUtil = getJSONUtil();

		ObjectNode embedded = (ObjectNode) jsonNode.get(EMBEDDED);
		ArrayNode arrayNode = (ArrayNode) embedded.get(entityName);

		Iterator<JsonNode> iterator = arrayNode.elements();
		while (iterator.hasNext()) {
			ObjectNode data = (ObjectNode) iterator.next();
			TableRow tableRow = jsonUtil.toTableRow(schema, data);

			JsonNode links = data.get("_links");
			if (links != null && !(links instanceof NullNode)) {
				Iterator<Entry<String, JsonNode>> fields = links.fields();
				while (fields.hasNext()) {
					Entry<String, JsonNode> fieldEntry = fields.next();
					String key = fieldEntry.getKey();
					JsonNode linkNode = fieldEntry.getValue();
					String link = linkNode.asText();
					tableRow.set(key, link);
				}
			}

			ctx.output(tableRow);
		}
	}

	private URL getNext(JsonNode jsonNode) {
		JsonNode linksNode = jsonNode.get("_links");
		if (linksNode == null) {
			return null;
		}

		JsonNode nextNode = linksNode.get("next");
		if (nextNode == null || nextNode instanceof NullNode) {
			return null;
		}

		String url = nextNode.asText();
		try {
			return new URL(url);
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
	}

	private JSONUtil getJSONUtil() {
		if (jsonUtil == null) {
			jsonUtil = new JSONUtil("yyyy-MM-dd", "America/Sao_Paulo");
		}
		return jsonUtil;
	}

}

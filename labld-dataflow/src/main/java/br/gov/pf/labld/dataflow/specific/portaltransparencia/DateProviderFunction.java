package br.gov.pf.labld.dataflow.specific.portaltransparencia;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import com.google.cloud.dataflow.sdk.transforms.SerializableFunction;

public abstract class DateProviderFunction implements SerializableFunction<String, String> {

	private static final long serialVersionUID = -7332330636618016414L;

	protected static final Locale DEFAULT_LOCALE = new Locale("en", "US");

	private final String inputFormat;
	private final String zoneId;

	private SimpleDateFormat sdf;

	public DateProviderFunction(String inputFormat, String zoneId) {
		super();
		this.inputFormat = inputFormat;
		this.zoneId = zoneId;
	}

	public DateProviderFunction() {
		this("yyyy-MM-dd", "America/Sao_Paulo");
	}

	@Override
	public String apply(String input) {
		if (input != null) {
			return applyDate(parse(input));
		} else {
			return applyDate(null);
		}
	}

	public Calendar parse(String input) {
		try {
			Date date = getDateFormatter().parse(input);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			return calendar;
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}

	public String applyDate(Calendar input) {
		if (input != null) {
			return getDateFormatter().format(input.getTime());
		} else {
			return null;
		}
	}

	protected SimpleDateFormat getDateFormatter() {
		if (sdf == null) {
			sdf = new SimpleDateFormat(inputFormat, DEFAULT_LOCALE);
			sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.of(zoneId)));
		}
		return sdf;
	}

	public static abstract class CurrentDateProviderFunction extends DateProviderFunction {

		private static final long serialVersionUID = -3029360311848691779L;

		@Override
		public String apply(String input) {
			if (input == null) {
				return applyDate(Calendar.getInstance());
			} else {
				return super.apply(input);
			}
		}

	}

}

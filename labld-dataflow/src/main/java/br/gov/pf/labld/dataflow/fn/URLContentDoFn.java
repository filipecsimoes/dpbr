package br.gov.pf.labld.dataflow.fn;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.cloud.dataflow.sdk.options.ValueProvider;
import com.google.cloud.dataflow.sdk.transforms.DoFn;

public abstract class URLContentDoFn<V, T> extends DoFn<V, T> {

	private static final Logger LOGGER = LoggerFactory.getLogger(URLContentDoFn.class);

	private static final long serialVersionUID = 4650675710745766819L;

	private List<URL> proxies = null;
	private boolean proxiesSet = false;

	private ValueProvider<String> proxyProvider;

	public static final String CONTENT_WWW_FORM_URLENCODED = "application/x-www-form-urlencoded";
	public static final String CONTENT_APPLICATION_JSON = "application/json";

	protected HttpURLConnection openConnection(URL url) throws IOException {
		return openConnection(url, null);
	}

	protected HttpURLConnection openConnection(URL url, String payload) throws IOException {
		HttpURLConnection conn;
		if (getProxies().isEmpty()) {
			conn = (HttpURLConnection) url.openConnection();
		} else {
			Type type = Proxy.Type.valueOf(getProxy().getProtocol().toUpperCase());
			String host = getProxy().getHost();
			int port = getProxy().getPort();

			Proxy proxy = new Proxy(type, new InetSocketAddress(host, port));
			conn = (HttpURLConnection) url.openConnection(proxy);
		}
		if (conn instanceof HttpsURLConnection && ignoreCerticates()) {
			bypassCertificateVerification((HttpsURLConnection) conn);
		}
		String method = getMethod();
		if ("POST".equalsIgnoreCase(method)) {
			conn.setDoOutput(true);
			conn.setInstanceFollowRedirects(false);
			conn.setRequestMethod(method);
			if (payload == null) {
				payload = url.getQuery();
			}
			byte[] postData = payload.getBytes(Charset.forName("UTF-8"));
			int postDataLength = postData.length;
			conn.setRequestProperty("Content-Type", getContentType());
			conn.setRequestProperty("charset", "utf-8");
			conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
			conn.setRequestProperty("User-Agent", getUserAgent());
			conn.setUseCaches(false);
			try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
				wr.write(postData);
			}
		} else {
			conn.setRequestProperty("User-Agent", getUserAgent());
		}
		return conn;
	}

	private String getUserAgent() {
		return "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36";
	}

	private void bypassCertificateVerification(HttpsURLConnection conn) {
		LOGGER.warn("Bypassing certificate verification!");
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManagerImplementation() };

		// Get a new SSL context
		SSLContext sc;
		try {
			sc = SSLContext.getInstance("TLSv1.2");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
		} catch (NoSuchAlgorithmException | KeyManagementException e) {
			throw new RuntimeException(e);
		}
		// Set our connection to use this SSL context, with the "Trust all"
		// manager in place.
		conn.setSSLSocketFactory(sc.getSocketFactory());
		// Also force it to trust all hosts
		HostnameVerifier allHostsValid = new HostnameVerifier() {
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		};
		// and set the hostname verifier.
		conn.setHostnameVerifier(allHostsValid);
	}

	protected String getMethod() {
		return "GET";
	}

	protected boolean ignoreCerticates() {
		return false;
	}

	private final class X509TrustManagerImplementation implements X509TrustManager {
		public java.security.cert.X509Certificate[] getAcceptedIssuers() {
			return null;
		}

		public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
		}

		public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
		}
	}

	private URL getProxy() {
		int rand = new Random().nextInt(getProxies().size());
		return getProxies().get(rand);
	}

	public List<URL> getProxies() {
		if (!proxiesSet) {
			setProxy();
		}
		return proxies;
	}

	public void setProxy(URL... proxy) {
		this.proxies.addAll(Arrays.asList(proxy));
	}

	public synchronized void setProxy() {
		if (proxiesSet) {
			return;
		} else if (proxyProvider == null || proxyProvider.get().trim().isEmpty()) {
			proxies = Collections.emptyList();
		} else {
			String[] proxiesStr = proxyProvider.get().split(",");
			this.proxies = new ArrayList<>();
			for (String proxyStr : proxiesStr) {
				try {
					this.proxies.add(new URL(proxyStr));
				} catch (MalformedURLException e) {
					throw new RuntimeException(e);
				}
			}
		}
		LOGGER.info("Using Proxies: " + proxies.stream().map(u -> u.toExternalForm()).collect(Collectors.joining(",")));
		proxiesSet = true;
	}

	public void setProxy(ValueProvider<String> proxy) {
		this.proxyProvider = proxy;
		this.proxies = null;
	}

	public String getContentType() {
		return CONTENT_WWW_FORM_URLENCODED;
	}

}

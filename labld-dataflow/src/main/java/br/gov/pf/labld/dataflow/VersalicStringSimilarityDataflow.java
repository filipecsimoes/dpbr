package br.gov.pf.labld.dataflow;

import java.util.ArrayList;
import java.util.List;

import br.gov.pf.labld.dataflow.specific.versalic.StringSimilarityCalculator;
import br.gov.pf.labld.dataflow.specific.versalic.StringSimilarityDoFn;
import br.gov.pf.labld.dataflow.specific.versalic.StringsKey;

import com.google.api.services.bigquery.model.TableFieldSchema;
import com.google.api.services.bigquery.model.TableRow;
import com.google.api.services.bigquery.model.TableSchema;
import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.io.BigQueryIO;
import com.google.cloud.dataflow.sdk.io.BigQueryIO.Write.CreateDisposition;
import com.google.cloud.dataflow.sdk.io.BigQueryIO.Write.WriteDisposition;
import com.google.cloud.dataflow.sdk.options.DataflowPipelineOptions;
import com.google.cloud.dataflow.sdk.options.Default;
import com.google.cloud.dataflow.sdk.options.Description;
import com.google.cloud.dataflow.sdk.options.PipelineOptionsFactory;
import com.google.cloud.dataflow.sdk.transforms.GroupByKey;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.values.KV;
import com.google.cloud.dataflow.sdk.values.PCollection;

public class VersalicStringSimilarityDataflow {

	public static final TableSchema SIMILARITY_SCHEMA;

	static {
		SIMILARITY_SCHEMA = new TableSchema();

		List<TableFieldSchema> fields = new ArrayList<>();
		fields.add(new TableFieldSchema().setName("pronac1").setType("STRING"));
		fields.add(new TableFieldSchema().setName("pronac2").setType("STRING"));

		for (StringSimilarityCalculator calculator : StringSimilarityDoFn.getCalculators()) {
			fields.add(new TableFieldSchema().setName(calculator.getName()).setType("FLOAT"));
		}

		SIMILARITY_SCHEMA.setFields(fields);
	}

	public static interface Options extends DataflowPipelineOptions {

		@Description("Dataflow project")
		@Default.String("arctic-cursor-171520")
		String getProject();

		void setProject(String project);

		@Description("Bigquery dataset")
		@Default.String("versalic")
		String getDataset();

		void setDataset(String project);

	}

	public static void main(String[] args) {
		Options options = PipelineOptionsFactory.fromArgs(args).withValidation().as(Options.class);
		Pipeline pipeline = Pipeline.create(options);

		String project = options.getProject();
		String dataset = options.getDataset();

		String tableSpec = project + ":" + dataset + ".projetos_similarity";

		PCollection<TableRow> rows = pipeline.apply(BigQueryIO.Read.named("ReadBigQuery").fromQuery(QUERY));
		PCollection<KV<StringsKey, TableRow>> resultKVRows = rows.apply(ParDo.of(new StringSimilarityDoFn("pronac1", "pronac2", "resumo1", "resumo2")).named("CalcSimilarity"));
		PCollection<KV<StringsKey, Iterable<TableRow>>> groupedRows = resultKVRows.apply(GroupByKey.create());
		PCollection<TableRow> resultRows = groupedRows.apply(ParDo.of(new FirstOcurrenceDoFn()));

		resultRows.apply(BigQueryIO.Write.named("WriteBigQuery").to(tableSpec).withSchema(SIMILARITY_SCHEMA).withWriteDisposition(WriteDisposition.WRITE_TRUNCATE)
				.withCreateDisposition(CreateDisposition.CREATE_IF_NEEDED).withoutValidation());

		pipeline.run();
	}

	private static final String QUERY = "SELECT * FROM [arctic-cursor-171520:versalic.projetos_resumos]";

}

package br.gov.pf.labld.dataflow;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.csv.CSVFormat;

import br.gov.pf.labld.dataflow.fn.CSVParseDoFn;
import br.gov.pf.labld.dataflow.fn.CSVRecordLine;
import br.gov.pf.labld.dataflow.fn.CSVRecordToTableRowDoFn;
import br.gov.pf.labld.dataflow.fn.FileHandler;
import br.gov.pf.labld.dataflow.fn.Line;
import br.gov.pf.labld.dataflow.fn.LineReadDoFn;
import br.gov.pf.labld.dataflow.fn.URLDownloadDoFn;
import br.gov.pf.labld.dataflow.fn.ZipExtractDoFn;
import br.gov.pf.labld.dataflow.util.CSVBigQueryConverter;
import br.gov.pf.labld.dataflow.util.CSVBigQueryFieldConverter;

import com.google.api.services.bigquery.model.TableRow;
import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.coders.StringUtf8Coder;
import com.google.cloud.dataflow.sdk.io.BigQueryIO;
import com.google.cloud.dataflow.sdk.io.BigQueryIO.Write.CreateDisposition;
import com.google.cloud.dataflow.sdk.io.BigQueryIO.Write.WriteDisposition;
import com.google.cloud.dataflow.sdk.options.Default;
import com.google.cloud.dataflow.sdk.options.Description;
import com.google.cloud.dataflow.sdk.options.PipelineOptions;
import com.google.cloud.dataflow.sdk.options.PipelineOptionsFactory;
import com.google.cloud.dataflow.sdk.transforms.Create;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.values.PCollection;
import com.google.cloud.storage.Acl;
import com.google.cloud.storage.Acl.Role;
import com.google.cloud.storage.Acl.User;

public class TSELegendasDataflow {

	private static final MessageFormat URL_FMT = new MessageFormat("http://agencia.tse.jus.br/estatistica/sead/odsele/consulta_legendas/consulta_legendas_{0}.zip");

	public static final CSVFormat CSV_FORMAT = CSVFormat.EXCEL.withSkipHeaderRecord(true).withQuote('"').withDelimiter(';').withRecordSeparator("\r\n");
	public static final CSVBigQueryConverter CONVERTER;

	static {
		CONVERTER = new CSVBigQueryConverter();

		CONVERTER.addForPosition("ano_eleicao", 2).setDescription("Ano da elei\u00e7\u00e3o");
		CONVERTER.addForPosition("num_turno", 3).setDescription("N\u00famero do turno");
		CONVERTER.addForPosition("descricao_eleicao", 4).setDescription("Descri\u00e7\u00e3o da elei\u00e7\u00e3o");
		CONVERTER.addForPosition("sigla_uf", 5).setDescription("Sigla da Unidade da Federa\u00e7\u00e3o em que ocorreu a elei\u00e7\u00e3o");
		CONVERTER.addForPosition("sigla_ue", 6).setDescription("Sigla da Unidade Eleitoral");
		CONVERTER.addForPosition("nome_ue", 7).setDescription("Nome de Unidade Eleitoral");
		CONVERTER.addForPosition("codigo_cargo", 8).setDescription("C\u00f3digo do cargo a que o candidato concorre");
		CONVERTER.addForPosition("descricao_cargo", 9).setDescription("Descri\u00e7\u00e3o do cargo a que o candidato concorre");
		CONVERTER.addForPosition("tipo_legenda", 10).setDescription("Informa se o candidato concorre por \u201clegenda\u201d ou \u201dpartido isolado\u201d");
		CONVERTER.addForPosition("num_partido", 11).setDescription("N\u00famero do partido");
		CONVERTER.addForPosition("sigla_partido", 12).setDescription("Sigla do partido");
		CONVERTER.addForPosition("nome_partido", 13).setDescription("Nome do partido");
		CONVERTER.addForPosition("sigla_coligacao", 14).setDescription("Sigla da coliga\u00e7\u00e3o");
		CONVERTER.addForPosition("nome_coligacao", 15).setDescription("Nome da coliga\u00e7\u00e3o");
		CONVERTER.addForPosition("composicao_coligacao", 16).setDescription("Composi\u00e7\u00e3o da coliga\u00e7\u00e3o").setMode("REPEATED").setConverter(new CSVBigQueryFieldConverter.ArrayConverter("\\s\\/\\s"));
	}

	public static interface Options extends PipelineOptions {

		@Description("Bucket for temp data")
		@Default.String("dpbr-temp-files")
		String getBucket();

		void setBucket(String bucket);

		@Description("Folder for temp data")
		@Default.String("")
		String getFolder();

		void setFolder(String folder);

		@Description("BigQuery table to write to.")
		@Default.String("sispubbr:dadospublicos.tse_legendas_eleicoes_2016")
		String getTableSpec();

		void setTableSpec(String tableSpec);

		@Description("Data year (comma separated)")
		@Default.String("2016")
		String getYear();

		void setYear(String year);

	}

	public static void main(String[] args) {
		Options options = PipelineOptionsFactory.fromArgs(args).withValidation().as(Options.class);
		Pipeline p = Pipeline.create(options);

		String folder = options.getFolder();
		String bucket = options.getBucket();
		String tableSpec = options.getTableSpec();
		List<Acl> acls = new ArrayList<Acl>(Arrays.asList(Acl.of(User.ofAllUsers(), Role.READER)));
		FileHandler.GCSFileHandler fileHandler = new FileHandler.GCSFileHandler(bucket, acls);

		String[] years = options.getYear().split(",");
		List<String> eleicoes = new ArrayList<String>();
		for (String year : years) {
			eleicoes.add(URL_FMT.format(new Object[] { year }));
		}

		PCollection<String> urls = p.apply(Create.of(eleicoes).withCoder(StringUtf8Coder.of()));
		PCollection<String> zips = urls.apply(ParDo.of(new URLDownloadDoFn(folder, fileHandler)).named("DownloadZIP"));
		PCollection<String> csvs = zips.apply(ParDo.of(new ZipExtractDoFn(folder, fileHandler, ".+\\.txt$")).named("UnzipCSV"));
		PCollection<Line> lines = csvs.apply(ParDo.of(new LineReadDoFn(fileHandler, 1, "iso-8859-1")).named("ReadCSV"));
		PCollection<CSVRecordLine> csv = lines.apply(ParDo.of(new CSVParseDoFn(CSV_FORMAT)).named("ParseCSV"));
		PCollection<TableRow> rows = csv.apply(ParDo.of(new CSVRecordToTableRowDoFn(CONVERTER)).named("ConvertBigQuery"));

		rows.apply(BigQueryIO.Write.named("WriteBigQuery").to(tableSpec).withSchema(CONVERTER.getTableFieldSchema()).withWriteDisposition(WriteDisposition.WRITE_TRUNCATE)
				.withCreateDisposition(CreateDisposition.CREATE_IF_NEEDED).withoutValidation());

		p.run();

	}
}

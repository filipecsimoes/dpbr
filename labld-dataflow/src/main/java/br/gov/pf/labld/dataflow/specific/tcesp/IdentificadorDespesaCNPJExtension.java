package br.gov.pf.labld.dataflow.specific.tcesp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.gov.pf.labld.dataflow.DespesasMunicipaisEstadoSaoPauloDataFlow;
import br.gov.pf.labld.dataflow.util.CSVBigQueryFieldExtension;

/**
 * Extrai CNPJ da coluna 'identificador_despesa'.
 * 
 * @see DespesasMunicipaisEstadoSaoPauloDataFlow
 *
 */
public class IdentificadorDespesaCNPJExtension implements CSVBigQueryFieldExtension {

	private static final long serialVersionUID = 4905173474718863073L;

	private Pattern pattern;

	@Override
	public String extend(String str) {
		Matcher matcher = getPattern().matcher(str);
		if (matcher.matches()) {
			String cnpj = matcher.group(1);
			return cnpj;
		}
		return null;
	}

	private Pattern getPattern() {
		if (pattern == null) {
			pattern = Pattern.compile("CNPJ - PESSOA JUR\u00cdDICA - (\\b\\d{14}\\b)");
		}
		return pattern;
	}

}

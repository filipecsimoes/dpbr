package br.gov.pf.labld.dataflow.specific.versalic;

import info.debatty.java.stringsimilarity.interfaces.StringSimilarity;

public class StringSimilarityCalculator {

	private StringSimilarity algorithm;

	public StringSimilarityCalculator(StringSimilarity algorithm) {
		super();
		this.algorithm = algorithm;
	}

	public double similarity(String s1, String s2) {
		return algorithm.similarity(s1, s2);
	}

	public String getName() {
		return algorithm.getClass().getSimpleName().toLowerCase();
	}

}
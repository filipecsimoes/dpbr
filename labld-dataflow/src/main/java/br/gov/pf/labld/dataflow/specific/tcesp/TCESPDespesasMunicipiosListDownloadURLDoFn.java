package br.gov.pf.labld.dataflow.specific.tcesp;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.pf.labld.dataflow.fn.JSoupProcessorDoFn;

import com.google.cloud.dataflow.sdk.transforms.DoFn;

public class TCESPDespesasMunicipiosListDownloadURLDoFn extends JSoupProcessorDoFn<String, String> {

	private static final Logger LOGGER = LoggerFactory.getLogger(TCESPDespesasMunicipiosListDownloadURLDoFn.class);

	private static final long serialVersionUID = -2352178833834993898L;

	@Override
	public void process(DoFn<String, String>.ProcessContext ctx, Document doc) throws Exception {
		Elements links = doc.select("table tr td:nth-child(4) > a");
		List<String> outs = new ArrayList<String>(links.size());
		Iterator<Element> iterator = links.iterator();
		while (iterator.hasNext()) {
			Element el = iterator.next();
			outs.add(el.attr("href"));
		}
		LOGGER.info("Listing page urls {} ", ctx.element());
		for (String out : outs) {
			ctx.output(out);
		}
	}

	@Override
	protected URL getURL(DoFn<String, String>.ProcessContext ctx) throws MalformedURLException {
		return new URL(ctx.element());
	}
}

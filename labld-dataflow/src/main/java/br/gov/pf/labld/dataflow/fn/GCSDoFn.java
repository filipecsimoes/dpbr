package br.gov.pf.labld.dataflow.fn;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.gax.paging.Page;
import com.google.cloud.dataflow.sdk.transforms.DoFn;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.Storage.BlobListOption;
import com.google.cloud.storage.StorageOptions;

public class GCSDoFn extends URLContentDoFn<String, String> {

	private static final long serialVersionUID = -4769151868731051451L;

	private static final Logger LOGGER = LoggerFactory.getLogger(GCSDoFn.class);

	private Storage storage;

	private final String bucket;

	private final String prefix;

	public GCSDoFn(String bucket, String prefix) {
		super();
		this.bucket = bucket;
		this.prefix = prefix;
	}

	public Storage getStorage() {
		if (storage == null) {
			storage = StorageOptions.getDefaultInstance().getService();
		}
		return storage;
	}

	@Override
	public void processElement(DoFn<String, String>.ProcessContext ctx) throws Exception {
		List<String> outs = new ArrayList<>();

		BlobListOption prefixOpt = BlobListOption.prefix(prefix);
		Storage gcStorage = getStorage();
		Page<Blob> blobs = gcStorage.list(bucket, prefixOpt);

		Iterable<Blob> blobIterator = blobs.iterateAll();
		for (Blob blob : blobIterator) {
			String gsUrl = "gs://" + bucket + "/" + blob.getName();
			outs.add(gsUrl);
		}

		LOGGER.info("Found " + outs.size() + " files: " + outs.subList(0, Math.min(outs.size(), 2)).toString() + "...");

		for (String out : outs) {
			ctx.output(out);
		}
	}

}

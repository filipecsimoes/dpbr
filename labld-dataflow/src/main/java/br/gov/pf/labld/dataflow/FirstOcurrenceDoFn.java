package br.gov.pf.labld.dataflow;

import br.gov.pf.labld.dataflow.specific.versalic.StringsKey;

import com.google.api.services.bigquery.model.TableRow;
import com.google.cloud.dataflow.sdk.transforms.DoFn;
import com.google.cloud.dataflow.sdk.values.KV;

public class FirstOcurrenceDoFn extends DoFn<KV<StringsKey, Iterable<TableRow>>, TableRow> {

	private static final long serialVersionUID = -4264844207435444283L;

	@Override
	public void processElement(DoFn<KV<StringsKey, Iterable<TableRow>>, TableRow>.ProcessContext c) throws Exception {
		KV<StringsKey, Iterable<TableRow>> kv = c.element();
		for (TableRow row : kv.getValue()) {
			c.output(row);
			break;
		}
	}

}

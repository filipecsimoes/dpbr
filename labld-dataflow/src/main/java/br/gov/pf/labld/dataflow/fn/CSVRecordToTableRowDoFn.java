package br.gov.pf.labld.dataflow.fn;

import java.util.List;

import org.apache.commons.csv.CSVRecord;

import br.gov.pf.labld.dataflow.util.CSVBigQueryConverter;
import br.gov.pf.labld.dataflow.util.CSVBigQueryField;
import br.gov.pf.labld.dataflow.util.CSVBigQueryFieldConverter;
import br.gov.pf.labld.dataflow.util.ExtendedCSVBigQueryField;

import com.google.api.services.bigquery.model.TableRow;
import com.google.cloud.dataflow.sdk.transforms.DoFn;

public class CSVRecordToTableRowDoFn extends DoFn<CSVRecordLine, TableRow> {

	private static final long serialVersionUID = 3206974012194994818L;

	private final CSVBigQueryConverter converter;

	public CSVRecordToTableRowDoFn(CSVBigQueryConverter converter) {
		super();
		this.converter = converter;
	}

	@Override
	public void processElement(DoFn<CSVRecordLine, TableRow>.ProcessContext ctx) throws Exception {
		CSVRecordLine line = ctx.element();
		CSVRecord csv = line.getContent();
		TableRow row = new TableRow();
		for (int index = 0; index < csv.size(); index++) {
			String data = null;
			try {
				CSVBigQueryField field = converter.get(index);
				if (field != null) {
					String fieldName = field.getName();
					CSVBigQueryFieldConverter fieldConverter = field.getConverter();
					data = csv.get(index);
					Object obj = fieldConverter.convert(data);
					row.put(fieldName, obj);
				}
			} catch (Exception e) {
				throw new RuntimeException("Error parsing column " + index + " (" + data + ") at line " + line.getNumber() + " of file " + line.getFile(), e);
			}
			try {
				List<ExtendedCSVBigQueryField> extensions = converter.getExtensions(index);
				if (extensions != null) {
					for (ExtendedCSVBigQueryField extension : extensions) {
						String fieldName = extension.getName();
						CSVBigQueryFieldConverter fieldConverter = extension.getConverter();
						data = csv.get(index);
						data = extension.extend(data);
						Object obj = fieldConverter.convert(data);
						row.put(fieldName, obj);
					}
				}
			} catch (Exception e) {
				throw new RuntimeException("Error extending column " + index + " (" + data + ") at line " + line.getNumber() + " of file " + line.getFile(), e);
			}
		}
		ctx.output(row);
	}

}

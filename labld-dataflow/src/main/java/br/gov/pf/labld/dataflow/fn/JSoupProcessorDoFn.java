package br.gov.pf.labld.dataflow.fn;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.cloud.dataflow.sdk.transforms.DoFn;

public abstract class JSoupProcessorDoFn<V, T> extends URLContentDoFn<V, T> {

	private static final Logger LOGGER = LoggerFactory.getLogger(JSoupProcessorDoFn.class);

	private static final long serialVersionUID = -6933681114071139521L;

	@Override
	public void processElement(DoFn<V, T>.ProcessContext ctx) throws Exception {
		URL url = getURL(ctx);
		LOGGER.info("Processing {}.", url);
		HttpURLConnection conn = openConnection(url);
		conn.connect();
		Document doc;
		try (InputStream in = new BufferedInputStream(conn.getInputStream())) {
			doc = Jsoup.parse(in, getEncoding(), url.toString());
		}
		process(ctx, doc);
	}

	protected String getEncoding() {
		return "utf-8";
	}

	protected URL getURL(DoFn<V, T>.ProcessContext ctx) throws MalformedURLException {
		return null;
	}

	public abstract void process(DoFn<V, T>.ProcessContext ctx, Document doc) throws Exception;

}
package br.gov.pf.labld.dataflow.util;

public class ExtendedCSVBigQueryField extends CSVBigQueryField {

	private static final long serialVersionUID = -5694582548938040215L;

	private CSVBigQueryFieldExtension extension;

	public String extend(String str) {
		return extension.extend(str);
	}

	public ExtendedCSVBigQueryField setExtension(CSVBigQueryFieldExtension extension) {
		this.extension = extension;
		return this;
	}

}
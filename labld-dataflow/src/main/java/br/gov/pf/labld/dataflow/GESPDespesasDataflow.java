package br.gov.pf.labld.dataflow;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.apache.commons.csv.CSVFormat;

import br.gov.pf.labld.dataflow.GESPDespesasDataflow.Options;
import br.gov.pf.labld.dataflow.fn.CSVParseDoFn;
import br.gov.pf.labld.dataflow.fn.CSVRecordLine;
import br.gov.pf.labld.dataflow.fn.CSVRecordToTableRowDoFn;
import br.gov.pf.labld.dataflow.fn.FileHandler;
import br.gov.pf.labld.dataflow.fn.GCSDoFn;
import br.gov.pf.labld.dataflow.fn.Line;
import br.gov.pf.labld.dataflow.fn.LineReadDoFn;
import br.gov.pf.labld.dataflow.fn.RegexFix;
import br.gov.pf.labld.dataflow.fn.RegexFixDoFn;
import br.gov.pf.labld.dataflow.fn.ZipExtractDoFn;
import br.gov.pf.labld.dataflow.util.CSVBigQueryConverter;
import br.gov.pf.labld.dataflow.util.CSVBigQueryFieldConverter.FloatConverter;

import com.google.api.services.bigquery.model.TableRow;
import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.coders.StringUtf8Coder;
import com.google.cloud.dataflow.sdk.io.BigQueryIO;
import com.google.cloud.dataflow.sdk.io.BigQueryIO.Write.CreateDisposition;
import com.google.cloud.dataflow.sdk.io.BigQueryIO.Write.WriteDisposition;
import com.google.cloud.dataflow.sdk.options.DataflowPipelineOptions;
import com.google.cloud.dataflow.sdk.options.Default;
import com.google.cloud.dataflow.sdk.options.Description;
import com.google.cloud.dataflow.sdk.options.PipelineOptionsFactory;
import com.google.cloud.dataflow.sdk.transforms.Create;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.values.PCollection;
import com.google.cloud.storage.Acl;
import com.google.cloud.storage.Acl.Role;
import com.google.cloud.storage.Acl.User;

/**
 * Download data from
 * https://www.fazenda.sp.gov.br/SigeoLei131/Paginas/ConsultaDespesaAno
 * .aspx?orgao=
 *
 */
public class GESPDespesasDataflow extends LabLDDataflow<Options> {

	public static final CSVFormat CSV_FORMAT = CSVFormat.EXCEL.withSkipHeaderRecord(true).withQuote('"').withDelimiter(',').withRecordSeparator("\r\n");
	public static final CSVBigQueryConverter CONVERTER;

	static {
		PipelineOptionsFactory.register(Options.class);

		CONVERTER = new CSVBigQueryConverter();

		FloatConverter floatConverter = new FloatConverter(new Locale("pt", "BR"), "0.##");

		CONVERTER.addForPosition("ano_de_referencia", 0).setDescription("ANO DE REFERENCIA");
		CONVERTER.addForPosition("codigo_orgao", 1).setDescription("CODIGO ORGAO");
		CONVERTER.addForPosition("nome_orgao", 2).setDescription("NOME ORGAO");
		CONVERTER.addForPosition("codigo_unidade_orcamentaria", 3).setDescription("CODIGO UNIDADE ORCAMENTARIA");
		CONVERTER.addForPosition("nome_unidade_orcamentaria", 4).setDescription("NOME UNIDADE ORCAMENTARIA");
		CONVERTER.addForPosition("codigo_unidade_gestora", 5).setDescription("CODIGO UNIDADE GESTORA");
		CONVERTER.addForPosition("nome_unidade_gestora", 6).setDescription("NOME UNIDADE GESTORA");
		CONVERTER.addForPosition("codigo_categoria_de_despesa", 7).setDescription("CODIGO CATEGORIA DE DESPESA");
		CONVERTER.addForPosition("nome_categoria_de_despesa", 8).setDescription("NOME CATEGORIA DE DESPESA");
		CONVERTER.addForPosition("codigo_grupo_de_despesa", 9).setDescription("CODIGO GRUPO DE DESPESA");
		CONVERTER.addForPosition("nome_grupo_de_despesa", 10).setDescription("NOME GRUPO DE DESPESA");
		CONVERTER.addForPosition("codigo_modalidade", 11).setDescription("CODIGO MODALIDADE");
		CONVERTER.addForPosition("nome_modalidade", 12).setDescription("NOME MODALIDADE");
		CONVERTER.addForPosition("codigo_elemento_de_despesa", 13).setDescription("CODIGO ELEMENTO DE DESPESA");
		CONVERTER.addForPosition("nome_elemento_de_despesa", 14).setDescription("NOME ELEMENTO DE DESPESA");
		CONVERTER.addForPosition("codigo_item_de_despesa", 15).setDescription("CODIGO ITEM DE DESPESA");
		CONVERTER.addForPosition("nome_item_de_despesa", 16).setDescription("NOME ITEM DE DESPESA");
		CONVERTER.addForPosition("codigo_funcao", 17).setDescription("CODIGO FUNCAO");
		CONVERTER.addForPosition("nome_funcao", 18).setDescription("NOME FUNCAO");
		CONVERTER.addForPosition("codigo_subfuncao", 19).setDescription("CODIGO SUBFUNCAO");
		CONVERTER.addForPosition("nome_subfuncao", 20).setDescription("NOME SUBFUNCAO");
		CONVERTER.addForPosition("codigo_programa", 21).setDescription("CODIGO PROGRAMA");
		CONVERTER.addForPosition("nome_programa", 22).setDescription("NOME PROGRAMA");
		CONVERTER.addForPosition("codigo_programa_de_trabalho", 23).setDescription("CODIGO PROGRAMA DE TRABALHO");
		CONVERTER.addForPosition("nome_programa_de_trabalho", 24).setDescription("NOME PROGRAMA DE TRABALHO");
		CONVERTER.addForPosition("codigo_fonte_de_recursos", 25).setDescription("CODIGO FONTE DE RECURSOS");
		CONVERTER.addForPosition("nome_fonte_de_recursos", 26).setDescription("NOME FONTE DE RECURSOS");
		CONVERTER.addForPosition("numero_processo", 27).setDescription("NUMERO PROCESSO");
		CONVERTER.addForPosition("numero_nota_de_empenho", 28).setDescription("NUMERO NOTA DE EMPENHO");
		CONVERTER.addForPosition("codigo_credor", 29).setDescription("CODIGO CREDOR");
		CONVERTER.addForPosition("nome_credor", 30).setDescription("NOME CREDOR");
		CONVERTER.addForPosition("codigo_acao", 31).setDescription("CODIGO ACAO");
		CONVERTER.addForPosition("nome_acao", 32).setDescription("NOME ACAO");
		CONVERTER.addForPosition("tipo_licitacao", 33).setDescription("TIPO LICITACAO");
		CONVERTER.addForPosition("valor_empenhado", 34).setDescription("VALOR EMPENHADO").setConverter(floatConverter);
		CONVERTER.addForPosition("valor_liquidado", 35).setDescription("VALOR LIQUIDADO").setConverter(floatConverter);
		CONVERTER.addForPosition("valor_pago", 36).setDescription("VALOR PAGO").setConverter(floatConverter);
		CONVERTER.addForPosition("valor_pago_de_anos_anteriores", 37).setDescription("VALOR PAGO DE ANOS ANTERIORES").setConverter(floatConverter);

	}
	
	public static interface Options extends DataflowPipelineOptions {

		@Description("Bucket for temp data")
		@Default.String("dataflow-temp-data")
		String getBucket();

		void setBucket(String bucket);

		@Description("Folder for temp data")
		@Default.String("gesp")
		String getFolder();

		void setFolder(String folder);

		@Description("Dataflow project")
		@Default.String("arctic-cursor-171520")
		String getProject();

		void setProject(String project);

		@Description("Bigquery dataset")
		@Default.String("gesp")
		String getDataset();

		void setDataset(String project);

		@Description("Year")
		@Default.Integer(2017)
		Integer getYear();

		void setYear(Integer year);

	}

	public static void main(String[] args) {
		GESPDespesasDataflow df = new GESPDespesasDataflow();
		df.run(args);
	}

	@Override
	public Class<Options> getOptionType() {
		return Options.class;
	}

	@Override
	public Pipeline buildPipeline(Options options) {
		Pipeline p = Pipeline.create(options);

		Integer year = options.getYear();

		String folder = options.getFolder();
		String bucket = options.getBucket();
		String tableSpec = options.getProject() + ":" + options.getDataset() + ".gesp_despesas_" + year;
		List<Acl> acls = new ArrayList<Acl>(Arrays.asList(Acl.of(User.ofAllUsers(), Role.READER)));
		FileHandler.GCSFileHandler fileHandler = new FileHandler.GCSFileHandler(bucket, acls);

		PCollection<String> gcs = p.apply(Create.of("").withCoder(StringUtf8Coder.of()));
		PCollection<String> urls = gcs.apply(ParDo.of(new GCSDoFn(bucket, folder)).named("ListGCS"));
		PCollection<String> csvs = urls.apply(ParDo.of(new ZipExtractDoFn(folder, fileHandler, ".+\\.csv$")).named("UnzipCSV"));
		PCollection<Line> lines = csvs.apply(ParDo.of(new LineReadDoFn(fileHandler, 1, "iso-8859-1")).named("ReadCSV"));
		RegexFix fix = new RegexFix("\"([^0-9,\"]+),([^0-9,\"]+)\"", "\"$1 $2\"");
		RegexFix fix2 = new RegexFix("\\n", " ");
		RegexFix fix3 = new RegexFix("\"([ A-Z]+)\" *\",\"", "'$1'\",\"");
		RegexFix fix4 = new RegexFix(",\"([ \\-[0-9]\\p{L}]*?)\"([ [0-9]\\p{L}]+)\"([ \\-[0-9]\\p{L}]*?)\"", ",\"$1'$2'$3\"");
		RegexFix fix5 = new RegexFix("([^,]{1})\"\",\"", "$1'\",\"");

		PCollection<Line> fixedLines = lines.apply(ParDo.of(new RegexFixDoFn(fix, fix2, fix3, fix4, fix5)).named("FixCSV"));
		PCollection<CSVRecordLine> csv = fixedLines.apply(ParDo.of(new CSVParseDoFn(CSV_FORMAT)).named("ParseCSV"));
		PCollection<TableRow> rows = csv.apply(ParDo.of(new CSVRecordToTableRowDoFn(CONVERTER)).named("ConvertBigQuery"));

		rows.apply(BigQueryIO.Write.named("WriteBigQuery").to(tableSpec).withSchema(CONVERTER.getTableFieldSchema()).withWriteDisposition(WriteDisposition.WRITE_TRUNCATE)
				.withCreateDisposition(CreateDisposition.CREATE_IF_NEEDED).withoutValidation());

		return p;
	}

}

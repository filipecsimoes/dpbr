package br.gov.pf.labld.dataflow.specific.rab;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.pf.labld.dataflow.fn.JSoupProcessorDoFn;

import com.google.cloud.dataflow.sdk.transforms.DoFn;

public class RABListPrefixDoFn extends JSoupProcessorDoFn<String, String> {

	private static final Logger LOGGER = LoggerFactory.getLogger(RABListPrefixDoFn.class);

	private static final long serialVersionUID = 3138839377247981240L;

	private final MessageFormat fmt = new MessageFormat("https://sistemas.anac.gov.br/aeronaves/cons_rab.asp?radiobutton=p&enviar=ok&txmtc={0}");

	@Override
	public void process(DoFn<String, String>.ProcessContext ctx, Document doc) throws Exception {
		Elements prefixos = doc.select("#alter td");
		List<String> outs = new ArrayList<String>(prefixos.size());
		for (int count = 0; count < prefixos.size(); count++) {
			Element el = prefixos.get(count);
			String prefixo = el.text().trim();
			String url = fmt.format(new Object[] { prefixo });
			outs.add(url);
		}
		LOGGER.info("Found {} prefixes.", outs.size());
		for (String out : outs) {
			ctx.output(out);
		}
	}

	@Override
	protected URL getURL(DoFn<String, String>.ProcessContext ctx) throws MalformedURLException {
		URL url = new URL("https://sistemas.anac.gov.br/aeronaves/cons_rab.asp?radiobutton=t&enviar=ok");
		return url;
	}

	@Override
	protected String getMethod() {
		return "POST";
	}

	@Override
	protected boolean ignoreCerticates() {
		return true;
	}

	@Override
	protected String getEncoding() {
		return "windows-1252";
	}

}

package br.gov.pf.labld.dataflow.fn;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.csv.CSVFormat;

import com.google.api.services.bigquery.model.TableRow;
import com.google.cloud.dataflow.sdk.transforms.DoFn;

public class CSVWriteDoFn extends DoFn<TableRow, String> {

	private static final long serialVersionUID = -7790290951239844868L;

	private final CSVFormat format;

	public CSVWriteDoFn() {
		super();
		this.format = CSVFormat.DEFAULT;
	}

	public CSVWriteDoFn(String[] headers, CSVFormat format) {
		super();
		this.format = format;
	}

	@Override
	public void processElement(DoFn<TableRow, String>.ProcessContext ctx) throws Exception {
		TableRow record = ctx.element();
		List<Object> data = new ArrayList<Object>(record.size());
		Set<String> headers = record.keySet();
		for (String header : headers) {
			data.add(record.get(header));
		}
		String line = format.format(data.toArray());
		ctx.output(line);
	}

}

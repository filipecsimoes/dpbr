package br.gov.pf.labld.dataflow.fn;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.cloud.dataflow.sdk.options.ValueProvider;
import com.google.cloud.dataflow.sdk.transforms.DoFn;

public class URLDownloadDoFn extends URLContentDoFn<String, String> {

	private static final Logger LOGGER = LoggerFactory.getLogger(URLDownloadDoFn.class);

	private static final long serialVersionUID = 3768507098815916407L;

	private String folder;
	private ValueProvider<String> folderProvider;
	private final FileHandler downloader;
	private final String method;
	private boolean failSafe = false;

	public URLDownloadDoFn(ValueProvider<String> folderProvider, FileHandler downloader, boolean failSafe) {
		super();
		this.folderProvider = folderProvider;
		this.downloader = downloader;
		this.failSafe = failSafe;
		this.method = "GET";
	}

	public URLDownloadDoFn(ValueProvider<String> folderProvider, FileHandler downloader) {
		super();
		this.folderProvider = folderProvider;
		this.downloader = downloader;
		this.method = "GET";
	}

	public URLDownloadDoFn(ValueProvider<String> folderProvider, FileHandler downloader, String method) {
		super();
		this.folderProvider = folderProvider;
		this.downloader = downloader;
		this.method = method;
	}

	public URLDownloadDoFn(String folder, FileHandler downloader, boolean failSafe) {
		super();
		if (!folder.isEmpty() && !folder.endsWith("/")) {
			folder += "/";
		}
		this.folder = folder;
		this.downloader = downloader;
		this.failSafe = failSafe;
		this.method = "GET";
	}

	public URLDownloadDoFn(String folder, FileHandler downloader) {
		super();
		if (!folder.isEmpty() && !folder.endsWith("/")) {
			folder += "/";
		}
		this.folder = folder;
		this.downloader = downloader;
		this.method = "GET";
	}

	public URLDownloadDoFn(String folder, FileHandler downloader, String method) {
		super();
		if (!folder.isEmpty() && !folder.endsWith("/")) {
			folder += "/";
		}
		this.folder = folder;
		this.downloader = downloader;
		this.method = method;
	}

	@Override
	public void processElement(DoFn<String, String>.ProcessContext ctx) throws Exception {
		URL url = getURL(ctx);
		String filePath = getFolder() + getFileName(url) + "." + UUID.randomUUID().toString() + ".tmp";
		LOGGER.info("Downloading {} to {}.", url, filePath);
		HttpURLConnection conn = openConnection(url);
		conn.connect();
		int responseCode = conn.getResponseCode();
		if (responseCode >= 200 && responseCode <= 299) {
			try (InputStream in = new BufferedInputStream(conn.getInputStream())) {
				String out = downloader.write(in, filePath);
				ctx.output(out);
			} catch (IOException e) {
				if (failSafe) {
					LOGGER.info("Download of " + url + " failed.", e);
				} else {
					throw e;
				}
			}
		} else {
			String msg = "Download of " + url + " failed with code " + responseCode;
			if (failSafe) {
				LOGGER.info(msg);
			} else {
				throw new IOException(msg);
			}
		}
	}

	protected URL getURL(DoFn<String, String>.ProcessContext ctx) throws MalformedURLException {
		return new URL(ctx.element());
	}

	private String getFileName(URL url) throws URISyntaxException {
		String path = url.toURI().getPath();
		int index = path.lastIndexOf("/") + 1;
		String fileName = path.substring(index);
		return fileName;
	}

	@Override
	protected String getMethod() {
		return method;
	}

	private String getFolder() {
		setFolder();
		return folder;
	}

	private void setFolder() {
		if (folder == null) {
			folder = folderProvider.get();
			if (!folder.isEmpty() && !folder.endsWith("/")) {
				folder += "/";
			}
		}
	}

}

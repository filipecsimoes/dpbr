package br.gov.pf.labld.dataflow.specific.versalic;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

import com.google.cloud.dataflow.sdk.coders.DefaultCoder;

@DefaultCoder(StringsKeyCoder.class)
public class StringsKey {

	public Set<String> strings;

	public StringsKey() {
		super();
		this.strings = new TreeSet<>();
	}

	public StringsKey(String... strings) {
		this.strings = new TreeSet<String>(Arrays.asList(strings));
	}

	public Set<String> getStrings() {
		return strings;
	}

	public void add(String string) {
		this.strings.add(string);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((strings == null) ? 0 : strings.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StringsKey other = (StringsKey) obj;
		if (strings == null) {
			if (other.strings != null)
				return false;
		} else if (!strings.equals(other.strings))
			return false;
		return true;
	}

}

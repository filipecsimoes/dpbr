package br.gov.pf.labld.dataflow;

import java.util.ArrayList;
import java.util.List;

import br.gov.pf.labld.dataflow.specific.rab.RABDataExtractDoFn;
import br.gov.pf.labld.dataflow.specific.rab.RABListPrefixDoFn;

import com.google.api.services.bigquery.model.TableFieldSchema;
import com.google.api.services.bigquery.model.TableRow;
import com.google.api.services.bigquery.model.TableSchema;
import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.coders.StringUtf8Coder;
import com.google.cloud.dataflow.sdk.io.BigQueryIO;
import com.google.cloud.dataflow.sdk.io.BigQueryIO.Write.CreateDisposition;
import com.google.cloud.dataflow.sdk.io.BigQueryIO.Write.WriteDisposition;
import com.google.cloud.dataflow.sdk.options.Default;
import com.google.cloud.dataflow.sdk.options.Description;
import com.google.cloud.dataflow.sdk.options.PipelineOptions;
import com.google.cloud.dataflow.sdk.options.PipelineOptionsFactory;
import com.google.cloud.dataflow.sdk.transforms.Create;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.values.PCollection;

public class RegistroAeronauticoBrasileiroDataflow {

	private static final TableSchema TABLE_SCHEMA;

	static {

		List<TableFieldSchema> schemaFields = new ArrayList<>();

		schemaFields.add(new TableFieldSchema().setName("prefixo").setType("STRING").setDescription("Prefixo"));
		schemaFields.add(new TableFieldSchema().setName("proprietario").setType("STRING").setDescription("Propriet\u00e1rio"));
		schemaFields.add(new TableFieldSchema().setName("cpf_cnpj_proprietario").setType("STRING").setDescription("CPF/CNPJ Propriet\u00e1rio"));
		schemaFields.add(new TableFieldSchema().setName("operador").setType("STRING").setDescription("Operador"));
		schemaFields.add(new TableFieldSchema().setName("cpf_cnpj_operador").setType("STRING").setDescription("CPF/CNPJ Operador"));
		schemaFields.add(new TableFieldSchema().setName("fabricante").setType("STRING").setDescription("Fabricante"));
		schemaFields.add(new TableFieldSchema().setName("modelo").setType("STRING").setDescription("Modelo"));
		schemaFields.add(new TableFieldSchema().setName("numero_serie").setType("STRING").setDescription("N\u00famero de S\u00e9rie"));
		schemaFields.add(new TableFieldSchema().setName("tipo_icao").setType("STRING").setDescription("Tipo ICAO"));
		schemaFields.add(new TableFieldSchema().setName("tipo_habilitacao_pilotos").setType("STRING").setDescription("Tipo de Habilita\u00e7\u00e3o para Pilotos"));
		schemaFields.add(new TableFieldSchema().setName("classe_aeronave").setType("STRING").setDescription("Classe da Aeronave"));
		schemaFields.add(new TableFieldSchema().setName("peso_maximo_decolagem").setType("STRING").setDescription("Peso M\u00e1ximo de Decolagem"));
		schemaFields.add(new TableFieldSchema().setName("numero_maximo_passageiros").setType("STRING").setDescription("N\u00famero M\u00e1ximo de Passageiros"));
		schemaFields.add(new TableFieldSchema().setName("tipo_voo_autorizado").setType("STRING").setDescription("Tipo de voo autorizado"));
		schemaFields.add(new TableFieldSchema().setName("categoria_registro").setType("STRING").setDescription("Categoria de Registro"));
		schemaFields.add(new TableFieldSchema().setName("numero_certificados").setType("STRING").setDescription("N\u00famero dos Certificados (CM - CA)"));
		schemaFields.add(new TableFieldSchema().setName("situacao_rab").setType("STRING").setDescription("Situa\u00e7\u00e3o no RAB"));
		schemaFields.add(new TableFieldSchema().setName("data_compra_transferencia").setType("TIMESTAMP").setDescription("Data da Compra/Transfer\u00eancia"));
		schemaFields.add(new TableFieldSchema().setName("data_validade_ca").setType("TIMESTAMP").setDescription("Data de Validade do CA"));
		schemaFields.add(new TableFieldSchema().setName("data_validade_iam").setType("TIMESTAMP").setDescription("Data de Validade da IAM"));
		schemaFields.add(new TableFieldSchema().setName("situacao_aeronavegabilidade").setType("STRING").setDescription("Situa\u00e7\u00e3o de Aeronavegabilidade"));
		schemaFields.add(new TableFieldSchema().setName("motivos").setType("STRING").setDescription("Motivo(s)"));

		TABLE_SCHEMA = new TableSchema().setFields(schemaFields);

	}

	public static interface Options extends PipelineOptions {

		@Description("BigQuery table to write to.")
		@Default.String("sispubbr:dadospublicos.registro_aeronautico_brasileiro")
		String getTableSpec();

		void setTableSpec(String tableSpec);

	}

	public static void main(String[] args) {
		Options options = PipelineOptionsFactory.fromArgs(args).withValidation().as(Options.class);
		Pipeline p = Pipeline.create(options);

		String tableSpec = options.getTableSpec();

		// TODO Empty input dofn?
		PCollection<String> empty = p.apply(Create.of("").withCoder(StringUtf8Coder.of()));
		PCollection<String> prefix = empty.apply(ParDo.of(new RABListPrefixDoFn()).named("ListPrefix"));
		PCollection<TableRow> rows = prefix.apply(ParDo.of(new RABDataExtractDoFn()).named("DataExtract"));
		rows.apply(BigQueryIO.Write.named("WriteBigQuery").to(tableSpec).withSchema(TABLE_SCHEMA).withWriteDisposition(WriteDisposition.WRITE_TRUNCATE)
				.withCreateDisposition(CreateDisposition.CREATE_IF_NEEDED).withoutValidation());

		p.run();
	}

}

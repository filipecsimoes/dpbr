package br.gov.pf.labld.dataflow;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.apache.commons.csv.CSVFormat;

import br.gov.pf.labld.dataflow.fn.CSVParseDoFn;
import br.gov.pf.labld.dataflow.fn.CSVRecordLine;
import br.gov.pf.labld.dataflow.fn.CSVRecordToTableRowDoFn;
import br.gov.pf.labld.dataflow.fn.FileHandler;
import br.gov.pf.labld.dataflow.fn.Line;
import br.gov.pf.labld.dataflow.fn.LineReadDoFn;
import br.gov.pf.labld.dataflow.fn.RegexFix;
import br.gov.pf.labld.dataflow.fn.RegexFixDoFn;
import br.gov.pf.labld.dataflow.fn.URLDownloadDoFn;
import br.gov.pf.labld.dataflow.fn.ZipExtractDoFn;
import br.gov.pf.labld.dataflow.specific.tse.PrestacaoContas2014PartitionFn;
import br.gov.pf.labld.dataflow.specific.tse.PrestacaoContasPartitionFn;
import br.gov.pf.labld.dataflow.util.CSVBigQueryConverter;
import br.gov.pf.labld.dataflow.util.CSVBigQueryFieldConverter;
import br.gov.pf.labld.dataflow.util.CSVBigQueryFieldConverter.FloatConverter;

import com.google.api.services.bigquery.model.TableRow;
import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.coders.StringUtf8Coder;
import com.google.cloud.dataflow.sdk.io.BigQueryIO;
import com.google.cloud.dataflow.sdk.io.BigQueryIO.Write.CreateDisposition;
import com.google.cloud.dataflow.sdk.io.BigQueryIO.Write.WriteDisposition;
import com.google.cloud.dataflow.sdk.options.Default;
import com.google.cloud.dataflow.sdk.options.Description;
import com.google.cloud.dataflow.sdk.options.PipelineOptions;
import com.google.cloud.dataflow.sdk.options.PipelineOptionsFactory;
import com.google.cloud.dataflow.sdk.transforms.Create;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.transforms.Partition;
import com.google.cloud.dataflow.sdk.values.PCollection;
import com.google.cloud.dataflow.sdk.values.PCollectionList;
import com.google.cloud.storage.Acl;
import com.google.cloud.storage.Acl.Role;
import com.google.cloud.storage.Acl.User;

public class TSEPrestacaoContas2012Dataflow {

	private static final String URL_PRESTACAO_CONTAS_2012 = "http://agencia.tse.jus.br/estatistica/sead/odsele/prestacao_contas/prestacao_final_2012.zip";

	private static final String[] TABLES;
	private static final String[] ELEM_NAMES;

	public static final CSVFormat CSV_FORMAT = CSVFormat.EXCEL.withSkipHeaderRecord(true).withQuote('"').withDelimiter(';').withRecordSeparator("\r\n");

	public static final CSVBigQueryConverter CONVERTER_DESPESAS_CANDIDATOS;
	public static final CSVBigQueryConverter CONVERTER_DESPESAS_COMITES;
	public static final CSVBigQueryConverter CONVERTER_DESPESAS_PARTIDOS;
	public static final CSVBigQueryConverter CONVERTER_RECEITAS_CANDIDATOS;
	public static final CSVBigQueryConverter CONVERTER_RECEITAS_COMITES;
	public static final CSVBigQueryConverter CONVERTER_RECEITAS_PARTIDOS;

	public static final CSVBigQueryConverter[] CONVERTERS;

	static {
		TABLES = new String[] { "tse_despesas_candidatos_2012", "tse_despesas_comites_2012", "tse_despesas_partidos_2012", "tse_receitas_candidatos_2012",
				"tse_receitas_comites_2012", "tse_receitas_partidos_2012" };

		ELEM_NAMES = new String[] { "despesas_candidatos", "despesas_comites", "despesas_partidos", "receitas_candidatos", "receitas_comites", "receitas_partidos" };

		CSVBigQueryFieldConverter.TimestampConverter formato1DataFieldConverter = new CSVBigQueryFieldConverter.TimestampConverter("dd/MM/yyyyhh:mm:ss", "America/Sao_Paulo");
		CSVBigQueryFieldConverter.TimestampConverter formato2DataFieldConverter = new CSVBigQueryFieldConverter.TimestampConverter("dd-MMM-yy", "America/Sao_Paulo");
		CSVBigQueryFieldConverter.TimestampConverter formato3DataFieldConverter = new CSVBigQueryFieldConverter.TimestampConverter("dd/MM/yyyy", "America/Sao_Paulo");
		FloatConverter floatConverter = new FloatConverter(new Locale("pt", "BR"), "0.##");

		CONVERTER_DESPESAS_CANDIDATOS = new CSVBigQueryConverter();

		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("cod_eleicao", 0).setDescription("C\u00f3d. Elei\u00e7\u00e3o");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("desc_eleicao", 1).setDescription("Desc. Elei\u00e7\u00e3o");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("sequencial_candidato", 3).setDescription("Sequencial Candidato");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("uf", 4).setDescription("UF");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("numero_ue", 5).setDescription("N\u00famero UE");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("municipio", 6).setDescription("Munic\u00edpio");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("sigla_partido", 7).setDescription("Sigla  Partido");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("numero_candidato", 8).setDescription("N\u00famero candidato");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("cargo", 9).setDescription("Cargo");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("nome_candidato", 10).setDescription("Nome candidato");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("cpf_candidato", 11).setDescription("CPF do candidato");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("tipo_documento", 12).setDescription("Tipo do documento");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("numero_documento", 13).setDescription("N\u00famero do documento");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("cpf_cnpj_fornecedor", 14).setDescription("CPF/CNPJ do fornecedor");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("nome_fornecedor", 15).setDescription("Nome do fornecedor");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("nome_fornecedor_rfb", 16).setDescription("Nome do fornecedor (Receita Federal)");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("cod_setor_economico_doador", 17).setDescription("Cod setor econ\u00f4mico do doador");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("setor_economico_fornecedor", 18).setDescription("Setor econ\u00f4mico do fornecedor");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("data_despesa", 19).setDescription("Data da despesa").setConverter(formato3DataFieldConverter).setType("TIMESTAMP");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("valor_despesa", 20).setDescription("Valor despesa").setConverter(floatConverter).setType("FLOAT64");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("tipo_despesa", 21).setDescription("Tipo despesa");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("descricao_despesa", 22).setDescription("Descri\u00e7ao da despesa");

		CONVERTER_DESPESAS_COMITES = new CSVBigQueryConverter();

		CONVERTER_DESPESAS_COMITES.addForPosition("cod_eleicao", 0).setDescription("C\u00f3d. Elei\u00e7\u00e3o");
		CONVERTER_DESPESAS_COMITES.addForPosition("desc_eleicao", 1).setDescription("Desc. Elei\u00e7\u00e3o");
		CONVERTER_DESPESAS_COMITES.addForPosition("sequencial_comite", 3).setDescription("Sequencial Comite");
		CONVERTER_DESPESAS_COMITES.addForPosition("uf", 4).setDescription("UF");
		CONVERTER_DESPESAS_COMITES.addForPosition("numero_ue", 5).setDescription("N\u00famero UE");
		CONVERTER_DESPESAS_COMITES.addForPosition("municipio", 6).setDescription("Munic\u00edpio");
		CONVERTER_DESPESAS_COMITES.addForPosition("tipo_comite", 7).setDescription("Tipo comite");
		CONVERTER_DESPESAS_COMITES.addForPosition("sigla_partido", 8).setDescription("Sigla  Partido");
		CONVERTER_DESPESAS_COMITES.addForPosition("tipo_documento", 9).setDescription("Tipo do documento");
		CONVERTER_DESPESAS_COMITES.addForPosition("numero_documento", 10).setDescription("N\u00famero do documento");
		CONVERTER_DESPESAS_COMITES.addForPosition("cpf_cnpj_fornecedor", 11).setDescription("CPF/CNPJ do fornecedor");
		CONVERTER_DESPESAS_COMITES.addForPosition("nome_fornecedor", 12).setDescription("Nome do fornecedor");
		CONVERTER_DESPESAS_COMITES.addForPosition("nome_fornecedor_rfb", 13).setDescription("Nome do fornecedor (Receita Federal)");
		CONVERTER_DESPESAS_COMITES.addForPosition("cod_setor_economico_doador", 14).setDescription("Cod setor econ\u00f4mico do doador");
		CONVERTER_DESPESAS_COMITES.addForPosition("setor_economico_fornecedor", 15).setDescription("Setor econ\u00f4mico do fornecedor");
		CONVERTER_DESPESAS_COMITES.addForPosition("data_despesa", 16).setDescription("Data da despesa").setConverter(formato3DataFieldConverter).setType("TIMESTAMP");
		CONVERTER_DESPESAS_COMITES.addForPosition("valor_despesa", 17).setDescription("Valor despesa").setConverter(floatConverter).setType("FLOAT64");
		CONVERTER_DESPESAS_COMITES.addForPosition("tipo_despesa", 18).setDescription("Tipo despesa");
		CONVERTER_DESPESAS_COMITES.addForPosition("descricao_despesa", 19).setDescription("Descri\u00e7\u00e3o da despesa");

		CONVERTER_DESPESAS_PARTIDOS = new CSVBigQueryConverter();

		CONVERTER_DESPESAS_PARTIDOS.addForPosition("cod_eleicao", 0).setDescription("C\u00f3d. Elei\u00e7\u00e3o");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("desc_eleicao", 1).setDescription("Desc. Elei\u00e7\u00e3o");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("sequencial_diretorio", 3).setDescription("Sequencial Diretorio");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("uf", 4).setDescription("UF");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("numero_ue", 5).setDescription("N\u00famero UE");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("municipio", 6).setDescription("Munic\u00edpio");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("tipo_comite", 7).setDescription("Tipo diretorio");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("sigla_partido", 8).setDescription("Sigla  Partido");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("tipo_documento", 9).setDescription("Tipo do documento");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("numero_documento", 10).setDescription("N\u00famero do documento");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("cpf_cnpj_fornecedor", 11).setDescription("CPF/CNPJ do fornecedor");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("nome_fornecedor", 12).setDescription("Nome do fornecedor");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("nome_fornecedor_rfb", 13).setDescription("Nome do fornecedor (Receita Federal)");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("cod_setor_economico_doador", 14).setDescription("Cod setor econ\u00f4mico do doador");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("setor_economico_fornecedor", 15).setDescription("Setor econ\u00f4mico do fornecedor");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("data_despesa", 16).setDescription("Data da despesa").setConverter(formato3DataFieldConverter).setType("TIMESTAMP");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("valor_despesa", 17).setDescription("Valor despesa").setConverter(floatConverter).setType("FLOAT64");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("tipo_despesa", 18).setDescription("Tipo despesa");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("descricao_despesa", 19).setDescription("Descri\u00e7\u00e3o da despesa");

		CONVERTER_RECEITAS_CANDIDATOS = new CSVBigQueryConverter();

		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("cod_eleicao", 0).setDescription("C\u00f3d. Elei\u00e7\u00e3o");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("desc_eleicao", 1).setDescription("Desc. Elei\u00e7\u00e3o");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("sequencial_diretorio", 3).setDescription("Sequencial Candidato");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("uf", 4).setDescription("UF");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("numero_ue", 5).setDescription("Numero UE");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("municipio", 6).setDescription("Municipio");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("sigla_partido", 7).setDescription("Sigla  Partido");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("numero_candidato", 8).setDescription("Numero candidato");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("cargo", 9).setDescription("Cargo");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("nome_candidato", 10).setDescription("Nome candidato");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("cpf_candidato", 11).setDescription("CPF do candidato");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("numero_recibo_eleitoral", 12).setDescription("Numero Recibo Eleitoral");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("numero_documento", 13).setDescription("Numero do documento");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("cpf_cnpj_doador", 14).setDescription("CPF/CNPJ do doador");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("nome_doador", 15).setDescription("Nome do doador");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("nome_doador_rfb", 16).setDescription("Nome do doador (Receita Federal)");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("sigla_ue_doador", 17).setDescription("Sigla UE doador");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("numero_partido_doador", 18).setDescription("N\u00famero partido doador");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("numero_candidato_doador", 19).setDescription("N\u00famero candidato doador");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("cod_setor_economico_doador", 20).setDescription("Cod setor econ\u00f4mico do doador");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("setor_economico_doador", 21).setDescription("Setor econ\u00f4mico do doador");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("data_receita", 22).setDescription("Data da receita").setConverter(formato1DataFieldConverter).setType("TIMESTAMP");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("valor_receita", 23).setDescription("Valor receita").setConverter(floatConverter).setType("FLOAT64");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("tipo_receita", 24).setDescription("Tipo receita");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("fonte_recurso", 25).setDescription("Fonte recurso");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("especie_recurso", 26).setDescription("Especie recurso");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("descricao_receita", 27).setDescription("Descricao da receita");

		CONVERTER_RECEITAS_COMITES = new CSVBigQueryConverter();

		CONVERTER_RECEITAS_COMITES.addForPosition("cod_eleicao", 0).setDescription("C\u00f3d. Elei\u00e7\u00e3o");
		CONVERTER_RECEITAS_COMITES.addForPosition("desc_eleicao", 1).setDescription("Desc. Elei\u00e7\u00e3o");
		CONVERTER_RECEITAS_COMITES.addForPosition("sequencial_diretorio", 3).setDescription("Sequencial Comite");
		CONVERTER_RECEITAS_COMITES.addForPosition("uf", 4).setDescription("UF");
		CONVERTER_RECEITAS_COMITES.addForPosition("numero_ue", 5).setDescription("N\u00famero UE");
		CONVERTER_RECEITAS_COMITES.addForPosition("municipio", 6).setDescription("Munic\u00edpio");
		CONVERTER_RECEITAS_COMITES.addForPosition("tipo_comite", 7).setDescription("Tipo comite");
		CONVERTER_RECEITAS_COMITES.addForPosition("sigla_partido", 8).setDescription("Sigla  Partido");
		CONVERTER_RECEITAS_COMITES.addForPosition("tipo_documento", 9).setDescription("Tipo do documento");
		CONVERTER_RECEITAS_COMITES.addForPosition("numero_documento", 10).setDescription("N\u00famero do documento");
		CONVERTER_RECEITAS_COMITES.addForPosition("cpf_cnpj_doador", 11).setDescription("CPF/CNPJ do doador");
		CONVERTER_RECEITAS_COMITES.addForPosition("nome_doador", 12).setDescription("Nome do doador");
		CONVERTER_RECEITAS_COMITES.addForPosition("nome_doador_rfb", 13).setDescription("Nome do doador (Receita Federal)");
		CONVERTER_RECEITAS_COMITES.addForPosition("setor_economico_doador", 14).setDescription("Setor econ\u00f4mico do doador");
		CONVERTER_RECEITAS_COMITES.addForPosition("data_receita", 15).setDescription("Data da receita").setConverter(formato2DataFieldConverter).setType("TIMESTAMP");
		CONVERTER_RECEITAS_COMITES.addForPosition("valor_receita", 16).setDescription("Valor receita").setConverter(floatConverter).setType("FLOAT64");
		CONVERTER_RECEITAS_COMITES.addForPosition("tipo_receita", 17).setDescription("Tipo receita");
		CONVERTER_RECEITAS_COMITES.addForPosition("fonte_recurso", 18).setDescription("Fonte recurso");
		CONVERTER_RECEITAS_COMITES.addForPosition("especie_recurso", 19).setDescription("Esp\u00e9cie recurso");
		CONVERTER_RECEITAS_COMITES.addForPosition("descricao_receita", 20).setDescription("Descri\u00e7\u00e3o da receita");

		CONVERTER_RECEITAS_PARTIDOS = new CSVBigQueryConverter();

		CONVERTER_RECEITAS_PARTIDOS.addForPosition("cod_eleicao", 0).setDescription("C\u00f3d. Elei\u00e7\u00e3o");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("desc_eleicao", 1).setDescription("Desc. Elei\u00e7\u00e3o");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("sequencial_diretorio", 3).setDescription("Sequencial Diretorio");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("uf", 4).setDescription("UF");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("numero_ue", 5).setDescription("N\u00famero UE");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("municipio", 6).setDescription("Munic\u00edpio");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("tipo_diretorio", 7).setDescription("Tipo diretorio");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("sigla_partido", 8).setDescription("Sigla  Partido");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("numero_recibo_eleitoral", 9).setDescription("N\u00famero Recibo Eleitoral");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("numero_documento", 10).setDescription("N\u00famero do documento");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("cpf_cnpj_doador", 11).setDescription("CPF/CNPJ do doador");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("nome_doador", 12).setDescription("Nome do doador");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("nome_doador_rfb", 13).setDescription("Nome do doador (Receita Federal)");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("sigla_ue_doador", 14).setDescription("Sigla UE doador");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("numero_partido_doador", 15).setDescription("N\u00famero partido doador");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("numero_candidato_doador", 16).setDescription("N\u00famero candidato doador");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("cod_setor_economico_doador", 17).setDescription("Cod setor econ\u00f4mico do doador");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("setor_economico_doador", 18).setDescription("Setor econ\u00f4mico do doador");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("data_receita", 19).setDescription("Data da receita").setConverter(formato3DataFieldConverter).setType("TIMESTAMP");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("valor_receita", 20).setDescription("Valor receita").setConverter(floatConverter).setType("FLOAT64");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("tipo_receita", 21).setDescription("Tipo receita");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("fonte_recurso", 22).setDescription("Fonte recurso");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("especie_recurso", 23).setDescription("Esp\u00e9cie recurso");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("descricao_receita", 24).setDescription("Descri\u00e7\u00e3o da receita");

		CONVERTERS = new CSVBigQueryConverter[] { CONVERTER_DESPESAS_CANDIDATOS, CONVERTER_DESPESAS_COMITES, CONVERTER_DESPESAS_PARTIDOS, CONVERTER_RECEITAS_CANDIDATOS,
				CONVERTER_RECEITAS_COMITES, CONVERTER_RECEITAS_PARTIDOS };
	}

	public static interface Options extends PipelineOptions {

		@Description("Bucket for temp data")
		@Default.String("dataflow-temp-data")
		String getBucket();

		void setBucket(String bucket);

		@Description("Folder for temp data")
		@Default.String("")
		String getFolder();

		void setFolder(String folder);

		@Description("Dataflow project")
		@Default.String("arctic-cursor-171520")
		String getProject();

		void setProject(String project);

		@Description("Bigquery dataset")
		@Default.String("tse")
		String getDataset();

		void setDataset(String project);

	}

	public static void main(String[] args) {
		Options options = PipelineOptionsFactory.fromArgs(args).withValidation().as(Options.class);
		Pipeline p = Pipeline.create(options);

		String project = options.getProject();
		String dataset = options.getDataset();
		String folder = options.getFolder();
		String bucket = options.getBucket();
		List<Acl> acls = new ArrayList<Acl>(Arrays.asList(Acl.of(User.ofAllUsers(), Role.READER)));
		FileHandler.GCSFileHandler fileHandler = new FileHandler.GCSFileHandler(bucket, acls);

		PCollection<String> urls = p.apply(Create.of(URL_PRESTACAO_CONTAS_2012).withCoder(StringUtf8Coder.of()));
		PCollection<String> zips = urls.apply(ParDo.of(new URLDownloadDoFn(folder, fileHandler)).named("DownloadZIP"));
		PCollection<String> csvs = zips.apply(ParDo.of(new ZipExtractDoFn(folder, fileHandler, "[r|d].+brasil\\.txt$")).named("UnzipCSV"));
		PCollectionList<String> csvList = csvs.apply(Partition.of(6, new PrestacaoContasPartitionFn(ELEM_NAMES)));

		for (int index = 0; index < PrestacaoContas2014PartitionFn.ELEM_NAMES.length; index++) {
			String elementName = PrestacaoContas2014PartitionFn.ELEM_NAMES[index];
			csvs = csvList.get(index);
			PCollection<Line> lines = csvs.apply(ParDo.of(new LineReadDoFn(fileHandler, 1, "iso-8859-1")).named("ReadCSV_" + elementName));
			RegexFix fix = new RegexFix(";\"([ '&\\/\\.\\-[0-9]\\p{L}]*?)\"([ '\\/\\.[0-9]\\p{L}]+)\"([ '&\\/\\.\\-[0-9]\\p{L}]*?)\"", ";\"$1'$2'$3\"");
			RegexFix fix2 = new RegexFix(";\"([ '&\\-\\.[0-9]\\p{L}]*?)\"([ \\-[0-9]\\.\\p{L}]+)\"", ";\"$1'$2\"");
			RegexFix fix3 = new RegexFix("\"\";", "'\";");
			PCollection<Line> fixedLines = lines.apply(ParDo.of(new RegexFixDoFn(fix, fix2, fix3)).named("FixCSV_" + elementName));
			PCollection<CSVRecordLine> csv = fixedLines.apply(ParDo.of(new CSVParseDoFn(CSV_FORMAT)).named("ParseCSV_" + elementName));

			CSVBigQueryConverter converter = CONVERTERS[index];
			String tableSpec = project + ":" + dataset + "." + TABLES[index];

			PCollection<TableRow> rows = csv.apply(ParDo.of(new CSVRecordToTableRowDoFn(converter)).named("ConvertBigQuery_" + elementName));
			rows.apply(BigQueryIO.Write.named("WriteBigQuery_" + elementName).to(tableSpec).withSchema(converter.getTableFieldSchema())
					.withWriteDisposition(WriteDisposition.WRITE_TRUNCATE).withCreateDisposition(CreateDisposition.CREATE_IF_NEEDED).withoutValidation());
		}

		p.run();
	}

}

package br.gov.pf.labld.dataflow.util;

import java.io.Serializable;

import com.google.api.services.bigquery.model.TableFieldSchema;

public class CSVBigQueryField implements Serializable {

	private static final long serialVersionUID = -6772395914036687432L;

	private String name;
	private String mode = "NULLABLE";
	private String type = "STRING";
	private String description;
	private CSVBigQueryFieldConverter converter = CSVBigQueryFieldConverter.STRING_CONVERTER;

	public CSVBigQueryFieldConverter getConverter() {
		return converter;
	}

	public CSVBigQueryField setConverter(CSVBigQueryFieldConverter converter) {
		this.converter = converter;
		this.type = converter.getDefaultType();
		return this;
	}

	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}

	public CSVBigQueryField setName(String name) {
		this.name = name;
		return this;
	}

	public CSVBigQueryField setType(String type) {
		this.type = type;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public CSVBigQueryField setDescription(String description) {
		this.description = description;
		return this;
	}

	public String getMode() {
		return mode;
	}

	public CSVBigQueryField setMode(String mode) {
		this.mode = mode;
		return this;
	}

	public TableFieldSchema getTableFieldSchema() {
		return new TableFieldSchema().setName(name).setType(type).setMode(mode).setDescription(description);
	}

}
package br.gov.pf.labld.dataflow.util;

import java.io.Serializable;

public interface CSVBigQueryFieldExtension extends Serializable {

	String extend(String str);

}

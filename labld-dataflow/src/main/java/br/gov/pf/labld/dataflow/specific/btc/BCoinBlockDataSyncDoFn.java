package br.gov.pf.labld.dataflow.specific.btc;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;

import br.gov.pf.labld.bcoin.Block;
import br.gov.pf.labld.bcoin.Block.Builder;
import br.gov.pf.labld.dataflow.fn.JSONContentDoFn;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.cloud.dataflow.sdk.transforms.DoFn;
import com.google.cloud.dataflow.sdk.values.TupleTag;

public class BCoinBlockDataSyncDoFn extends JSONContentDoFn<Integer, Block> {

	private static final long serialVersionUID = -6798969448244261492L;

	private String bcoinUrl;

	public static final TupleTag<String> TX_HASH_TAG = new TupleTag<String>() {
		private static final long serialVersionUID = 5810466932562365490L;
	};

	public static final TupleTag<Block> BLOCK_RECORD_TAG = new TupleTag<Block>() {
		private static final long serialVersionUID = 4485976405790246774L;
	};

	public BCoinBlockDataSyncDoFn(String bcoinUrl) {
		super(false);
		this.bcoinUrl = bcoinUrl;
	}

	@Override
	public void processElement(DoFn<Integer, Block>.ProcessContext ctx) throws Exception {
		URL url = getURL(ctx);
		Integer height = ctx.element();
		JsonNode payload = getObjectMapper().readTree("{\"method\": \"getblockbyheight\",\"params\": [ \"" + height + "\", \"1\", \"0\" ]}");
		JsonNode jsonNode = readJsonNode(url, payload);
		process(ctx, jsonNode);
	}

	@Override
	public void process(DoFn<Integer, Block>.ProcessContext ctx, JsonNode jsonNode) throws Exception {
		JsonNode result = jsonNode.get("result");
		Block block = asBlock(result);
		ctx.output(block);

		Iterator<JsonNode> txs = result.get("tx").elements();
		while (txs.hasNext()) {
			JsonNode tx = txs.next();
			String txHash = tx.asText();
			ctx.sideOutput(TX_HASH_TAG, txHash);
		}
	}

	private Block asBlock(JsonNode jsonNode) {
		Builder builder = Block.newBuilder();
		builder.setHash(jsonNode.get("hash").asText());
		builder.setConfirmations(jsonNode.get("confirmations").asInt());
		builder.setStrippedsize(jsonNode.get("strippedsize").asInt());
		builder.setSize(jsonNode.get("size").asInt());
		builder.setWeight(jsonNode.get("weight").asInt());
		builder.setHeight(jsonNode.get("height").asInt());
		builder.setVersion(jsonNode.get("version").asInt());
		builder.setVersionHex(jsonNode.get("versionHex").asText());
		builder.setMerkleroot(jsonNode.get("merkleroot").asText());
		builder.setCoinbase(jsonNode.get("coinbase").asText());
		builder.setTime(jsonNode.get("time").asInt());
		builder.setMediantime(jsonNode.get("mediantime").asInt());
		builder.setBits(jsonNode.get("bits").asInt());
		builder.setDifficulty(jsonNode.get("difficulty").floatValue());
		builder.setChainwork(jsonNode.get("chainwork").asText());
		builder.setPreviousblockhash(jsonNode.get("previousblockhash").asText());
		builder.setNextblockhash(jsonNode.get("nextblockhash").asText());
		return builder.build();
	}

	@Override
	protected URL getURL(DoFn<Integer, Block>.ProcessContext ctx) throws MalformedURLException {
		return new URL(bcoinUrl);
	}

	@Override
	protected String getMethod() {
		return "POST";
	}

	@Override
	public String getContentType() {
		return CONTENT_APPLICATION_JSON;
	}

}

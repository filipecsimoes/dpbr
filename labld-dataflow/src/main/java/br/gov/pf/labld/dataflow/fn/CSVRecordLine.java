package br.gov.pf.labld.dataflow.fn;

import java.io.Serializable;

import org.apache.commons.csv.CSVRecord;

public class CSVRecordLine implements Serializable {

	private static final long serialVersionUID = 3458923291645218578L;

	private final int number;
	private final String file;
	private final CSVRecord content;

	public CSVRecordLine(int number, String file, CSVRecord content) {
		super();
		this.number = number;
		this.file = file;
		this.content = content;
	}

	public int getNumber() {
		return number;
	}

	public String getFile() {
		return file;
	}

	public CSVRecord getContent() {
		return content;
	}

}

package br.gov.pf.labld.dataflow.specific.versalic;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Set;

import org.apache.commons.compress.utils.IOUtils;

import com.google.cloud.dataflow.sdk.coders.CoderException;
import com.google.cloud.dataflow.sdk.coders.CustomCoder;
import com.google.cloud.dataflow.sdk.util.VarInt;
import com.google.cloud.dataflow.sdk.values.TypeDescriptor;

public class StringsKeyCoder extends CustomCoder<StringsKey> {

	private static final long serialVersionUID = 203862715673944452L;

	@Override
	public void verifyDeterministic() throws NonDeterministicException {
		// Yes, I am.
	}

	@Override
	public void encode(StringsKey value, OutputStream out, com.google.cloud.dataflow.sdk.coders.Coder.Context context) throws CoderException, IOException {
		Set<String> strings = value.getStrings();
		VarInt.encode(strings.size(), out);

		for (String string : strings) {
			encode(string, out);
		}
	}

	private void encode(String value, OutputStream out) throws IOException {
		byte[] bytes = value.getBytes(StandardCharsets.UTF_8);
		VarInt.encode(bytes.length, out);
		out.write(bytes);
	}

	@Override
	public StringsKey decode(InputStream inStream, com.google.cloud.dataflow.sdk.coders.Coder.Context context) throws CoderException, IOException {
		int size = VarInt.decodeInt(inStream);
		if (size < 0) {
			throw new CoderException("Invalid encoded set length: " + size);
		}
		StringsKey value = new StringsKey();
		for (int index = 0; index < size; index++) {
			String string = decodeString(inStream);
			value.add(string);
		}
		return value;
	}

	public String decodeString(InputStream inStream) throws IOException {
		int len = VarInt.decodeInt(inStream);
		if (len < 0) {
			throw new CoderException("Invalid encoded string length: " + len);
		}
		byte[] bytes = new byte[len];
		IOUtils.readFully(inStream, bytes);
		String string = new String(bytes, StandardCharsets.UTF_8);
		return string;
	}

	public static StringsKeyCoder of(TypeDescriptor<StringsKey> typeDescriptor) {
		return new StringsKeyCoder();
	}

}

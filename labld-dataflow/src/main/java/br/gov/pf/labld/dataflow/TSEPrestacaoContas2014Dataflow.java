package br.gov.pf.labld.dataflow;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.apache.commons.csv.CSVFormat;

import br.gov.pf.labld.dataflow.fn.CSVParseDoFn;
import br.gov.pf.labld.dataflow.fn.CSVRecordLine;
import br.gov.pf.labld.dataflow.fn.CSVRecordToTableRowDoFn;
import br.gov.pf.labld.dataflow.fn.FileHandler;
import br.gov.pf.labld.dataflow.fn.Line;
import br.gov.pf.labld.dataflow.fn.LineReadDoFn;
import br.gov.pf.labld.dataflow.fn.RegexFix;
import br.gov.pf.labld.dataflow.fn.RegexFixDoFn;
import br.gov.pf.labld.dataflow.fn.URLDownloadDoFn;
import br.gov.pf.labld.dataflow.fn.ZipExtractDoFn;
import br.gov.pf.labld.dataflow.specific.tse.PrestacaoContas2014PartitionFn;
import br.gov.pf.labld.dataflow.util.CSVBigQueryConverter;
import br.gov.pf.labld.dataflow.util.CSVBigQueryFieldConverter;
import br.gov.pf.labld.dataflow.util.CSVBigQueryFieldConverter.FloatConverter;

import com.google.api.services.bigquery.model.TableRow;
import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.coders.StringUtf8Coder;
import com.google.cloud.dataflow.sdk.io.BigQueryIO;
import com.google.cloud.dataflow.sdk.io.BigQueryIO.Write.CreateDisposition;
import com.google.cloud.dataflow.sdk.io.BigQueryIO.Write.WriteDisposition;
import com.google.cloud.dataflow.sdk.options.Default;
import com.google.cloud.dataflow.sdk.options.Description;
import com.google.cloud.dataflow.sdk.options.PipelineOptions;
import com.google.cloud.dataflow.sdk.options.PipelineOptionsFactory;
import com.google.cloud.dataflow.sdk.transforms.Create;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.transforms.Partition;
import com.google.cloud.dataflow.sdk.values.PCollection;
import com.google.cloud.dataflow.sdk.values.PCollectionList;
import com.google.cloud.storage.Acl;
import com.google.cloud.storage.Acl.Role;
import com.google.cloud.storage.Acl.User;

public class TSEPrestacaoContas2014Dataflow {

	private static final String URL_PRESTACAO_CONTAS_2014 = "http://agencia.tse.jus.br/estatistica/sead/odsele/prestacao_contas/prestacao_final_2014.zip";

	private static final String[] TABLES;

	public static final CSVFormat CSV_FORMAT = CSVFormat.EXCEL.withSkipHeaderRecord(true).withQuote('"').withDelimiter(';').withRecordSeparator("\r\n");

	public static final CSVBigQueryConverter CONVERTER_DESPESAS_CANDIDATOS;
	public static final CSVBigQueryConverter CONVERTER_DESPESAS_COMITES;
	public static final CSVBigQueryConverter CONVERTER_DESPESAS_PARTIDOS;
	public static final CSVBigQueryConverter CONVERTER_RECEITAS_CANDIDATOS;
	public static final CSVBigQueryConverter CONVERTER_RECEITAS_COMITES;
	public static final CSVBigQueryConverter CONVERTER_RECEITAS_PARTIDOS;

	public static final CSVBigQueryConverter[] CONVERTERS;

	static {
		TABLES = new String[] { "tse_despesas_candidatos_2014", "tse_despesas_comites_2014", "tse_despesas_partidos_2014", "tse_receitas_candidatos_2014",
				"tse_receitas_comites_2014", "tse_receitas_partidos_2014" };

		CSVBigQueryFieldConverter.TimestampConverter formato1DataFieldConverter = new CSVBigQueryFieldConverter.TimestampConverter("dd/MM/yyyyhh:mm:ss", "America/Sao_Paulo");
		CSVBigQueryFieldConverter.TimestampConverter formato2DataFieldConverter = new CSVBigQueryFieldConverter.TimestampConverter("dd-MMM-yy", "America/Sao_Paulo");
		FloatConverter floatConverter = new FloatConverter(new Locale("pt", "BR"), "0.##");

		CONVERTER_DESPESAS_CANDIDATOS = new CSVBigQueryConverter();

		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("cod_eleicao", 0).setDescription("C\u00f3d. Elei\u00e7\u00e3o");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("desc_eleicao", 1).setDescription("Desc. Elei\u00e7\u00e3o");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("cnpj_prestador_conta", 3).setDescription("CNPJ Prestador Conta");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("sequencial_candidato", 4).setDescription("Sequencial Candidato");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("uf", 5).setDescription("UF");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("sigla_partido", 6).setDescription("Sigla Partido");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("numero_candidato", 7).setDescription("N\u00famero candidato");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("cargo", 8).setDescription("Cargo");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("nome_candidato", 9).setDescription("Nome candidato");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("cpf_candidato", 10).setDescription("CPF do candidato");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("tipo_documento", 11).setDescription("Tipo do documento");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("numero_documento", 12).setDescription("N\u00famero do documento");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("cpf_cnpj_do_fornecedor", 13).setDescription("CPF/CNPJ do fornecedor");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("nome_fornecedor", 14).setDescription("Nome do fornecedor");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("nome_fornecedor_rfb", 15).setDescription("Nome do fornecedor (Receita Federal)");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("cod_setor_economico_fornecedor", 16).setDescription("Cod setor econ\u00f4mico do fornecedor");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("setor_economico_fornecedor", 17).setDescription("Setor econ\u00f4mico do fornecedor");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("data_despesa", 18).setDescription("Data da despesa").setType("TIMESTAMP").setConverter(formato1DataFieldConverter);
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("valor_despesa", 19).setDescription("Valor despesa").setConverter(floatConverter).setType("FLOAT64");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("tipo_despesa", 20).setDescription("Tipo despesa");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("descricao_despesa", 21).setDescription("Descri\u00e7ao da despesa");

		CONVERTER_DESPESAS_COMITES = new CSVBigQueryConverter();

		CONVERTER_DESPESAS_COMITES.addForPosition("cod_eleicao", 0).setDescription("C\u00f3d. Elei\u00e7\u00e3o");
		CONVERTER_DESPESAS_COMITES.addForPosition("desc_eleicao", 1).setDescription("Desc. Elei\u00e7\u00e3o");
		CONVERTER_DESPESAS_COMITES.addForPosition("data_hora", 2).setDescription("Data e hora");
		CONVERTER_DESPESAS_COMITES.addForPosition("cnpj_prestador_conta", 3).setDescription("CNPJ Prestador Conta");
		CONVERTER_DESPESAS_COMITES.addForPosition("sequencial_comite", 4).setDescription("Sequencial Comit\u00ea");
		CONVERTER_DESPESAS_COMITES.addForPosition("uf", 5).setDescription("UF");
		CONVERTER_DESPESAS_COMITES.addForPosition("tipo_comite", 6).setDescription("Tipo Comite");
		CONVERTER_DESPESAS_COMITES.addForPosition("sigla_partido", 7).setDescription("Sigla  Partido");
		CONVERTER_DESPESAS_COMITES.addForPosition("tipo_documento", 8).setDescription("Tipo do documento");
		CONVERTER_DESPESAS_COMITES.addForPosition("numero_documento", 9).setDescription("N\u00famero do documento");
		CONVERTER_DESPESAS_COMITES.addForPosition("cpf_cnpj_fornecedor", 10).setDescription("CPF/CNPJ do fornecedor");
		CONVERTER_DESPESAS_COMITES.addForPosition("nome_fornecedor", 11).setDescription("Nome do fornecedor");
		CONVERTER_DESPESAS_COMITES.addForPosition("nome_fornecedor_rfb", 12).setDescription("Nome do fornecedor (Receita Federal)");
		CONVERTER_DESPESAS_COMITES.addForPosition("cod_setor_economico_fornecedor", 13).setDescription("Cod setor econ\u00f4mico do fornecedor");
		CONVERTER_DESPESAS_COMITES.addForPosition("setor_economico_fornecedor", 14).setDescription("Setor econ\u00f4mico do fornecedor");
		CONVERTER_DESPESAS_COMITES.addForPosition("data_despesa", 15).setDescription("Data da despesa").setType("TIMESTAMP").setConverter(formato1DataFieldConverter);
		CONVERTER_DESPESAS_COMITES.addForPosition("valor_despesa", 16).setDescription("Valor despesa").setConverter(floatConverter).setType("FLOAT64");
		CONVERTER_DESPESAS_COMITES.addForPosition("tipo_despesa", 17).setDescription("Tipo despesa");
		CONVERTER_DESPESAS_COMITES.addForPosition("descricao_despesa", 18).setDescription("Descri\u00e7\u00e3o da despesa");

		CONVERTER_DESPESAS_PARTIDOS = new CSVBigQueryConverter();

		CONVERTER_DESPESAS_PARTIDOS.addForPosition("cod_eleicao", 0).setDescription("C\u00f3d. Elei\u00e7\u00e3o");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("desc_eleicao", 1).setDescription("Desc. Elei\u00e7\u00e3o");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("data_hora", 2).setDescription("Data e hora");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("cnpj_prestador_conta", 3).setDescription("CNPJ Prestador Conta");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("sequencial_diretorio", 4).setDescription("Sequencial Diretorio");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("uf", 5).setDescription("UF");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("tipo_diretorio", 6).setDescription("Tipo diretorio");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("sigla_partido", 7).setDescription("Sigla  Partido");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("tipo_documento", 8).setDescription("Tipo do documento");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("numero_documento", 9).setDescription("N\u00famero do documento");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("cpf_cnpj_fornecedor", 10).setDescription("CPF/CNPJ do fornecedor");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("nome_fornecedor", 11).setDescription("Nome do fornecedor");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("nome_fornecedor_rfb", 12).setDescription("Nome do fornecedor (Receita Federal)");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("cod_setor_economico_fornecedor", 13).setDescription("Cod setor econ\u00f4mico do fornecedor");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("setor_economico_fornecedor", 14).setDescription("Setor econ\u00f4mico do fornecedor");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("data_despesa", 15).setDescription("Data da despesa").setType("TIMESTAMP").setConverter(formato2DataFieldConverter);
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("valor_despesa", 16).setDescription("Valor despesa").setConverter(floatConverter).setType("FLOAT64");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("tipo_despesa", 17).setDescription("Tipo despesa");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("descricao_despesa", 18).setDescription("Descri\u00e7\u00e3o da despesa");

		CONVERTER_RECEITAS_CANDIDATOS = new CSVBigQueryConverter();

		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("cod_eleicao", 0).setDescription("C\u00f3d. Elei\u00e7\u00e3o");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("desc_eleicao", 1).setDescription("Desc. Elei\u00e7\u00e3o");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("data_hora", 2).setDescription("Data e hora");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("cnpj_prestador_conta", 3).setDescription("CNPJ Prestador Conta");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("sequencial_diretorio", 4).setDescription("Sequencial Candidato");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("uf", 5).setDescription("UF");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("sigla_partido", 6).setDescription("Sigla  Partido");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("numero_candidato", 7).setDescription("Numero candidato");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("cargo", 8).setDescription("Cargo");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("nome_candidato", 9).setDescription("Nome candidato");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("cpf_candidato", 10).setDescription("CPF do candidato");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("numero_recibo_eleitoral", 11).setDescription("Numero Recibo Eleitoral");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("numero_documento", 12).setDescription("Numero do documento");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("cpf_cnpj_doador", 13).setDescription("CPF/CNPJ do doador");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("nome_doador", 14).setDescription("Nome do doador");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("nome_doador_rfb", 15).setDescription("Nome do doador (Receita Federal)");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("sigla_ue_doador", 16).setDescription("Sigla UE doador");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("numero_partido_doador", 17).setDescription("N\u00famero partido doador");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("numero_candidato_doador", 18).setDescription("N\u00famero candidato doador");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("cod_setor_economico_doador", 19).setDescription("Cod setor econ\u00f4mico do doador");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("setor_economico_doador", 20).setDescription("Setor econ\u00f4mico do doador");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("data_receita", 21).setDescription("Data da receita").setType("TIMESTAMP").setConverter(formato1DataFieldConverter);
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("valor_receita", 22).setDescription("Valor receita").setConverter(floatConverter).setType("FLOAT64");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("tipo_receita", 23).setDescription("Tipo receita");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("fonte_recurso", 24).setDescription("Fonte recurso");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("especie_recurso", 25).setDescription("Especie recurso");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("descricao_receita", 26).setDescription("Descricao da receita");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("cpf_cnpj_doador_originario", 27).setDescription("CPF/CNPJ do doador origin\u00e1rio");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("nome_doador_originario", 28).setDescription("Nome do doador origin\u00e1rio");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("tipo_doador_originario", 29).setDescription("Tipo doador origin\u00e1rio");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("setor_economico_doador_originario", 30).setDescription("Setor econ\u00f4mico do doador origin\u00e1rio");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("nome_doador_originario_rfb", 31).setDescription("Nome do doador origin\u00e1rio (Receita Federal)");

		CONVERTER_RECEITAS_COMITES = new CSVBigQueryConverter();

		CONVERTER_RECEITAS_COMITES.addForPosition("cod_eleicao", 0).setDescription("C\u00f3d. Elei\u00e7\u00e3o");
		CONVERTER_RECEITAS_COMITES.addForPosition("desc_eleicao", 1).setDescription("Desc. Elei\u00e7\u00e3o");
		CONVERTER_RECEITAS_COMITES.addForPosition("data_hora", 2).setDescription("Data e hora");
		CONVERTER_RECEITAS_COMITES.addForPosition("cnpj_prestador_conta", 3).setDescription("CNPJ Prestador Conta");
		CONVERTER_RECEITAS_COMITES.addForPosition("sequencial_diretorio", 4).setDescription("Sequencial Comite");
		CONVERTER_RECEITAS_COMITES.addForPosition("uf", 5).setDescription("UF");
		CONVERTER_RECEITAS_COMITES.addForPosition("tipo_comite", 6).setDescription("Tipo Comite");
		CONVERTER_RECEITAS_COMITES.addForPosition("sigla_partido", 7).setDescription("Sigla  Partido");
		CONVERTER_RECEITAS_COMITES.addForPosition("tipo_documento", 8).setDescription("Tipo do documento");
		CONVERTER_RECEITAS_COMITES.addForPosition("numero_documento", 9).setDescription("N\u00famero do documento");
		CONVERTER_RECEITAS_COMITES.addForPosition("cpf_cnpj_doador", 10).setDescription("CPF/CNPJ do doador");
		CONVERTER_RECEITAS_COMITES.addForPosition("nome_doador", 11).setDescription("Nome do doador");
		CONVERTER_RECEITAS_COMITES.addForPosition("nome_doador_rfb", 12).setDescription("Nome do doador (Receita Federal)");
		CONVERTER_RECEITAS_COMITES.addForPosition("sigla_ue_doador", 13).setDescription("Sigla UE doador");
		CONVERTER_RECEITAS_COMITES.addForPosition("numero_partido_doador", 14).setDescription("N\u00famero partido doador");
		CONVERTER_RECEITAS_COMITES.addForPosition("numero_candidato_doador", 15).setDescription("N\u00famero candidato doador");
		CONVERTER_RECEITAS_COMITES.addForPosition("cod_setor_economico_doador", 16).setDescription("Cod setor econ\u00f4mico do doador");
		CONVERTER_RECEITAS_COMITES.addForPosition("setor_economico_doador", 17).setDescription("Setor econ\u00f4mico do doador");
		CONVERTER_RECEITAS_COMITES.addForPosition("data_receita", 18).setDescription("Data da receita").setType("TIMESTAMP").setConverter(formato1DataFieldConverter);
		CONVERTER_RECEITAS_COMITES.addForPosition("valor_receita", 19).setDescription("Valor receita").setConverter(floatConverter).setType("FLOAT64");
		CONVERTER_RECEITAS_COMITES.addForPosition("tipo_receita", 20).setDescription("Tipo receita");
		CONVERTER_RECEITAS_COMITES.addForPosition("fonte_recurso", 21).setDescription("Fonte recurso");
		CONVERTER_RECEITAS_COMITES.addForPosition("especie_recurso", 22).setDescription("Esp\u00e9cie recurso");
		CONVERTER_RECEITAS_COMITES.addForPosition("descricao_receita", 23).setDescription("Descri\u00e7\u00e3o da receita");
		CONVERTER_RECEITAS_COMITES.addForPosition("cpf_cnpj_doador_originario", 24).setDescription("CPF/CNPJ do doador origin\u00e1rio");
		CONVERTER_RECEITAS_COMITES.addForPosition("nome_doador_originario", 25).setDescription("Nome do doador origin\u00e1rio");
		CONVERTER_RECEITAS_COMITES.addForPosition("tipo_doador_originario", 26).setDescription("Tipo doador origin\u00e1rio");
		CONVERTER_RECEITAS_COMITES.addForPosition("setor_economico_doador_originario", 27).setDescription("Setor econ\u00f4mico do doador origin\u00e1rio");
		CONVERTER_RECEITAS_COMITES.addForPosition("nome_doador_originario_rfb", 28).setDescription("Nome do doador origin\u00e1rio (Receita Federal)");

		CONVERTER_RECEITAS_PARTIDOS = new CSVBigQueryConverter();

		CONVERTER_RECEITAS_PARTIDOS.addForPosition("cod_eleicao", 0).setDescription("C\u00f3d. Elei\u00e7\u00e3o");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("desc_eleicao", 1).setDescription("Desc. Elei\u00e7\u00e3o");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("data_hora", 2).setDescription("Data e hora");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("cnpj_prestador_conta", 3).setDescription("CNPJ Prestador Conta");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("sequencial_diretorio", 4).setDescription("Sequencial Diretorio");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("uf", 5).setDescription("UF");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("tipo_diretorio", 6).setDescription("Tipo diretorio");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("sigla_partido", 7).setDescription("Sigla  Partido");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("tipo_documento", 8).setDescription("Tipo do documento");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("numero_documento", 9).setDescription("N\u00famero do documento");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("cpf_cnpj_doador", 10).setDescription("CPF/CNPJ do doador");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("nome_doador", 11).setDescription("Nome do doador");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("nome_doador_rfb", 12).setDescription("Nome do doador (Receita Federal)");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("sigla_ue_doador", 13).setDescription("Sigla UE doador");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("numero_partido_doador", 14).setDescription("N\u00famero partido doador");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("numero_candidato_doador", 15).setDescription("N\u00famero candidato doador");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("cod_setor_economico_doador", 16).setDescription("Cod setor econ\u00f4mico do doador");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("setor_economico_doador", 17).setDescription("Setor econ\u00f4mico do doador");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("data_receita", 18).setDescription("Data da receita").setType("TIMESTAMP").setConverter(formato2DataFieldConverter);
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("valor_receita", 19).setDescription("Valor receita").setConverter(floatConverter).setType("FLOAT64");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("tipo_receita", 20).setDescription("Tipo receita");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("fonte_recurso", 21).setDescription("Fonte recurso");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("especie_recurso", 22).setDescription("Esp\u00e9cie recurso");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("descricao_receita", 23).setDescription("Descri\u00e7\u00e3o da receita");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("cpf_cnpj_doador_originario", 24).setDescription("CPF/CNPJ do doador origin\u00e1rio");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("nome_doador_originario", 25).setDescription("Nome do doador origin\u00e1rio");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("tipo_doador_originario", 26).setDescription("Tipo doador origin\u00e1rio");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("setor_economico_doador_originario", 27).setDescription("Setor econ\u00f4mico do doador origin\u00e1rio");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("nome_doador_originario_rfb", 28).setDescription("Nome do doador origin\u00e1rio (Receita Federal)");

		CONVERTERS = new CSVBigQueryConverter[] { CONVERTER_DESPESAS_CANDIDATOS, CONVERTER_DESPESAS_COMITES, CONVERTER_DESPESAS_PARTIDOS, CONVERTER_RECEITAS_CANDIDATOS,
				CONVERTER_RECEITAS_COMITES, CONVERTER_RECEITAS_PARTIDOS };
	}

	public static interface Options extends PipelineOptions {

		@Description("Bucket for temp data")
		@Default.String("dataflow-temp-data")
		String getBucket();

		void setBucket(String bucket);

		@Description("Folder for temp data")
		@Default.String("")
		String getFolder();

		void setFolder(String folder);

		@Description("Dataflow project")
		@Default.String("arctic-cursor-171520")
		String getProject();

		void setProject(String project);

		@Description("Bigquery dataset")
		@Default.String("tse")
		String getDataset();

		void setDataset(String project);

	}

	public static void main(String[] args) {
		Options options = PipelineOptionsFactory.fromArgs(args).withValidation().as(Options.class);
		Pipeline p = Pipeline.create(options);

		String project = options.getProject();
		String dataset = options.getDataset();
		String folder = options.getFolder();
		String bucket = options.getBucket();
		List<Acl> acls = new ArrayList<Acl>(Arrays.asList(Acl.of(User.ofAllUsers(), Role.READER)));
		FileHandler.GCSFileHandler fileHandler = new FileHandler.GCSFileHandler(bucket, acls);

		PCollection<String> urls = p.apply(Create.of(URL_PRESTACAO_CONTAS_2014).withCoder(StringUtf8Coder.of()));
		PCollection<String> zips = urls.apply(ParDo.of(new URLDownloadDoFn(folder, fileHandler)).named("DownloadZIP"));
		PCollection<String> csvs = zips.apply(ParDo.of(new ZipExtractDoFn(folder, fileHandler, ".+brasil\\.txt$")).named("UnzipCSV"));
		PCollectionList<String> csvList = csvs.apply(Partition.of(6, new PrestacaoContas2014PartitionFn()));

		for (int index = 0; index < PrestacaoContas2014PartitionFn.ELEM_NAMES.length; index++) {
			String elementName = PrestacaoContas2014PartitionFn.ELEM_NAMES[index];
			csvs = csvList.get(index);
			PCollection<Line> lines = csvs.apply(ParDo.of(new LineReadDoFn(fileHandler, 1, "iso-8859-1")).named("ReadCSV_" + elementName));
			RegexFix fix = new RegexFix(";\"([ \\-[0-9]\\p{L}]*?)\"([ [0-9]\\p{L}]+)\"([ \\-[0-9]\\p{L}]*?)\"", ";\"$1'$2'$3\"");
			PCollection<Line> fixedLines = lines.apply(ParDo.of(new RegexFixDoFn(fix)).named("FixCSV_" + elementName));
			PCollection<CSVRecordLine> csv = fixedLines.apply(ParDo.of(new CSVParseDoFn(CSV_FORMAT)).named("ParseCSV_" + elementName));

			CSVBigQueryConverter converter = CONVERTERS[index];
			String tableSpec = project + ":" + dataset + "." + TABLES[index];

			PCollection<TableRow> rows = csv.apply(ParDo.of(new CSVRecordToTableRowDoFn(converter)).named("ConvertBigQuery_" + elementName));
			rows.apply(BigQueryIO.Write.named("WriteBigQuery_" + elementName).to(tableSpec).withSchema(converter.getTableFieldSchema())
					.withWriteDisposition(WriteDisposition.WRITE_TRUNCATE).withCreateDisposition(CreateDisposition.CREATE_IF_NEEDED).withoutValidation());
		}

		p.run();
	}

}

package br.gov.pf.labld.dataflow;

import java.util.ArrayList;
import java.util.List;

import com.google.api.services.bigquery.model.TableFieldSchema;
import com.google.api.services.bigquery.model.TableRow;
import com.google.api.services.bigquery.model.TableSchema;
import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.coders.StringUtf8Coder;
import com.google.cloud.dataflow.sdk.io.BigQueryIO;
import com.google.cloud.dataflow.sdk.io.BigQueryIO.Write.CreateDisposition;
import com.google.cloud.dataflow.sdk.io.BigQueryIO.Write.WriteDisposition;
import com.google.cloud.dataflow.sdk.options.DataflowPipelineOptions;
import com.google.cloud.dataflow.sdk.options.Default;
import com.google.cloud.dataflow.sdk.options.Description;
import com.google.cloud.dataflow.sdk.options.PipelineOptionsFactory;
import com.google.cloud.dataflow.sdk.transforms.Create;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.values.PCollection;

import br.gov.pf.labld.dataflow.specific.versalic.VersalicContentDoFn;
import br.gov.pf.labld.dataflow.specific.versalic.VersalicRelatedContentDoFn;
import br.gov.pf.labld.dataflow.util.JSONUtil;

public class VersalicDataflow {

	private static final String PROJETOS_INITIAL_URL = "http://api.salic.cultura.gov.br/v1/projetos/?limit=100&format=json";
	private static final String PROPOSTAS_INITIAL_URL = "http://api.salic.cultura.gov.br/v1/propostas/?limit=100&format=json";
	private static final String INCENTIVADORES_INITIAL_URL = "http://api.salic.cultura.gov.br/v1/incentivadores/?limit=100&format=json";
	private static final String FORNECEDORES_INITIAL_URL = "http://api.salic.cultura.gov.br/v1/fornecedores/?limit=100&format=json";
	private static final String PROPONENTES_INITIAL_URL = "http://api.salic.cultura.gov.br/v1/proponentes/?limit=100&format=json";

	public static final TableSchema PROJETOS_SCHEMA;
	public static final TableSchema PROPOSTAS_SCHEMA;
	public static final TableSchema INCENTIVADORES_SCHEMA;
	public static final TableSchema FORNECEDORES_SCHEMA;
	public static final TableSchema PROPONENTES_SCHEMA;

	public static final TableSchema PROJETOS_INCENTIVADORES_SCHEMA;
	public static final TableSchema PROJETOS_PROPONENTES_SCHEMA;
	public static final TableSchema PROJETOS_FORNECEDORES_SCHEMA;

	static {
		PROJETOS_SCHEMA = new TableSchema();

		List<TableFieldSchema> fieldsProjetos = new ArrayList<>();
		fieldsProjetos.add(new TableFieldSchema().setName("etapa").setType("STRING"));
		fieldsProjetos.add(new TableFieldSchema().setName("providencia").setType("STRING"));
		fieldsProjetos.add(new TableFieldSchema().setName("area").setType("STRING"));
		fieldsProjetos.add(new TableFieldSchema().setName("enquadramento").setType("STRING"));
		fieldsProjetos.add(new TableFieldSchema().setName("objetivos").setType("STRING"));
		fieldsProjetos.add(new TableFieldSchema().setName("ficha_tecnica").setType("STRING"));
		fieldsProjetos.add(new TableFieldSchema().setName("situacao").setType("STRING"));
		fieldsProjetos.add(new TableFieldSchema().setName("outras_fontes").setType("INTEGER"));
		fieldsProjetos.add(new TableFieldSchema().setName("acessibilidade").setType("STRING"));
		fieldsProjetos.add(new TableFieldSchema().setName("sinopse").setType("STRING"));
		fieldsProjetos.add(new TableFieldSchema().setName("nome").setType("STRING"));
		fieldsProjetos.add(new TableFieldSchema().setName("cgccpf").setType("STRING"));
		fieldsProjetos.add(new TableFieldSchema().setName("mecanismo").setType("STRING"));
		fieldsProjetos.add(new TableFieldSchema().setName("segmento").setType("STRING"));
		fieldsProjetos.add(new TableFieldSchema().setName("PRONAC").setType("STRING"));
		fieldsProjetos.add(new TableFieldSchema().setName("estrategia_execucao").setType("STRING"));
		fieldsProjetos.add(new TableFieldSchema().setName("valor_aprovado").setType("FLOAT"));
		fieldsProjetos.add(new TableFieldSchema().setName("justificativa").setType("STRING"));
		fieldsProjetos.add(new TableFieldSchema().setName("resumo").setType("STRING"));
		fieldsProjetos.add(new TableFieldSchema().setName("valor_solicitado").setType("FLOAT"));
		fieldsProjetos.add(new TableFieldSchema().setName("especificacao_tecnica").setType("STRING"));
		fieldsProjetos.add(new TableFieldSchema().setName("municipio").setType("STRING"));
		fieldsProjetos.add(new TableFieldSchema().setName("data_termino").setType("TIMESTAMP"));
		fieldsProjetos.add(new TableFieldSchema().setName("UF").setType("STRING"));
		fieldsProjetos.add(new TableFieldSchema().setName("impacto_ambiental").setType("STRING"));
		fieldsProjetos.add(new TableFieldSchema().setName("democratizacao").setType("STRING"));
		fieldsProjetos.add(new TableFieldSchema().setName("valor_projeto").setType("FLOAT"));
		// fieldsProjetos.add(new
		// TableFieldSchema().setName("proponente").setType("STRING"));
		fieldsProjetos.add(new TableFieldSchema().setName("ano_projeto").setType("STRING"));
		fieldsProjetos.add(new TableFieldSchema().setName("data_inicio").setType("TIMESTAMP"));
		fieldsProjetos.add(new TableFieldSchema().setName("valor_captado").setType("FLOAT"));
		fieldsProjetos.add(new TableFieldSchema().setName("valor_proposta").setType("FLOAT"));

		fieldsProjetos.add(new TableFieldSchema().setName("self").setType("STRING"));
		fieldsProjetos.add(new TableFieldSchema().setName("fornecedores").setType("STRING"));
		fieldsProjetos.add(new TableFieldSchema().setName("incentivadores").setType("STRING"));
		fieldsProjetos.add(new TableFieldSchema().setName("proponente").setType("STRING"));

		PROJETOS_SCHEMA.setFields(fieldsProjetos);

		PROPOSTAS_SCHEMA = new TableSchema();

		List<TableFieldSchema> fieldsPropostas = new ArrayList<>();
		fieldsPropostas.add(new TableFieldSchema().setName("data_arquivamento").setType("TIMESTAMP"));
		fieldsPropostas.add(new TableFieldSchema().setName("acessibilidade").setType("STRING"));
		fieldsPropostas.add(new TableFieldSchema().setName("impacto_ambiental").setType("STRING"));
		fieldsPropostas.add(new TableFieldSchema().setName("nome").setType("STRING"));
		fieldsPropostas.add(new TableFieldSchema().setName("democratizacao").setType("STRING"));
		fieldsPropostas.add(new TableFieldSchema().setName("justificativa").setType("STRING"));
		fieldsPropostas.add(new TableFieldSchema().setName("mecanismo").setType("STRING"));
		fieldsPropostas.add(new TableFieldSchema().setName("resumo").setType("STRING"));
		fieldsPropostas.add(new TableFieldSchema().setName("sinopse").setType("STRING"));
		fieldsPropostas.add(new TableFieldSchema().setName("especificacao_tecnica").setType("STRING"));
		fieldsPropostas.add(new TableFieldSchema().setName("data_inicio").setType("TIMESTAMP"));
		fieldsPropostas.add(new TableFieldSchema().setName("objetivos").setType("STRING"));
		fieldsPropostas.add(new TableFieldSchema().setName("ficha_tecnica").setType("STRING"));
		fieldsPropostas.add(new TableFieldSchema().setName("etapa").setType("STRING"));
		fieldsPropostas.add(new TableFieldSchema().setName("data_aceite").setType("STRING"));
		fieldsPropostas.add(new TableFieldSchema().setName("id").setType("STRING"));
		fieldsPropostas.add(new TableFieldSchema().setName("estrategia_execucao").setType("STRING"));
		fieldsPropostas.add(new TableFieldSchema().setName("data_termino").setType("TIMESTAMP"));

		fieldsPropostas.add(new TableFieldSchema().setName("self").setType("STRING"));

		PROPOSTAS_SCHEMA.setFields(fieldsPropostas);

		INCENTIVADORES_SCHEMA = new TableSchema();

		List<TableFieldSchema> fieldsIncentivadores = new ArrayList<>();

		fieldsIncentivadores.add(new TableFieldSchema().setName("nome").setType("STRING"));
		fieldsIncentivadores.add(new TableFieldSchema().setName("cgccpf").setType("STRING"));
		fieldsIncentivadores.add(new TableFieldSchema().setName("total_doado").setType("FLOAT"));
		fieldsIncentivadores.add(new TableFieldSchema().setName("tipo_pessoa").setType("STRING"));
		fieldsIncentivadores.add(new TableFieldSchema().setName("responsavel").setType("STRING"));
		fieldsIncentivadores.add(new TableFieldSchema().setName("UF").setType("STRING"));
		fieldsIncentivadores.add(new TableFieldSchema().setName("municipio").setType("STRING"));

		fieldsIncentivadores.add(new TableFieldSchema().setName("self").setType("STRING"));
		fieldsIncentivadores.add(new TableFieldSchema().setName("doacoes").setType("STRING"));

		INCENTIVADORES_SCHEMA.setFields(fieldsIncentivadores);

		FORNECEDORES_SCHEMA = new TableSchema();

		List<TableFieldSchema> fieldsFornecedores = new ArrayList<>();

		fieldsFornecedores.add(new TableFieldSchema().setName("cgccpf").setType("STRING"));
		fieldsFornecedores.add(new TableFieldSchema().setName("email").setType("STRING"));
		fieldsFornecedores.add(new TableFieldSchema().setName("nome").setType("STRING"));

		fieldsFornecedores.add(new TableFieldSchema().setName("self").setType("STRING"));
		fieldsFornecedores.add(new TableFieldSchema().setName("produtos").setType("STRING"));

		FORNECEDORES_SCHEMA.setFields(fieldsFornecedores);

		PROPONENTES_SCHEMA = new TableSchema();

		List<TableFieldSchema> fieldsProponentes = new ArrayList<>();

		fieldsProponentes.add(new TableFieldSchema().setName("nome").setType("STRING"));
		fieldsProponentes.add(new TableFieldSchema().setName("cgccpf").setType("STRING"));
		fieldsProponentes.add(new TableFieldSchema().setName("tipo_pessoa").setType("STRING"));
		fieldsProponentes.add(new TableFieldSchema().setName("responsavel").setType("STRING"));
		fieldsProponentes.add(new TableFieldSchema().setName("UF").setType("STRING"));
		fieldsProponentes.add(new TableFieldSchema().setName("total_captado").setType("FLOAT"));
		fieldsProponentes.add(new TableFieldSchema().setName("municipio").setType("STRING"));

		fieldsProponentes.add(new TableFieldSchema().setName("self").setType("STRING"));
		fieldsProponentes.add(new TableFieldSchema().setName("projetos").setType("STRING"));

		PROPONENTES_SCHEMA.setFields(fieldsProponentes);

		PROJETOS_INCENTIVADORES_SCHEMA = new TableSchema();

		List<TableFieldSchema> fieldsProjetosIncentivadores = new ArrayList<>();

		fieldsProjetosIncentivadores.add(new TableFieldSchema().setName("PRONAC").setType("STRING"));
		fieldsProjetosIncentivadores.add(new TableFieldSchema().setName("nome").setType("STRING"));
		fieldsProjetosIncentivadores.add(new TableFieldSchema().setName("cgccpf").setType("STRING"));
		fieldsProjetosIncentivadores.add(new TableFieldSchema().setName("total_doado").setType("FLOAT"));
		fieldsProjetosIncentivadores.add(new TableFieldSchema().setName("tipo_pessoa").setType("STRING"));
		fieldsProjetosIncentivadores.add(new TableFieldSchema().setName("responsavel").setType("STRING"));
		fieldsProjetosIncentivadores.add(new TableFieldSchema().setName("UF").setType("STRING"));
		fieldsProjetosIncentivadores.add(new TableFieldSchema().setName("municipio").setType("STRING"));

		PROJETOS_INCENTIVADORES_SCHEMA.setFields(fieldsProjetosIncentivadores);

		PROJETOS_PROPONENTES_SCHEMA = new TableSchema();

		List<TableFieldSchema> fieldsProjetosProponentes = new ArrayList<>();
		fieldsProjetosProponentes.add(new TableFieldSchema().setName("PRONAC").setType("STRING"));
		fieldsProjetosProponentes.add(new TableFieldSchema().setName("nome").setType("STRING"));
		fieldsProjetosProponentes.add(new TableFieldSchema().setName("cgccpf").setType("STRING"));
		fieldsProjetosProponentes.add(new TableFieldSchema().setName("tipo_pessoa").setType("STRING"));
		fieldsProjetosProponentes.add(new TableFieldSchema().setName("responsavel").setType("STRING"));
		fieldsProjetosProponentes.add(new TableFieldSchema().setName("UF").setType("STRING"));
		fieldsProjetosProponentes.add(new TableFieldSchema().setName("municipio").setType("STRING"));

		PROJETOS_PROPONENTES_SCHEMA.setFields(fieldsProjetosProponentes);

		PROJETOS_FORNECEDORES_SCHEMA = new TableSchema();

		List<TableFieldSchema> fieldsProjetosFornecedores = new ArrayList<>();
		fieldsProjetosFornecedores.add(new TableFieldSchema().setName("PRONAC").setType("STRING"));
		fieldsProjetosFornecedores.add(new TableFieldSchema().setName("cgccpf").setType("STRING"));
		fieldsProjetosFornecedores.add(new TableFieldSchema().setName("email").setType("STRING"));
		fieldsProjetosFornecedores.add(new TableFieldSchema().setName("nome").setType("STRING"));

		PROJETOS_FORNECEDORES_SCHEMA.setFields(fieldsProjetosFornecedores);

	}

	public static interface Options extends DataflowPipelineOptions {

		@Description("Dataflow project")
		@Default.String("arctic-cursor-171520")
		String getProject();

		void setProject(String project);

		@Description("Bigquery dataset")
		@Default.String("versalic")
		String getDataset();

		void setDataset(String project);

	}

	public static void main(String[] args) {
		Options options = PipelineOptionsFactory.fromArgs(args).withValidation().as(Options.class);
		Pipeline pipeline = Pipeline.create(options);

		String project = options.getProject();
		String dataset = options.getDataset();

		String projetosTableSpec = project + ":" + dataset + ".projetos";
		String propostasTableSpec = project + ":" + dataset + ".propostas";
		String incentivadoresTableSpec = project + ":" + dataset + ".incentivadores";
		String fornecedoresTableSpec = project + ":" + dataset + ".fornecedores";
		String proponentesTableSpec = project + ":" + dataset + ".proponentes";
		String projetosIncentivadoresTableSpec = project + ":" + dataset + ".projetos_incentivadores";
		String projetosFornecedoresTableSpec = project + ":" + dataset + ".projetos_fornecedores";
		String projetosProponentesTableSpec = project + ":" + dataset + ".projetos_proponentes";

		PCollection<TableRow> projetosRows = buildPipeline(pipeline, PROJETOS_INITIAL_URL, "projetos", PROJETOS_SCHEMA,
				projetosTableSpec);
		buildPipeline(pipeline, PROPOSTAS_INITIAL_URL, "propostas", PROPOSTAS_SCHEMA, propostasTableSpec);
		buildPipeline(pipeline, INCENTIVADORES_INITIAL_URL, "incentivadores", INCENTIVADORES_SCHEMA,
				incentivadoresTableSpec);
		buildPipeline(pipeline, FORNECEDORES_INITIAL_URL, "fornecedores", FORNECEDORES_SCHEMA, fornecedoresTableSpec);
		buildPipeline(pipeline, PROPONENTES_INITIAL_URL, "proponentes", PROPONENTES_SCHEMA, proponentesTableSpec);

		// Projetos-Incentivadores
		PCollection<TableRow> rowsProjetosRelIncentivadores = projetosRows
				.apply(ParDo.of(new VersalicRelatedContentDoFn(JSONUtil.schemaAsMap(PROJETOS_INCENTIVADORES_SCHEMA),
						"PRONAC", "incentivadores")).named("ParseProjetosIncentivadores"));

		rowsProjetosRelIncentivadores.apply(BigQueryIO.Write.named("WriteBigQueryProjetosIncentivadores")
				.to(projetosIncentivadoresTableSpec).withSchema(PROJETOS_INCENTIVADORES_SCHEMA)
				.withWriteDisposition(WriteDisposition.WRITE_TRUNCATE)
				.withCreateDisposition(CreateDisposition.CREATE_IF_NEEDED).withoutValidation());

		// Projetos-Fornecedores
		PCollection<TableRow> rowsProjetosRelFornecedores = projetosRows
				.apply(ParDo.of(new VersalicRelatedContentDoFn(JSONUtil.schemaAsMap(PROJETOS_FORNECEDORES_SCHEMA),
						"PRONAC", "fornecedores")).named("ParseProjetosFornecedores"));

		rowsProjetosRelFornecedores
				.apply(BigQueryIO.Write.named("WriteBigQueryProjetosFornecedores").to(projetosFornecedoresTableSpec)
						.withSchema(PROJETOS_FORNECEDORES_SCHEMA).withWriteDisposition(WriteDisposition.WRITE_TRUNCATE)
						.withCreateDisposition(CreateDisposition.CREATE_IF_NEEDED).withoutValidation());

		// Projetos-Proponente
		PCollection<TableRow> rowsProjetosRelProponente = projetosRows
				.apply(ParDo.of(new VersalicRelatedContentDoFn(JSONUtil.schemaAsMap(PROJETOS_PROPONENTES_SCHEMA),
						"PRONAC", "proponente")).named("ParseProjetosProponentes"));

		rowsProjetosRelProponente
				.apply(BigQueryIO.Write.named("WriteBigQueryProjetosProponentes").to(projetosProponentesTableSpec)
						.withSchema(PROJETOS_PROPONENTES_SCHEMA).withWriteDisposition(WriteDisposition.WRITE_TRUNCATE)
						.withCreateDisposition(CreateDisposition.CREATE_IF_NEEDED).withoutValidation());

		pipeline.run();
	}

	private static PCollection<TableRow> buildPipeline(Pipeline pipeline, String url, String entity, TableSchema schema,
			String projetosTableSpec) {
		PCollection<String> urls = pipeline.apply(Create.of(url).withCoder(StringUtf8Coder.of()));
		PCollection<TableRow> rows = urls
				.apply(ParDo.of(new VersalicContentDoFn(entity, JSONUtil.schemaAsMap(schema))).named("Parse" + entity));
		rows.apply(BigQueryIO.Write.named("WriteBigQuery" + entity).to(projetosTableSpec).withSchema(schema)
				.withWriteDisposition(WriteDisposition.WRITE_TRUNCATE)
				.withCreateDisposition(CreateDisposition.CREATE_IF_NEEDED).withoutValidation());

		return rows;
	}
}

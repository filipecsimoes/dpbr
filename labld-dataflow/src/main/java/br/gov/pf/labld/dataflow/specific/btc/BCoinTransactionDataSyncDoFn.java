package br.gov.pf.labld.dataflow.specific.btc;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;

import br.gov.pf.labld.bcoin.Transaction;
import br.gov.pf.labld.bcoin.TransactionInput;
import br.gov.pf.labld.bcoin.TransactionOutput;
import br.gov.pf.labld.dataflow.fn.JSONContentDoFn;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.google.cloud.dataflow.sdk.transforms.DoFn;
import com.google.cloud.dataflow.sdk.values.TupleTag;

public class BCoinTransactionDataSyncDoFn extends JSONContentDoFn<String, Transaction> {

	private static final long serialVersionUID = 7217654761457784585L;

	private String bcoinUrl;

	public static final TupleTag<Transaction> TX_DATA_RECORD_TAG = new TupleTag<Transaction>() {
		private static final long serialVersionUID = -4475128871093848545L;
	};

	public static final TupleTag<TransactionInput> TX_INPUT_RECORD_TAG = new TupleTag<TransactionInput>() {
		private static final long serialVersionUID = -4475128871093848545L;
	};

	public static final TupleTag<TransactionOutput> TX_OUTPUT_RECORD_TAG = new TupleTag<TransactionOutput>() {
		private static final long serialVersionUID = -4856609934050739998L;
	};

	public BCoinTransactionDataSyncDoFn(String bcoinUrl) {
		super(true);
		this.bcoinUrl = bcoinUrl;
	}

	@Override
	public void processElement(DoFn<String, Transaction>.ProcessContext ctx) throws Exception {
		URL url = getURL(ctx);
		JsonNode jsonNode = readJsonNode(url);
		if (jsonNode != null) {
			process(ctx, jsonNode);
		} else {
			String txHash = ctx.element();
			ctx.output(asTransaction(txHash));
		}
	}

	@Override
	public void process(DoFn<String, Transaction>.ProcessContext ctx, JsonNode jsonNode) throws Exception {
		String txHash = ctx.element();

		Transaction transaction = asTransaction(jsonNode);
		ctx.output(transaction);

		Iterator<JsonNode> inputs = jsonNode.get("inputs").elements();
		while (inputs.hasNext()) {
			JsonNode input = inputs.next();
			TransactionInput txInput = asTransactionInput(input, txHash);
			ctx.sideOutput(TX_INPUT_RECORD_TAG, txInput);
		}

		Iterator<JsonNode> outputs = jsonNode.get("outputs").elements();
		while (outputs.hasNext()) {
			JsonNode output = outputs.next();
			TransactionOutput txOutput = asTransactionOutput(output, txHash);
			ctx.sideOutput(TX_OUTPUT_RECORD_TAG, txOutput);
		}
	}

	private TransactionOutput asTransactionOutput(JsonNode output, String txHash) {
		TransactionOutput.Builder builder = TransactionOutput.newBuilder();

		builder.setTxHash(txHash);
		builder.setValue(output.get("value").asInt());
		builder.setScript(output.get("script").asText());
		builder.setAddress(output.get("address").asText());

		return builder.build();
	}

	private TransactionInput asTransactionInput(JsonNode input, String txHash) {
		TransactionInput.Builder builder = TransactionInput.newBuilder();

		builder.setTxHash(txHash);

		builder.setScript(input.get("script").asText());
		builder.setWitness(input.get("witness").asText());
		builder.setSequence(input.get("sequence").asInt());

		JsonNode coin = input.get("coin");

		if (coin != null && !(coin instanceof NullNode)) {
			builder.setVersion(coin.get("version").asText());
			builder.setHeight(coin.get("height").asInt());
			builder.setValue(coin.get("value").asInt());
			builder.setAddress(coin.get("address").asText());
			builder.setCoinbase(coin.get("coinbase").asBoolean());
		} else {
			builder.setVersion(null);
			builder.setHeight(null);
			builder.setValue(null);
			builder.setAddress(null);
			builder.setCoinbase(null);
		}

		JsonNode prevout = input.get("prevout");
		builder.setPrevoutHash(prevout.get("hash").asText());
		builder.setPrevoutIndex(prevout.get("index").asInt());

		return builder.build();
	}

	private Transaction asTransaction(String txHash) {
		Transaction.Builder builder = Transaction.newBuilder();

		builder.setHash(txHash);
		builder.setWitnessHash(null);
		builder.setFee(null);
		builder.setRate(null);
		builder.setMtime(null);
		builder.setHeight(null);
		builder.setBlock(null);
		builder.setTime(null);
		builder.setIndex(null);
		builder.setVersion(null);
		builder.setLocktime(null);
		builder.setHex(null);

		return builder.build();
	}

	private Transaction asTransaction(JsonNode jsonNode) {
		Transaction.Builder builder = Transaction.newBuilder();

		builder.setHash(jsonNode.get("hash").asText());
		builder.setWitnessHash(jsonNode.get("witnessHash").asText());
		builder.setFee(jsonNode.get("fee").floatValue());
		builder.setRate(jsonNode.get("rate").floatValue());
		builder.setMtime(jsonNode.get("mtime").asInt());
		builder.setHeight(jsonNode.get("height").asInt());
		builder.setBlock(jsonNode.get("block").asText());
		builder.setTime(jsonNode.get("time").asInt());
		builder.setIndex(jsonNode.get("index").asInt());
		builder.setVersion(jsonNode.get("version").asInt());
		builder.setLocktime(jsonNode.get("locktime").asInt());

		JsonNode hex = jsonNode.get("hex");
		if (hex != null && !(hex instanceof NullNode)) {
			builder.setHex(hex.asText());
		} else {
			builder.setHex(null);
		}

		return builder.build();
	}

	@Override
	protected URL getURL(DoFn<String, Transaction>.ProcessContext ctx) throws MalformedURLException {
		String txHash = ctx.element();
		return new URL(bcoinUrl + "/tx/" + txHash);
	}

}

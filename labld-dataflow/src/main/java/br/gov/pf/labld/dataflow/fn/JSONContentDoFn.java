package br.gov.pf.labld.dataflow.fn;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.cloud.dataflow.sdk.transforms.DoFn;

public abstract class JSONContentDoFn<S, T> extends URLContentDoFn<S, T> {

	private static final Logger LOGGER = LoggerFactory.getLogger(JSONContentDoFn.class);

	private static final long serialVersionUID = 2504289436337166822L;

	private ObjectMapper objectMapper;

	private boolean failSafe;

	public JSONContentDoFn(boolean failSafe) {
		super();
		this.failSafe = failSafe;
	}

	public abstract void process(DoFn<S, T>.ProcessContext ctx, JsonNode jsonNode) throws Exception;

	@Override
	public void processElement(DoFn<S, T>.ProcessContext ctx) throws Exception {
		URL url = getURL(ctx);
		JsonNode jsonNode = readJsonNode(url);
		if (jsonNode != null) {
			process(ctx, jsonNode);
		}
	}

	protected JsonNode readJsonNode(URL url) throws IOException {
		return readJsonNode(url, null);
	}

	protected JsonNode readJsonNode(URL url, JsonNode payloadNode) throws IOException {
		LOGGER.info("Processing json at {}.", url);
		String payload = null;
		if (payloadNode != null) {
			payload = payloadNode.toString();
		}
		HttpURLConnection conn = openConnection(url, payload);
		conn.connect();
		int responseCode = conn.getResponseCode();
		if (isResponseCodeAcceptable(responseCode)) {
			JsonNode jsonNode;
			try (InputStream in = new BufferedInputStream(conn.getInputStream())) {
				jsonNode = getObjectMapper().readTree(in);
			}
			return jsonNode;
		} else {
			String msg = "Download of " + url + " failed with code " + responseCode;
			if (failSafe) {
				LOGGER.info(msg);
				return null;
			} else {
				throw new IOException(msg);
			}
		}
	}

	protected boolean isResponseCodeAcceptable(int responseCode) {
		return responseCode >= 200 && responseCode <= 299;
	}

	protected ObjectMapper getObjectMapper() {
		if (objectMapper == null) {
			objectMapper = new ObjectMapper();
		}
		return objectMapper;
	}

	protected URL getURL(DoFn<S, T>.ProcessContext ctx) throws MalformedURLException {
		return null;
	}

}

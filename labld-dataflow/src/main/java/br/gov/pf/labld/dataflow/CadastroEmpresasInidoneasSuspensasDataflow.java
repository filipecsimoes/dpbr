package br.gov.pf.labld.dataflow;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.csv.CSVFormat;

import br.gov.pf.labld.dataflow.fn.CSVParseDoFn;
import br.gov.pf.labld.dataflow.fn.CSVRecordLine;
import br.gov.pf.labld.dataflow.fn.CSVRecordToTableRowDoFn;
import br.gov.pf.labld.dataflow.fn.FileHandler;
import br.gov.pf.labld.dataflow.fn.Line;
import br.gov.pf.labld.dataflow.fn.LineReadDoFn;
import br.gov.pf.labld.dataflow.fn.URLDownloadDoFn;
import br.gov.pf.labld.dataflow.fn.ZipExtractDoFn;
import br.gov.pf.labld.dataflow.util.CSVBigQueryConverter;
import br.gov.pf.labld.dataflow.util.CSVBigQueryFieldConverter;

import com.google.api.services.bigquery.model.TableRow;
import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.coders.StringUtf8Coder;
import com.google.cloud.dataflow.sdk.io.BigQueryIO;
import com.google.cloud.dataflow.sdk.io.BigQueryIO.Write.CreateDisposition;
import com.google.cloud.dataflow.sdk.io.BigQueryIO.Write.WriteDisposition;
import com.google.cloud.dataflow.sdk.options.Default;
import com.google.cloud.dataflow.sdk.options.Description;
import com.google.cloud.dataflow.sdk.options.PipelineOptions;
import com.google.cloud.dataflow.sdk.options.PipelineOptionsFactory;
import com.google.cloud.dataflow.sdk.transforms.Create;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.values.PCollection;
import com.google.cloud.storage.Acl;
import com.google.cloud.storage.Acl.Role;
import com.google.cloud.storage.Acl.User;

public class CadastroEmpresasInidoneasSuspensasDataflow {

	private static final SimpleDateFormat URL_FORMATTER;
	public static final CSVFormat CSV_FORMAT = CSVFormat.EXCEL.withSkipHeaderRecord(true).withQuote('"').withDelimiter(';').withRecordSeparator("\r\n");
	public static final CSVBigQueryConverter CONVERTER;

	static {
		String format = "'http://arquivos.portaldatransparencia.gov.br/downloads.asp?a='yyyy'&m='MM'&d='dd'&consulta=CEIS'";
		URL_FORMATTER = new SimpleDateFormat(format);

		CONVERTER = new CSVBigQueryConverter();

		CSVBigQueryFieldConverter.TimestampConverter dataFieldConverter = new CSVBigQueryFieldConverter.TimestampConverter("dd/MM/yyyy", "America/Sao_Paulo");

		CONVERTER.addForPosition("tipo_pessoa", 0).setDescription("Tipo de Pessoa F\u00edsica ou Jur\u00eddica");
		CONVERTER.addForPosition("cpf_cnpj_sancionado", 1).setDescription("CPF ou CNPJ do Sancionado");
		CONVERTER.addForPosition("nome_informado", 2).setDescription("Nome Informado pelo \u00d3rg\u00e3o Sancionador");
		CONVERTER.addForPosition("razao_social", 3).setDescription("Raz\u00e3o Social - Cadastro Receita");
		CONVERTER.addForPosition("nome_fantasia", 4).setDescription("Nome Fantasia - Cadastro Receita");
		CONVERTER.addForPosition("numero_processo", 5).setDescription("N\u00famero do processo");
		CONVERTER.addForPosition("tipo_sancao", 6).setDescription("Tipo San\u00e7\u00e3o");
		CONVERTER.addForPosition("data_inicio_sancao", 7).setDescription("Data In\u00edcio San\u00e7\u00e3o").setConverter(dataFieldConverter).setType("TIMESTAMP");
		CONVERTER.addForPosition("data_final_sancao", 8).setDescription("Data Final San\u00e7\u00e3o").setConverter(dataFieldConverter).setType("TIMESTAMP");
		CONVERTER.addForPosition("orgao_sancionador", 9).setDescription("\u00d3rg\u00e3o Sancionador");
		CONVERTER.addForPosition("uf_orgao_sancionador", 10).setDescription("UF \u00d3rg\u00e3o Sancionador");
		CONVERTER.addForPosition("origem_informacoes", 11).setDescription("Origem Informa\u00e7\u00f5es");
		CONVERTER.addForPosition("data_origem_informacoes", 12).setDescription("Data Origem Informa\u00e7\u00f5es").setConverter(dataFieldConverter).setType("TIMESTAMP");
		CONVERTER.addForPosition("data_publicacao", 13).setDescription("Data Publica\u00e7\u00e3o").setConverter(dataFieldConverter).setType("TIMESTAMP");
		CONVERTER.addForPosition("publicacao", 14).setDescription("Publica\u00e7\u00e3o");
		CONVERTER.addForPosition("detalhamento", 15).setDescription("Detalhamento");
		CONVERTER.addForPosition("abrangencia", 16).setDescription("Abrang\u00eancia definida em decis\u00e3o judicial");
	}

	public static interface Options extends PipelineOptions {

		@Description("Bucket for temp data")
		@Default.String("dpbr-temp-files")
		String getBucket();

		void setBucket(String bucket);

		@Description("Folder for temp data")
		@Default.String("")
		String getFolder();

		void setFolder(String folder);

		@Description("BigQuery table to write to.")
		@Default.String("sispubbr:dadospublicos.cadastro_empresas_inidoneas_suspensas")
		String getTableSpec();

		void setTableSpec(String tableSpec);

	}

	public static void main(String[] args) {
		Options options = PipelineOptionsFactory.fromArgs(args).withValidation().as(Options.class);
		Pipeline p = Pipeline.create(options);

		String folder = options.getFolder();
		String bucket = options.getBucket();
		String tableSpec = options.getTableSpec();
		List<Acl> acls = new ArrayList<Acl>(Arrays.asList(Acl.of(User.ofAllUsers(), Role.READER)));
		FileHandler.GCSFileHandler fileHandler = new FileHandler.GCSFileHandler(bucket, acls);

		String currentUrl = URL_FORMATTER.format(new Date());
		PCollection<String> url = p.apply(Create.of(currentUrl).withCoder(StringUtf8Coder.of()));
		PCollection<String> zip = url.apply(ParDo.of(new URLDownloadDoFn(folder, fileHandler)).named("DownloadZIP"));
		PCollection<String> csvs = zip.apply(ParDo.of(new ZipExtractDoFn(folder, fileHandler, ".+\\.csv$")).named("UnzipCSV"));
		PCollection<Line> lines = csvs.apply(ParDo.of(new LineReadDoFn(fileHandler, 1, "ISO-8859-1")).named("ReadCSV"));
		PCollection<CSVRecordLine> csv = lines.apply(ParDo.of(new CSVParseDoFn(CSV_FORMAT)).named("ParseCSV"));
		PCollection<TableRow> rows = csv.apply(ParDo.of(new CSVRecordToTableRowDoFn(CONVERTER)).named("ConvertBigQuery"));
		rows.apply(BigQueryIO.Write.named("WriteBigQuery").to(tableSpec).withSchema(CONVERTER.getTableFieldSchema()).withWriteDisposition(WriteDisposition.WRITE_TRUNCATE)
				.withCreateDisposition(CreateDisposition.CREATE_IF_NEEDED).withoutValidation());

		p.run();
	}

}

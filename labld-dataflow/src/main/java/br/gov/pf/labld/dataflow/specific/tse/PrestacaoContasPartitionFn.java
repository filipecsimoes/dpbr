package br.gov.pf.labld.dataflow.specific.tse;

import com.google.cloud.dataflow.sdk.transforms.Partition.PartitionFn;

public class PrestacaoContasPartitionFn implements PartitionFn<String> {

	private static final long serialVersionUID = 6901450570735076789L;

	private final String[] fileNames;

	public PrestacaoContasPartitionFn(String[] fileNames) {
		super();
		this.fileNames = fileNames;
	}

	@Override
	public int partitionFor(String elem, int numPartitions) {
		for (int index = 0; index < fileNames.length; index++) {
			if (elem.contains(fileNames[index])) {
				return index;
			}
		}
		throw new RuntimeException("Unknown Element: " + elem);
	}

}

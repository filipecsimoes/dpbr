package br.gov.pf.labld.dataflow.fn;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import com.google.cloud.dataflow.sdk.transforms.DoFn;

public class LineReadDoFn extends DoFn<String, Line> {

	private static final long serialVersionUID = 3592506359414526379L;

	private final FileHandler fileHandler;

	private final int numLinesToIgnore;

	private final String enconding;

	public LineReadDoFn(FileHandler fileHandler) {
		super();
		this.fileHandler = fileHandler;
		this.numLinesToIgnore = 0;
		this.enconding = "utf-8";
	}

	public LineReadDoFn(FileHandler fileHandler, int numLinesToIgnore) {
		super();
		this.fileHandler = fileHandler;
		this.numLinesToIgnore = numLinesToIgnore;
		this.enconding = "utf-8";
	}

	public LineReadDoFn(FileHandler fileHandler, int numLinesToIgnore, String enconding) {
		super();
		this.fileHandler = fileHandler;
		this.numLinesToIgnore = numLinesToIgnore;
		this.enconding = enconding;
	}

	@Override
	public void processElement(DoFn<String, Line>.ProcessContext ctx) throws Exception {
		String path = ctx.element();
		int lineNumber = 0;
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(fileHandler.open(path), enconding))) {
			for (int count = 0; count < numLinesToIgnore; count++) {
				lineNumber++;
				reader.readLine();
			}
			String content;
			while ((content = reader.readLine()) != null && !content.trim().isEmpty()) {
				lineNumber++;
				Line line = new Line(lineNumber, path, content);
				ctx.output(line);
			}
		}
	}
}

package br.gov.pf.labld.dataflow.specific.rab;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import br.gov.pf.labld.dataflow.util.CSVBigQueryFieldConverter.TimestampConverter;

public class RABData {

	private Document doc;
	private TimestampConverter converterSimple;
	private TimestampConverter converterComplete;

	public RABData(Document doc, TimestampConverter converterSimple, TimestampConverter converterComplete) {
		super();
		this.doc = doc;
		this.converterSimple = converterSimple;
		this.converterComplete = converterComplete;
	}

	private String get(String cssQuery) {
		Element first = doc.select(cssQuery).first();
		if (first != null) {
			return first.text().trim();
		}
		return null;
	}

	public String getPrefixo() {
		String prefixo = get("body > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td.bgCorpo > table:nth-child(2) > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(2) > td");
		String separator = ":";
		if (prefixo != null && !prefixo.isEmpty() && prefixo.contains(separator)) {
			prefixo = prefixo.split(separator)[1];
			prefixo = prefixo.trim();
		}
		return prefixo;
	}

	public String getProprietario() {
		return get("body > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td.bgCorpo > table:nth-child(2) > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > div");
	}

	public String getCpfCnpjProprietario() {
		return get("body > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td.bgCorpo > table:nth-child(2) > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(6) > td:nth-child(2) > div");
	}

	public String getOperador() {
		return get("body > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td.bgCorpo > table:nth-child(2) > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(7) > td:nth-child(2) > div");
	}

	public String getCpfCnpjOperador() {
		return get("body > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td.bgCorpo > table:nth-child(2) > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(8) > td:nth-child(2) > div");
	}

	public String getFabricante() {
		return get("body > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td.bgCorpo > table:nth-child(2) > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(10) > td:nth-child(2) > div");
	}

	public String getModelo() {
		return get("body > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td.bgCorpo > table:nth-child(2) > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(11) > td:nth-child(2) > div");
	}

	public String getNumeroSerie() {
		return get("body > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td.bgCorpo > table:nth-child(2) > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(12) > td:nth-child(2) > div");
	}

	public String getTipoIcao() {
		return get("body > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td.bgCorpo > table:nth-child(2) > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(13) > td:nth-child(2) > div");
	}

	public String getTipoHabilitacaoPilotos() {
		return get("body > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td.bgCorpo > table:nth-child(2) > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(14) > td:nth-child(2) > div");
	}

	public String getClasseAeronave() {
		return get("body > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td.bgCorpo > table:nth-child(2) > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(15) > td:nth-child(2) > div");
	}

	public String getPesoMaximoDecolagem() {
		return get("body > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td.bgCorpo > table:nth-child(2) > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(16) > td:nth-child(2) > div");
	}

	public String getNumeroMaximoPassageiros() {
		return get("body > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td.bgCorpo > table:nth-child(2) > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(17) > td:nth-child(2) > div");
	}

	public String getTipoVooAutorizado() {
		return get("body > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td.bgCorpo > table:nth-child(2) > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(18) > td:nth-child(2) > div");
	}

	public String getCategoriaRegistro() {
		return get("body > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td.bgCorpo > table:nth-child(2) > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(22) > td:nth-child(2) > div");
	}

	public String getNumeroCertificados() {
		return get("body > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td.bgCorpo > table:nth-child(2) > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(23) > td:nth-child(2) > div");
	}

	public String getSituacaoRab() {
		return get("body > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td.bgCorpo > table:nth-child(2) > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(24) > td:nth-child(2) > div");
	}

	public Object getDataCompraTransferencia() {
		String data = get("body > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td.bgCorpo > table:nth-child(2) > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(25) > td:nth-child(2) > div");
		try {
			return converterSimple.convert(data);
		} catch (Exception e) {
			return null;
		}
	}

	public Object getDataValidadeCa() {
		String data = get("body > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td.bgCorpo > table:nth-child(2) > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(29) > td:nth-child(2) > div");
		try {
			return converterComplete.convert(data);
		} catch (Exception e) {
			return null;
		}
	}

	public Object getDataValidadeIam() {
		String data = get("body > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td.bgCorpo > table:nth-child(2) > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(30) > td:nth-child(2) > div");
		try {
			return converterSimple.convert(data);
		} catch (Exception e) {
			return null;
		}
	}

	public String getSituacaoAeronavegabilidade() {
		return get("body > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td.bgCorpo > table:nth-child(2) > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(31) > td:nth-child(2) > div");
	}

	public String getMotivos() {
		return get("body > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td.bgCorpo > table:nth-child(2) > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(33) > td");
	}

	public boolean isAeronaveCadastrada() {
		Element msg = doc.select("body > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td.bgCorpo > table:nth-child(2)").first();
		return msg == null || !msg.text().contains("Aeronave n\u00e3o cadastrada!");
	}

}

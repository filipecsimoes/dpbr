package br.gov.pf.labld.dataflow;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.apache.commons.csv.CSVFormat;

import br.gov.pf.labld.dataflow.fn.CSVParseDoFn;
import br.gov.pf.labld.dataflow.fn.CSVRecordLine;
import br.gov.pf.labld.dataflow.fn.CSVRecordToTableRowDoFn;
import br.gov.pf.labld.dataflow.fn.FileHandler;
import br.gov.pf.labld.dataflow.fn.Line;
import br.gov.pf.labld.dataflow.fn.LineReadDoFn;
import br.gov.pf.labld.dataflow.fn.RegexFix;
import br.gov.pf.labld.dataflow.fn.RegexFixDoFn;
import br.gov.pf.labld.dataflow.fn.URLDownloadDoFn;
import br.gov.pf.labld.dataflow.fn.ZipExtractDoFn;
import br.gov.pf.labld.dataflow.specific.tse.PrestacaoContas2010PartitionFn;
import br.gov.pf.labld.dataflow.util.CSVBigQueryConverter;
import br.gov.pf.labld.dataflow.util.CSVBigQueryFieldConverter;
import br.gov.pf.labld.dataflow.util.CSVBigQueryFieldConverter.FloatConverter;

import com.google.api.services.bigquery.model.TableRow;
import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.coders.StringUtf8Coder;
import com.google.cloud.dataflow.sdk.io.BigQueryIO;
import com.google.cloud.dataflow.sdk.io.BigQueryIO.Write.CreateDisposition;
import com.google.cloud.dataflow.sdk.io.BigQueryIO.Write.WriteDisposition;
import com.google.cloud.dataflow.sdk.options.Default;
import com.google.cloud.dataflow.sdk.options.Description;
import com.google.cloud.dataflow.sdk.options.PipelineOptions;
import com.google.cloud.dataflow.sdk.options.PipelineOptionsFactory;
import com.google.cloud.dataflow.sdk.transforms.Create;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.transforms.Partition;
import com.google.cloud.dataflow.sdk.values.PCollection;
import com.google.cloud.dataflow.sdk.values.PCollectionList;
import com.google.cloud.storage.Acl;
import com.google.cloud.storage.Acl.Role;
import com.google.cloud.storage.Acl.User;

public class TSEPrestacaoContas2010Dataflow {

	private static final String URL_PRESTACAO_CONTAS_2010 = "http://agencia.tse.jus.br/estatistica/sead/odsele/prestacao_contas/prestacao_contas_2010.zip";

	private static final CSVFormat CSV_FORMAT = CSVFormat.EXCEL.withSkipHeaderRecord(true).withQuote('"').withDelimiter(';').withRecordSeparator("\r\n");

	private static final String[] TABLES;
	public static final CSVBigQueryConverter[] CONVERTERS;

	public static final CSVBigQueryConverter CONVERTER_DESPESAS_CANDIDATOS;
	public static final CSVBigQueryConverter CONVERTER_RECEITAS_CANDIDATOS;

	public static final CSVBigQueryConverter CONVERTER_DESPESAS_COMITES;
	public static final CSVBigQueryConverter CONVERTER_RECEITAS_COMITES;

	public static final CSVBigQueryConverter CONVERTER_DESPESAS_PARTIDOS;
	public static final CSVBigQueryConverter CONVERTER_RECEITAS_PARTIDOS;

	static {
		TABLES = new String[] { "tse_despesas_candidatos_2010", "tse_receitas_candidatos_2010", "tse_despesas_comites_2010", "tse_receitas_comites_2010",
				"tse_despesas_partidos_2010", "tse_receitas_partidos_2010" };

		CSVBigQueryFieldConverter.TimestampConverter dataFieldConverter = new CSVBigQueryFieldConverter.TimestampConverter("dd/MM/yyyy", "America/Sao_Paulo");
		FloatConverter floatConverter = new FloatConverter(new Locale("pt", "BR"), "0.##");

		CONVERTER_DESPESAS_CANDIDATOS = new CSVBigQueryConverter();

		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("sequencial_candidato", 1).setDescription("Sequencial Candidato");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("uf", 2).setDescription("UF");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("sigla_partido", 3).setDescription("Sigla Partido");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("numero_candidato", 4).setDescription("N\u00famero candidato");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("cargo", 5).setDescription("Cargo");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("nome_candidato", 6).setDescription("Nome candidato");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("cpf_candidato", 7).setDescription("CPF do candidato");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("entrega_em_conjunto", 8).setDescription("Entrega em conjunto?");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("tipo_documento", 9).setDescription("Tipo do documento");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("numero_documento", 10).setDescription("N\u00famero do documento");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("cpf_cnpj_fornecedor", 11).setDescription("CPF/CNPJ do fornecedor");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("nome_fornecedor", 12).setDescription("Nome do fornecedor");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("data_despesa", 13).setDescription("Data da despesa").setConverter(dataFieldConverter).setType("TIMESTAMP");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("valor_despesa", 14).setDescription("Valor despesa").setConverter(floatConverter).setType("FLOAT64");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("tipo_despesa", 15).setDescription("Tipo despesa");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("fonte_recurso", 16).setDescription("Fonte recurso");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("especie_recurso", 17).setDescription("Esp\u00e9cie recurso");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("descricao_despesa", 18).setDescription("Descri\u00e7ao da despesa");

		CONVERTER_RECEITAS_CANDIDATOS = new CSVBigQueryConverter();

		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("sequencial_candidato", 1).setDescription("Sequencial Candidato");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("uf", 2).setDescription("UF");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("sigla_partido", 3).setDescription("Sigla Partido");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("numero_candidato", 4).setDescription("N\u00famero candidato");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("cargo", 5).setDescription("Cargo");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("nome_candidato", 6).setDescription("Nome candidato");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("cpf_candidato", 7).setDescription("CPF do candidato");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("entrega_em_conjunto", 8).setDescription("Entrega em conjunto?");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("numero_recibo_eleitoral", 9).setDescription("N\u00famero Recibo Eleitoral");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("numero_documento", 10).setDescription("N\u00famero do documento");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("cpf_cnpj_doador", 11).setDescription("CPF/CNPJ do doador");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("nome_doador", 12).setDescription("Nome do doador");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("data_receita", 13).setDescription("Data da receita").setConverter(dataFieldConverter).setType("TIMESTAMP");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("valor_receita", 14).setDescription("Valor receita").setConverter(floatConverter).setType("FLOAT64");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("tipo_receita", 15).setDescription("Tipo receita");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("fonte_recurso", 16).setDescription("Fonte recurso");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("especie_recurso", 17).setDescription("Esp\u00e9cie recurso");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("descricao_receita", 18).setDescription("Descri\u00e7\u00e3o da receita");

		CONVERTER_DESPESAS_COMITES = new CSVBigQueryConverter();

		CONVERTER_DESPESAS_COMITES.addForPosition("uf", 1).setDescription("UF");
		CONVERTER_DESPESAS_COMITES.addForPosition("tipo_comite", 2).setDescription("Tipo comite");
		CONVERTER_DESPESAS_COMITES.addForPosition("sigla_partido", 3).setDescription("Sigla Partido");
		CONVERTER_DESPESAS_COMITES.addForPosition("tipo_documento", 4).setDescription("Tipo do documento");
		CONVERTER_DESPESAS_COMITES.addForPosition("numero_documento", 5).setDescription("N\u00famero do documento");
		CONVERTER_DESPESAS_COMITES.addForPosition("cpf_cnpj_fornecedor", 6).setDescription("CPF/CNPJ do fornecedor");
		CONVERTER_DESPESAS_COMITES.addForPosition("nome_fornecedor", 7).setDescription("Nome do fornecedor");
		CONVERTER_DESPESAS_COMITES.addForPosition("data_despesa", 8).setDescription("Data da despesa").setConverter(dataFieldConverter).setType("TIMESTAMP");
		CONVERTER_DESPESAS_COMITES.addForPosition("valor_despesa", 9).setDescription("Valor despesa").setConverter(floatConverter).setType("FLOAT64");
		CONVERTER_DESPESAS_COMITES.addForPosition("tipo_despesa", 10).setDescription("Tipo despesa");
		CONVERTER_DESPESAS_COMITES.addForPosition("fonte_recurso", 11).setDescription("Fonte recurso");
		CONVERTER_DESPESAS_COMITES.addForPosition("especie_recurso", 12).setDescription("Esp\u00e9cie recurso");
		CONVERTER_DESPESAS_COMITES.addForPosition("descricao_despesa", 13).setDescription("Descri\u00e7\u00e3o da despesa");

		CONVERTER_RECEITAS_COMITES = new CSVBigQueryConverter();

		CONVERTER_RECEITAS_COMITES.addForPosition("uf", 1).setDescription("UF");
		CONVERTER_RECEITAS_COMITES.addForPosition("tipo_comite", 2).setDescription("Tipo comite");
		CONVERTER_RECEITAS_COMITES.addForPosition("sigla_partido", 3).setDescription("Sigla Partido");
		CONVERTER_RECEITAS_COMITES.addForPosition("tipo_documento", 4).setDescription("Tipo do documento");
		CONVERTER_RECEITAS_COMITES.addForPosition("numero_documento", 5).setDescription("N\u00famero do documento");
		CONVERTER_RECEITAS_COMITES.addForPosition("cpf_cnpj_doador", 6).setDescription("CPF/CNPJ do doador");
		CONVERTER_RECEITAS_COMITES.addForPosition("nome_doador", 7).setDescription("Nome do doador");
		CONVERTER_RECEITAS_COMITES.addForPosition("data_receita", 8).setDescription("Data da receita").setConverter(dataFieldConverter).setType("TIMESTAMP");
		CONVERTER_RECEITAS_COMITES.addForPosition("valor_receita", 9).setDescription("Valor receita").setConverter(floatConverter).setType("FLOAT64");
		CONVERTER_RECEITAS_COMITES.addForPosition("tipo_receita", 10).setDescription("Tipo receita");
		CONVERTER_RECEITAS_COMITES.addForPosition("fonte_recurso", 11).setDescription("Fonte recurso");
		CONVERTER_RECEITAS_COMITES.addForPosition("especie_recurso", 12).setDescription("Esp\u00e9cie recurso");
		CONVERTER_RECEITAS_COMITES.addForPosition("descricao_receita", 13).setDescription("Descri\u00e7\u00e3o da receita");

		CONVERTER_DESPESAS_PARTIDOS = new CSVBigQueryConverter();

		CONVERTER_DESPESAS_PARTIDOS.addForPosition("uf", 1).setDescription("UF");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("tipo_partido", 2).setDescription("Tipo partido");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("sigla_partido", 3).setDescription("Sigla Partido");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("tipo_documento", 4).setDescription("Tipo do documento");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("numero_documento", 5).setDescription("N\u00famero do documento");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("cpf_cnpj_fornecedor", 6).setDescription("CPF/CNPJ do fornecedor");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("nome_fornecedor", 7).setDescription("Nome do fornecedor");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("data_despesa", 8).setDescription("Data da despesa").setConverter(dataFieldConverter).setType("TIMESTAMP");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("valor_despesa", 9).setDescription("Valor despesa").setConverter(floatConverter).setType("FLOAT64");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("tipo_despesa", 10).setDescription("Tipo despesa");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("fonte_recurso", 11).setDescription("Fonte recurso");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("especie_recurso", 12).setDescription("Esp\u00e9cie recurso");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("descricao_receita", 13).setDescription("Descri\u00e7\u00e3o da despesa");

		CONVERTER_RECEITAS_PARTIDOS = new CSVBigQueryConverter();

		CONVERTER_RECEITAS_PARTIDOS.addForPosition("uf", 1).setDescription("UF");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("tipo_partido", 2).setDescription("Tipo partido");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("sigla_partido", 3).setDescription("Sigla Partido");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("tipo_documento", 4).setDescription("Tipo do documento");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("numero_documento", 5).setDescription("N\u00famero do documento");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("cpf_cnpj_doador", 6).setDescription("CPF/CNPJ do doador");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("nome_doador", 7).setDescription("Nome do doador");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("data_receita", 8).setDescription("Data da receita").setConverter(dataFieldConverter).setType("TIMESTAMP");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("valor_receita", 9).setDescription("Valor receita").setConverter(floatConverter).setType("FLOAT64");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("tipo_receita", 10).setDescription("Tipo receita");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("fonte_recurso", 11).setDescription("Fonte recurso");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("especie_recurso", 12).setDescription("Esp\u00e9cie recurso");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("descricao_receita", 13).setDescription("Descri\u00e7\u00e3o da receita");

		CONVERTERS = new CSVBigQueryConverter[] { CONVERTER_DESPESAS_CANDIDATOS, CONVERTER_RECEITAS_CANDIDATOS, CONVERTER_DESPESAS_COMITES, CONVERTER_RECEITAS_COMITES,
				CONVERTER_DESPESAS_PARTIDOS, CONVERTER_RECEITAS_PARTIDOS };
	}

	public static interface Options extends PipelineOptions {

		@Description("Bucket for temp data")
		@Default.String("dataflow-temp-data")
		String getBucket();

		void setBucket(String bucket);

		@Description("Folder for temp data")
		@Default.String("")
		String getFolder();

		void setFolder(String folder);

		@Description("Dataflow project")
		@Default.String("arctic-cursor-171520")
		String getProject();

		void setProject(String project);

		@Description("Bigquery dataset")
		@Default.String("tse")
		String getDataset();

		void setDataset(String project);

	}

	public static void main(String[] args) {
		Options options = PipelineOptionsFactory.fromArgs(args).withValidation().as(Options.class);
		Pipeline p = Pipeline.create(options);

		String project = options.getProject();
		String dataset = options.getDataset();
		String folder = options.getFolder();
		String bucket = options.getBucket();
		List<Acl> acls = new ArrayList<Acl>(Arrays.asList(Acl.of(User.ofAllUsers(), Role.READER)));
		FileHandler.GCSFileHandler fileHandler = new FileHandler.GCSFileHandler(bucket, acls);

		PCollection<String> urls = p.apply(Create.of(URL_PRESTACAO_CONTAS_2010).withCoder(StringUtf8Coder.of()));
		PCollection<String> zips = urls.apply(ParDo.of(new URLDownloadDoFn(folder, fileHandler)).named("DownloadZIP"));
		PCollection<String> csvs = zips.apply(ParDo.of(new ZipExtractDoFn(folder, fileHandler, ".+\\.txt$")).named("UnzipCSV"));

		PCollectionList<String> csvList = csvs.apply(Partition.of(6, new PrestacaoContas2010PartitionFn()));
		for (int index = 0; index < PrestacaoContas2010PartitionFn.ELEM_NAMES.length; index++) {
			String elementName = PrestacaoContas2010PartitionFn.ELEM_NAMES[index];
			csvs = csvList.get(index);
			PCollection<Line> lines = csvs.apply(ParDo.of(new LineReadDoFn(fileHandler, 1, "iso-8859-1")).named("ReadCSV_" + elementName));
			RegexFix fix = new RegexFix(";\"([ \\-[0-9]\\p{L}]*?)\"([ [0-9]\\p{L}]+)\"([ \\-[0-9]\\p{L}]*?)\"", ";\"$1'$2'$3\"");
			PCollection<Line> fixedLines = lines.apply(ParDo.of(new RegexFixDoFn(fix)).named("FixCSV_" + elementName));
			PCollection<CSVRecordLine> csv = fixedLines.apply(ParDo.of(new CSVParseDoFn(CSV_FORMAT)).named("ParseCSV_" + elementName));

			CSVBigQueryConverter converter = CONVERTERS[index];
			String tableSpec = project + ":" + dataset + "." + TABLES[index];

			PCollection<TableRow> rows = csv.apply(ParDo.of(new CSVRecordToTableRowDoFn(converter)).named("ConvertBigQuery_" + elementName));
			rows.apply(BigQueryIO.Write.named("WriteBigQuery_" + elementName).to(tableSpec).withSchema(converter.getTableFieldSchema())
					.withWriteDisposition(WriteDisposition.WRITE_TRUNCATE).withCreateDisposition(CreateDisposition.CREATE_IF_NEEDED).withoutValidation());
		}

		p.run();
	}

}

package br.gov.pf.labld.dataflow.specific.tcesp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.gov.pf.labld.dataflow.DespesasMunicipaisEstadoSaoPauloDataFlow;
import br.gov.pf.labld.dataflow.util.CSVBigQueryFieldExtension;

/**
 * Extrai tipo de pessoa da coluna 'identificador_despesa'.
 * 
 * @see DespesasMunicipaisEstadoSaoPauloDataFlow
 *
 */
public class IdentificadorDespesaTipoPessoaExtension implements CSVBigQueryFieldExtension {

	private static final long serialVersionUID = 101008571545285792L;

	private Pattern patternPJ;
	private Pattern patternPF;
	private Pattern patternEspecial;

	@Override
	public String extend(String str) {
		Matcher matcher = getPatternPJ().matcher(str);
		if (matcher.matches()) {
			return matcher.group(1);
		}
		matcher = getPatternPF().matcher(str);
		if (matcher.matches()) {
			return matcher.group(1);
		}
		matcher = getPatternEspecial().matcher(str);
		if (matcher.matches()) {
			return matcher.group(1);
		}
		return null;
	}

	private Pattern getPatternPJ() {
		if (patternPJ == null) {
			patternPJ = Pattern.compile("CNPJ - (PESSOA JUR\u00cdDICA) - \\d{14}");
		}
		return patternPJ;
	}

	private Pattern getPatternPF() {
		if (patternPF == null) {
			patternPF = Pattern.compile("(PESSOA F\u00cdSICA) - \\d+");
		}
		return patternPF;
	}

	private Pattern getPatternEspecial() {
		if (patternEspecial == null) {
			patternEspecial = Pattern.compile("(IDENTIFICA\u00c7\u00c3O ESPECIAL) - SEM CPF\\/CNPJ - \\d+");
		}
		return patternEspecial;
	}

}

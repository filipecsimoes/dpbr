package br.gov.pf.labld.dataflow.specific.portaltransparencia;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.TimeZone;

import br.gov.pf.labld.dataflow.specific.portaltransparencia.DateProviderFunction.CurrentDateProviderFunction;

public class DateFormatProviderFunction extends CurrentDateProviderFunction {

	private static final long serialVersionUID = 3440658847640216028L;

	private final String outputFormat;
	private final String zoneId;

	private SimpleDateFormat sdf;

	public DateFormatProviderFunction(String outputFormat) {
		super();
		this.outputFormat = outputFormat;
		this.zoneId = "America/Sao_Paulo";
	}

	public DateFormatProviderFunction(String outputFormat, String zoneId) {
		super();
		this.outputFormat = outputFormat;
		this.zoneId = zoneId;
	}

	@Override
	public String applyDate(Calendar input) {
		return getOutputDateFormatter().format(input.getTime());
	}

	protected SimpleDateFormat getOutputDateFormatter() {
		if (sdf == null) {
			sdf = new SimpleDateFormat(outputFormat, DEFAULT_LOCALE);
			sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.of(zoneId)));
		}
		return sdf;
	}

}

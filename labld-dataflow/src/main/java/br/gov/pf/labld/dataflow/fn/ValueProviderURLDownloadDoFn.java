package br.gov.pf.labld.dataflow.fn;

import java.net.MalformedURLException;
import java.net.URL;

import com.google.cloud.dataflow.sdk.options.ValueProvider;
import com.google.cloud.dataflow.sdk.transforms.DoFn;

public class ValueProviderURLDownloadDoFn extends URLDownloadDoFn {

	private static final long serialVersionUID = -7689613392814974153L;

	private ValueProvider<String> provider;

	public ValueProviderURLDownloadDoFn(ValueProvider<String> provider, String folder, FileHandler downloader, boolean failSafe) {
		super(folder, downloader, failSafe);
		this.provider = provider;
	}

	public ValueProviderURLDownloadDoFn(ValueProvider<String> provider, String folder, FileHandler downloader, String method) {
		super(folder, downloader, method);
		this.provider = provider;
	}

	public ValueProviderURLDownloadDoFn(ValueProvider<String> provider, String folder, FileHandler downloader) {
		super(folder, downloader);
		this.provider = provider;
	}

	public ValueProviderURLDownloadDoFn(ValueProvider<String> provider, ValueProvider<String> folder, FileHandler downloader) {
		super(folder, downloader);
		this.provider = provider;
	}

	@Override
	protected URL getURL(DoFn<String, String>.ProcessContext ctx) throws MalformedURLException {
		return new URL(provider.get());
	}

}

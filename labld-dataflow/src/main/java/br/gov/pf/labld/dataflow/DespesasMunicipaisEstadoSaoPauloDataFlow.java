package br.gov.pf.labld.dataflow;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.csv.CSVFormat;

import br.gov.pf.labld.dataflow.fn.CSVParseDoFn;
import br.gov.pf.labld.dataflow.fn.CSVRecordLine;
import br.gov.pf.labld.dataflow.fn.CSVRecordToTableRowDoFn;
import br.gov.pf.labld.dataflow.fn.FileHandler;
import br.gov.pf.labld.dataflow.fn.Line;
import br.gov.pf.labld.dataflow.fn.LineReadDoFn;
import br.gov.pf.labld.dataflow.fn.URLDownloadDoFn;
import br.gov.pf.labld.dataflow.fn.ZipExtractDoFn;
import br.gov.pf.labld.dataflow.specific.tcesp.IdentificadorDespesaCNPJExtension;
import br.gov.pf.labld.dataflow.specific.tcesp.IdentificadorDespesaTipoPessoaExtension;
import br.gov.pf.labld.dataflow.specific.tcesp.TCESPDespesasMunicipiosListDownloadURLDoFn;
import br.gov.pf.labld.dataflow.specific.tcesp.TCESPDespesasMunicipiosListPageURLDoFn;
import br.gov.pf.labld.dataflow.util.CSVBigQueryConverter;
import br.gov.pf.labld.dataflow.util.CSVBigQueryFieldConverter;
import br.gov.pf.labld.dataflow.util.CSVBigQueryFieldConverter.FloatConverter;

import com.google.api.services.bigquery.model.TableRow;
import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.coders.StringUtf8Coder;
import com.google.cloud.dataflow.sdk.io.BigQueryIO;
import com.google.cloud.dataflow.sdk.io.BigQueryIO.Write.CreateDisposition;
import com.google.cloud.dataflow.sdk.io.BigQueryIO.Write.WriteDisposition;
import com.google.cloud.dataflow.sdk.options.Default;
import com.google.cloud.dataflow.sdk.options.Description;
import com.google.cloud.dataflow.sdk.options.PipelineOptions;
import com.google.cloud.dataflow.sdk.options.PipelineOptionsFactory;
import com.google.cloud.dataflow.sdk.options.ValueProvider;
import com.google.cloud.dataflow.sdk.options.ValueProvider.NestedValueProvider;
import com.google.cloud.dataflow.sdk.transforms.Create;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.transforms.SerializableFunction;
import com.google.cloud.dataflow.sdk.values.PCollection;
import com.google.cloud.storage.Acl;
import com.google.cloud.storage.Acl.Role;
import com.google.cloud.storage.Acl.User;

public class DespesasMunicipaisEstadoSaoPauloDataFlow {

	public static final CSVFormat CSV_FORMAT = CSVFormat.EXCEL.withSkipHeaderRecord(true).withDelimiter(';')
	        .withRecordSeparator("\r\n");
	public static final CSVBigQueryConverter CONVERTER;

	static {
		CONVERTER = new CSVBigQueryConverter();

		CSVBigQueryFieldConverter.TimestampConverter dataFieldConverter = new CSVBigQueryFieldConverter.TimestampConverter(
		        "dd/MM/yyyy", "America/Sao_Paulo");
		FloatConverter floatConverter = new FloatConverter(new Locale("pt", "BR"), "0.##");

		CONVERTER.addForPosition("id_despesa_detalhe", 0).setDescription("ID da despesa");
		CONVERTER.addForPosition("ano_exercicio", 1).setDescription("Ano Exerc\u00edcio");
		CONVERTER.addForPosition("ds_municipio", 2).setDescription("Munic\u00edpio da despesa");
		CONVERTER.addForPosition("ds_orgao", 3).setDescription("\u00d3rg\u00e3o da despesa");
		CONVERTER.addForPosition("mes_referencia", 4).setDescription("M\u00eas de refer\u00eancia");
		CONVERTER.addForPosition("mes_ref_extenso", 5).setDescription("M\u00eas de refer\u00eancia por extenso");
		CONVERTER.addForPosition("tp_despesa", 6).setDescription("Tipo de despesa");
		CONVERTER.addForPosition("nr_empenho", 7).setDescription("Numero empenho");
		CONVERTER.addForPosition("identificador_despesa", 8).setDescription("Identificador da despesa");
		CONVERTER.addForPosition("ds_despesa", 9).setDescription("Descri\u00e7\u00e3o despesa");
		CONVERTER.addForPosition("dt_emissao_despesa", 10).setDescription("Data emiss\u00e3o da despesa")
		        .setConverter(dataFieldConverter).setType("TIMESTAMP");
		CONVERTER.addForPosition("vl_despesa", 11).setDescription("Valor da despesa").setConverter(floatConverter)
		        .setType("FLOAT64");
		CONVERTER.addForPosition("ds_funcao_governo", 12).setDescription("Fun\u00e7\u00e3o");
		CONVERTER.addForPosition("ds_subfuncao_governo", 13).setDescription("Sub-fun\u00e7\u00e3o");
		CONVERTER.addForPosition("cd_programa", 14).setDescription("C\u00f3digo programa");
		CONVERTER.addForPosition("ds_programa", 15).setDescription("Descri\u00e7\u00e3o programa");
		CONVERTER.addForPosition("cd_acao", 16).setDescription("C\u00f3digo a\u00e7\u00e3o");
		CONVERTER.addForPosition("ds_acao", 17).setDescription("Descri\u00e7\u00e3o a\u00e7\u00e3o");
		CONVERTER.addForPosition("ds_fonte_recurso", 18).setDescription("Descri\u00e7\u00e3o fonte recurso");
		CONVERTER.addForPosition("ds_cd_aplicacao_fixo", 19)
		        .setDescription("Descri\u00e7\u00e3o c\u00f3digo aplica\u00e7\u00e3o fixo");
		CONVERTER.addForPosition("ds_modalidade_lic", 20)
		        .setDescription("Descri\u00e7\u00e3o modalidade licita\u00e7\u00e3o");
		CONVERTER.addForPosition("ds_elemento", 21).setDescription("Descri\u00e7\u00e3o elemento");
		CONVERTER.addForPosition("historico_despesa", 22).setDescription("Hist\u00f3rico da despesa");

		CONVERTER.extendPosition("cnpj_identificador_despesa", 8).setExtension(new IdentificadorDespesaCNPJExtension())
		        .setDescription("CNPJ extra\u00eddo da coluna 'identificador_despesa'");
		CONVERTER.extendPosition("tipo_pessoa_identificador_despesa", 8)
		        .setExtension(new IdentificadorDespesaTipoPessoaExtension())
		        .setDescription("Tipo de pessoa extra\u00eddo da coluna 'identificador_despesa'");

	}

	public static interface Options extends PipelineOptions {

		@Description("Bucket for temp data")
		@Default.String("dataflow-temp-data")
		String getBucket();

		void setBucket(String bucket);

		@Description("Folder for temp data")
		@Default.String("tce-despesas-municipais")
		ValueProvider<String> getFolder();

		void setFolder(ValueProvider<String> folder);

		@Description("Dataflow project")
		@Default.String("arctic-cursor-171520")
		String getProject();

		void setProject(String project);

		@Description("Bigquery dataset")
		@Default.String("tce_sp")
		String getDataset();

		void setDataset(String project);

		@Description("Year")
		ValueProvider<String> getYear();

		void setYear(ValueProvider<String> year);

		@Description("Proxy")
		ValueProvider<String> getProxy();

		void setProxy(ValueProvider<String> proxy);

	}

	public static class TableSpecProviderFunction implements SerializableFunction<String, String> {

		private static final long serialVersionUID = -4707508448773237585L;

		private String project;
		private String dataset;

		private static final MessageFormat FMT = new MessageFormat("{0}:{1}.tce_sp_despesas_municipais_{2}");

		public TableSpecProviderFunction(String project, String dataset) {
			super();
			this.project = project;
			this.dataset = dataset;
		}

		@Override
		public String apply(String input) {
			return FMT.format(new Object[] { project, dataset, input });
		}

	}

	public static class ExercicioProviderFunction implements SerializableFunction<String, String> {

		private static final long serialVersionUID = -326058826751182940L;

		@Override
		public String apply(String input) {
			if (input == null) {
				input = calcCurrent();
			}
			return input;
		}

		private String calcCurrent() {
			Calendar cal = Calendar.getInstance();
			return Integer.toString(cal.get(Calendar.YEAR));
		}

	}

	public static void main(String[] args) throws IOException {
		Options options = PipelineOptionsFactory.fromArgs(args).withValidation().as(Options.class);
		Pipeline p = Pipeline.create(options);

		ValueProvider<String> folder = options.getFolder();
		String bucket = options.getBucket();
		String project = options.getProject();
		String dataset = options.getDataset();
		ValueProvider<String> proxy = options.getProxy();
		ValueProvider<String> year = options.getYear();

		year = NestedValueProvider.of(year, new ExercicioProviderFunction());

		List<Acl> acls = new ArrayList<Acl>(Arrays.asList(Acl.of(User.ofAllUsers(), Role.READER)));
		FileHandler.GCSFileHandler fileHandler = new FileHandler.GCSFileHandler(bucket, acls);

		// PCollection<String> urlExercicios =
		// p.apply(Create.of("").withCoder(StringUtf8Coder.of()));

		// TCESPDespesasMunicipiosListPageURLDoFn tcespDespesasMunicipiosListPageURLDoFn
		// = new TCESPDespesasMunicipiosListPageURLDoFn(year);
		// tcespDespesasMunicipiosListPageURLDoFn.setProxy(proxy);
		// PCollection<String> pages =
		// urlExercicios.apply(ParDo.of(tcespDespesasMunicipiosListPageURLDoFn).named("ListPageURL"));
		// TCESPDespesasMunicipiosListDownloadURLDoFn
		// tcespDespesasMunicipiosListDownloadURLDoFn = new
		// TCESPDespesasMunicipiosListDownloadURLDoFn();
		// tcespDespesasMunicipiosListDownloadURLDoFn.setProxy(proxy);

		Set<String> urlsSet = new HashSet<>();
		try (BufferedReader in = new BufferedReader(new InputStreamReader(
		        DespesasMunicipaisEstadoSaoPauloDataFlow.class.getClassLoader().getResourceAsStream("tce-sp-downloads.txt"),
		        Charset.forName("utf-8")))) {
			String line = null;
			while ((line = in.readLine()) != null) {
				if (!line.trim().isEmpty()) {
					line = line.replace("{0}", year.get());
					urlsSet.add(line);
				}
			}
		}

		PCollection<String> urls = p.apply(Create.of(urlsSet).withCoder(StringUtf8Coder.of()));
		PCollection<String> zips = urls.apply(ParDo.of(new URLDownloadDoFn(folder, fileHandler)).named("DownloadZIP"));
		PCollection<String> csvs = zips
		        .apply(ParDo.of(new ZipExtractDoFn(folder, fileHandler, ".+\\.csv$")).named("UnzipCSV"));
		PCollection<Line> lines = csvs.apply(ParDo.of(new LineReadDoFn(fileHandler, 1, "ISO-8859-1")).named("ReadCSV"));
		PCollection<CSVRecordLine> csv = lines.apply(ParDo.of(new CSVParseDoFn(CSV_FORMAT)).named("ParseCSV"));
		PCollection<TableRow> rows = csv
		        .apply(ParDo.of(new CSVRecordToTableRowDoFn(CONVERTER)).named("ConvertBigQuery"));

		NestedValueProvider<String, String> tableSpecProvider = NestedValueProvider.of(year,
		        new TableSpecProviderFunction(project, dataset));

		rows.apply(BigQueryIO.Write.named("WriteBigQuery").to(tableSpecProvider)
		        .withSchema(CONVERTER.getTableFieldSchema()).withWriteDisposition(WriteDisposition.WRITE_TRUNCATE)
		        .withCreateDisposition(CreateDisposition.CREATE_IF_NEEDED).withoutValidation());

		p.run();
	}
}

package br.gov.pf.labld.dataflow;

import org.apache.commons.csv.CSVFormat;

import br.gov.pf.labld.dataflow.util.CSVBigQueryConverter;
import br.gov.pf.labld.dataflow.util.CSVBigQueryFieldConverter;

import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.options.PipelineOptionsFactory;

public class PortalTransparenciaCadastroServidoresDataflow extends PortalTransparenciaDataflow {

	public static final CSVFormat CSV_FORMAT = CSVFormat.EXCEL.withSkipHeaderRecord(true).withDelimiter('\t').withRecordSeparator("\r\n");

	private static final String URL_FMT = "'http://arquivos.portaldatransparencia.gov.br/downloads.asp?a='yyyy'&m='MM'&d=C&consulta=Servidores'";

	public static final CSVBigQueryConverter CONVERTER;

	static {
		CONVERTER = new CSVBigQueryConverter();

		CSVBigQueryFieldConverter.TimestampConverter dataFieldConverter = new CSVBigQueryFieldConverter.TimestampConverter("dd/MM/yyyy", "America/Sao_Paulo", true);

		CONVERTER.addForPosition("id_servidor_portal", 0).setDescription("ID_SERVIDOR_PORTAL");
		CONVERTER.addForPosition("nome", 1).setDescription("NOME");
		CONVERTER.addForPosition("cpf", 2).setDescription("CPF");
		CONVERTER.addForPosition("matricula", 3).setDescription("MATRICULA");
		CONVERTER.addForPosition("descricao_cargo", 4).setDescription("DESCRICAO_CARGO");
		CONVERTER.addForPosition("classe_cargo", 5).setDescription("CLASSE_CARGO");
		CONVERTER.addForPosition("referencia_cargo", 6).setDescription("REFERENCIA_CARGO");
		CONVERTER.addForPosition("padrao_cargo", 7).setDescription("PADRAO_CARGO");
		CONVERTER.addForPosition("nivel_cargo", 8).setDescription("NIVEL_CARGO");
		CONVERTER.addForPosition("sigla_funcao", 9).setDescription("SIGLA_FUNCAO");
		CONVERTER.addForPosition("nivel_funcao", 10).setDescription("NIVEL_FUNCAO");
		CONVERTER.addForPosition("funcao", 11).setDescription("FUNCAO");
		CONVERTER.addForPosition("codigo_atividade", 12).setDescription("CODIGO_ATIVIDADE");
		CONVERTER.addForPosition("atividade", 13).setDescription("ATIVIDADE");
		CONVERTER.addForPosition("opcao_parcial", 14).setDescription("OPCAO_PARCIAL");
		CONVERTER.addForPosition("cod_uorg_lotacao", 15).setDescription("COD_UORG_LOTACAO");
		CONVERTER.addForPosition("uorg_lotacao", 16).setDescription("UORG_LOTACAO");
		CONVERTER.addForPosition("cod_org_lotacao", 17).setDescription("COD_ORG_LOTACAO");
		CONVERTER.addForPosition("org_lotacao", 18).setDescription("ORG_LOTACAO");
		CONVERTER.addForPosition("cod_orgsup_lotacao", 19).setDescription("COD_ORGSUP_LOTACAO");
		CONVERTER.addForPosition("orgsup_lotacao", 20).setDescription("ORGSUP_LOTACAO");
		CONVERTER.addForPosition("cod_uorg_exercicio", 21).setDescription("COD_UORG_EXERCICIO");
		CONVERTER.addForPosition("uorg_exercicio", 22).setDescription("UORG_EXERCICIO");
		CONVERTER.addForPosition("cod_org_exercicio", 23).setDescription("COD_ORG_EXERCICIO");
		CONVERTER.addForPosition("org_exercicio", 24).setDescription("ORG_EXERCICIO");
		CONVERTER.addForPosition("cod_orgsup_exercicio", 25).setDescription("COD_ORGSUP_EXERCICIO");
		CONVERTER.addForPosition("orgsup_exercicio", 26).setDescription("ORGSUP_EXERCICIO");
		CONVERTER.addForPosition("tipo_vinculo", 27).setDescription("TIPO_VINCULO");
		CONVERTER.addForPosition("situacao_vinculo", 28).setDescription("SITUACAO_VINCULO");
		CONVERTER.addForPosition("data_inicio_afastamento", 29).setDescription("DATA_INICIO_AFASTAMENTO");
		CONVERTER.addForPosition("data_termino_afastamento", 30).setDescription("DATA_TERMINO_AFASTAMENTO");
		CONVERTER.addForPosition("regime_juridico", 31).setDescription("REGIME_JURIDICO");
		CONVERTER.addForPosition("jornada_de_trabalho", 32).setDescription("JORNADA_DE_TRABALHO");
		CONVERTER.addForPosition("data_ingresso_cargofuncao", 33).setDescription("DATA_INGRESSO_CARGOFUNCAO").setConverter(dataFieldConverter);
		CONVERTER.addForPosition("data_nomeacao_cargofuncao", 34).setDescription("DATA_NOMEACAO_CARGOFUNCAO").setConverter(dataFieldConverter);
		CONVERTER.addForPosition("data_ingresso_orgao", 35).setDescription("DATA_INGRESSO_ORGAO").setConverter(dataFieldConverter);
		CONVERTER.addForPosition("documento_ingresso_servicopublico", 36).setDescription("DOCUMENTO_INGRESSO_SERVICOPUBLICO");
		CONVERTER.addForPosition("data_diploma_ingresso_servicopublico", 37).setDescription("DATA_DIPLOMA_INGRESSO_SERVICOPUBLICO").setConverter(dataFieldConverter);
		CONVERTER.addForPosition("diploma_ingresso_cargofuncao", 38).setDescription("DIPLOMA_INGRESSO_CARGOFUNCAO");
		CONVERTER.addForPosition("diploma_ingresso_orgao", 39).setDescription("DIPLOMA_INGRESSO_ORGAO");
		CONVERTER.addForPosition("diploma_ingresso_servicopublico", 40).setDescription("DIPLOMA_INGRESSO_SERVICOPUBLICO");
	}

	public static void main(String[] args) {
		PortalTransparenciaCadastroServidoresDataflow dataflow = new PortalTransparenciaCadastroServidoresDataflow();

		Options options = PipelineOptionsFactory.fromArgs(args).withValidation().as(Options.class);
		Pipeline p = dataflow.buildPipeline(options);

		p.run();
	}

	@Override
	public CSVFormat getCSVFormat() {
		return CSV_FORMAT;
	}

	@Override
	public CSVBigQueryConverter getConverter() {
		return CONVERTER;
	}

	@Override
	public String getFileRegex() {
		return "\\d{8}_Cadastro\\.csv$";
	}

	@Override
	public String getTablePrefix() {
		return "cadastro_servidores_civis";
	}

	@Override
	public String getURLFormat() {
		return URL_FMT;
	}

}

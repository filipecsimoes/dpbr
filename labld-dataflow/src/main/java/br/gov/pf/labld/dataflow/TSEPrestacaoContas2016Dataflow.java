package br.gov.pf.labld.dataflow;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.apache.commons.csv.CSVFormat;

import br.gov.pf.labld.dataflow.fn.CSVParseDoFn;
import br.gov.pf.labld.dataflow.fn.CSVRecordLine;
import br.gov.pf.labld.dataflow.fn.CSVRecordToTableRowDoFn;
import br.gov.pf.labld.dataflow.fn.FileHandler;
import br.gov.pf.labld.dataflow.fn.Line;
import br.gov.pf.labld.dataflow.fn.LineReadDoFn;
import br.gov.pf.labld.dataflow.fn.RegexFix;
import br.gov.pf.labld.dataflow.fn.RegexFixDoFn;
import br.gov.pf.labld.dataflow.fn.URLDownloadDoFn;
import br.gov.pf.labld.dataflow.fn.ZipExtractDoFn;
import br.gov.pf.labld.dataflow.specific.tse.PrestacaoContasPartitionFn;
import br.gov.pf.labld.dataflow.util.CSVBigQueryConverter;
import br.gov.pf.labld.dataflow.util.CSVBigQueryFieldConverter;
import br.gov.pf.labld.dataflow.util.CSVBigQueryFieldConverter.FloatConverter;

import com.google.api.services.bigquery.model.TableRow;
import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.coders.StringUtf8Coder;
import com.google.cloud.dataflow.sdk.io.BigQueryIO;
import com.google.cloud.dataflow.sdk.io.BigQueryIO.Write.CreateDisposition;
import com.google.cloud.dataflow.sdk.io.BigQueryIO.Write.WriteDisposition;
import com.google.cloud.dataflow.sdk.options.Default;
import com.google.cloud.dataflow.sdk.options.Description;
import com.google.cloud.dataflow.sdk.options.PipelineOptions;
import com.google.cloud.dataflow.sdk.options.PipelineOptionsFactory;
import com.google.cloud.dataflow.sdk.transforms.Create;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.transforms.Partition;
import com.google.cloud.dataflow.sdk.values.PCollection;
import com.google.cloud.dataflow.sdk.values.PCollectionList;
import com.google.cloud.storage.Acl;
import com.google.cloud.storage.Acl.Role;
import com.google.cloud.storage.Acl.User;

public class TSEPrestacaoContas2016Dataflow {

	private static final String URL_PRESTACAO_CONTAS_2016 = "http://agencia.tse.jus.br/estatistica/sead/odsele/prestacao_contas/prestacao_contas_final_2016.zip";

	private static final String[] TABLES;

	private static final String[] ELEM_NAMES = new String[] { "despesas_candidatos", "despesas_partidos", "receitas_candidatos", "receitas_partidos" };

	public static final CSVFormat CSV_FORMAT = CSVFormat.EXCEL.withSkipHeaderRecord(true).withQuote('"').withDelimiter(';').withRecordSeparator("\r\n");

	public static final CSVBigQueryConverter CONVERTER_DESPESAS_CANDIDATOS;
	public static final CSVBigQueryConverter CONVERTER_DESPESAS_PARTIDOS;
	public static final CSVBigQueryConverter CONVERTER_RECEITAS_CANDIDATOS;
	public static final CSVBigQueryConverter CONVERTER_RECEITAS_PARTIDOS;

	public static final CSVBigQueryConverter[] CONVERTERS;

	static {
		TABLES = new String[] { "tse_despesas_candidatos_2016", "tse_despesas_partidos_2016", "tse_receitas_candidatos_2016", "tse_receitas_partidos_2016" };

		CSVBigQueryFieldConverter.TimestampConverter formato1DataFieldConverter = new CSVBigQueryFieldConverter.TimestampConverter("dd/MM/yyyyhh:mm:ss", "America/Sao_Paulo");
		CSVBigQueryFieldConverter.TimestampConverter formato2DataFieldConverter = new CSVBigQueryFieldConverter.TimestampConverter("dd-MMM-yy", "America/Sao_Paulo");
		FloatConverter floatConverter = new FloatConverter(new Locale("pt", "BR"), "0.##");

		CONVERTER_DESPESAS_CANDIDATOS = new CSVBigQueryConverter();

		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("cod_eleicao", 0).setDescription("C\u00f3d. Elei\u00e7\u00e3o");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("desc_eleicao", 1).setDescription("Desc. Elei\u00e7\u00e3o");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("cnpj_prestador_conta", 3).setDescription("CNPJ Prestador Conta");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("sequencial_candidato", 4).setDescription("Sequencial Candidato");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("uf", 5).setDescription("UF");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("sigla_ue", 6).setDescription("Sigla da UE");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("nome_ue", 7).setDescription("Nome da UE");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("sigla_partido", 8).setDescription("Sigla  Partido");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("numero_candidato", 9).setDescription("N\u00famero candidato");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("cargo", 10).setDescription("Cargo");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("nome_candidato", 11).setDescription("Nome candidato");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("cpf_candidato", 12).setDescription("CPF do candidato");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("cpf_vice_suplente", 13).setDescription("CPF do vice/suplente");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("tipo_documento", 14).setDescription("Tipo de documento");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("numero_documento", 15).setDescription("N\u00famero do documento");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("cpf_cnpj_fornecedor", 16).setDescription("CPF/CNPJ do fornecedor");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("nome_fornecedor", 17).setDescription("Nome do fornecedor");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("nome_fornecedor_rfb", 18).setDescription("Nome do fornecedor (Receita Federal)");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("cod_setor_economico_fornecedor", 19).setDescription("Cod setor econ\u00f4mico do fornecedor");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("setor_economico_fornecedor", 20).setDescription("Setor econ\u00f4mico do fornecedor");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("data_despesa", 21).setDescription("Data da despesa").setConverter(formato1DataFieldConverter).setType("TIMESTAMP");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("valor_despesa", 22).setDescription("Valor despesa").setConverter(floatConverter).setType("FLOAT64");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("tipo_despesa", 23).setDescription("Tipo despesa");
		CONVERTER_DESPESAS_CANDIDATOS.addForPosition("descricao_despesa", 24).setDescription("Descri\u00e7ao da despesa");

		CONVERTER_DESPESAS_PARTIDOS = new CSVBigQueryConverter();

		CONVERTER_DESPESAS_PARTIDOS.addForPosition("cod_eleicao", 0).setDescription("C\u00f3d. Elei\u00e7\u00e3o");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("desc_eleicao", 1).setDescription("Desc. Elei\u00e7\u00e3o");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("cnpj_prestador_conta", 3).setDescription("CNPJ Prestador Conta");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("sequencial_prestador_conta", 4).setDescription("Sequencial do Prestador de conta");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("uf", 5).setDescription("UF");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("sigla_ue", 6).setDescription("Sigla da UE");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("nome_ue", 7).setDescription("Nome da UE");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("tipo_diretorio", 8).setDescription("Tipo diretorio");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("sigla_partido", 9).setDescription("Sigla  Partido");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("tipo_documento", 10).setDescription("Tipo do documento");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("numero_documento", 11).setDescription("N\u00famero do documento");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("cpf_cnpj_fornecedor", 12).setDescription("CPF/CNPJ do fornecedor");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("nome_fornecedor", 13).setDescription("Nome do fornecedor");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("nome_fornecedor_rfb", 14).setDescription("Nome do fornecedor (Receita Federal)");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("cod_setor_economico_fornecedor", 15).setDescription("Cod setor econ\u00f4mico do fornecedor");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("setor_economico_fornecedor", 16).setDescription("Setor econ\u00f4mico do fornecedor");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("data_despesa", 17).setDescription("Data da despesa").setConverter(formato2DataFieldConverter).setType("TIMESTAMP");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("valor_despesa", 18).setDescription("Valor despesa").setConverter(floatConverter).setType("FLOAT64");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("tipo_despesa", 19).setDescription("Tipo despesa");
		CONVERTER_DESPESAS_PARTIDOS.addForPosition("descricao_despesa", 20).setDescription("Descri\u00e7\u00e3o da despesa");

		CONVERTER_RECEITAS_CANDIDATOS = new CSVBigQueryConverter();

		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("cod_eleicao", 0).setDescription("C\u00f3d. Elei\u00e7\u00e3o");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("desc_eleicao", 1).setDescription("Desc. Elei\u00e7\u00e3o");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("data_hora", 2).setDescription("Data e hora");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("cnpj_prestador_conta", 3).setDescription("CNPJ Prestador Conta");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("sequencial_candidato", 4).setDescription("Sequencial Candidato");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("uf", 5).setDescription("UF");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("sigla_ue", 6).setDescription("Sigla da UE");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("nome_ue", 7).setDescription("Nome da UE");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("sigla_partido", 8).setDescription("Sigla  Partido");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("numero_candidato", 9).setDescription("Numero candidato");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("cargo", 10).setDescription("Cargo");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("nome_candidato", 11).setDescription("Nome candidato");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("cpf_candidato", 12).setDescription("CPF do candidato");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("cpf_vice_suplente", 13).setDescription("CPF do vice/suplente");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("numero_recibo_eleitoral", 14).setDescription("Numero Recibo Eleitoral");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("numero_documento", 15).setDescription("Numero do documento");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("cpf_cnpj_doador", 16).setDescription("CPF/CNPJ do doador");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("nome_doador", 17).setDescription("Nome do doador");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("nome_doador_rfb", 18).setDescription("Nome do doador (Receita Federal)");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("sigla_ue_doador", 19).setDescription("Sigla UE doador");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("numero_partido_doador", 20).setDescription("N\u00famero partido doador");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("numero_candidato_doador", 21).setDescription("N\u00famero candidato doador");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("cod_setor_economico_doador", 22).setDescription("Cod setor econ\u00f4mico do doador");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("setor_economico_doador", 23).setDescription("Setor econ\u00f4mico do doador");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("data_receita", 24).setDescription("Data da receita").setConverter(formato1DataFieldConverter).setType("TIMESTAMP");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("valor_receita", 25).setDescription("Valor receita").setConverter(floatConverter).setType("FLOAT64");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("tipo_receita", 26).setDescription("Tipo receita");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("fonte_recurso", 27).setDescription("Fonte recurso");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("especie_recurso", 28).setDescription("Especie recurso");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("descricao_receita", 29).setDescription("Descricao da receita");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("cpf_cnpj_doador_originiario", 30).setDescription("CPF/CNPJ do doador origin\u00e1rio");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("nome_doador_originario", 31).setDescription("Nome do doador origin\u00e1rio");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("tipo_doador_originario", 32).setDescription("Tipo doador origin\u00e1rio");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("setor_economico_doador_originario", 33).setDescription("Setor econ\u00f4mico do doador origin\u00e1rio");
		CONVERTER_RECEITAS_CANDIDATOS.addForPosition("nome_doador_originario_rfb", 34).setDescription("Nome do doador origin\u00e1rio (Receita Federal)");

		CONVERTER_RECEITAS_PARTIDOS = new CSVBigQueryConverter();

		CONVERTER_RECEITAS_PARTIDOS.addForPosition("cod_eleicao", 0).setDescription("C\u00f3d. Elei\u00e7\u00e3o");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("desc_eleicao", 1).setDescription("Desc. Elei\u00e7\u00e3o");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("cnpj_prestador_conta", 3).setDescription("CNPJ Prestador Conta");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("sequencial_prestador_conta", 4).setDescription("Sequencial prestador conta");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("uf", 5).setDescription("UF");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("sigla_ue", 6).setDescription("Sigla da UE");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("nome_ue", 7).setDescription("Nome da UE");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("tipo_diretorio", 8).setDescription("Tipo diretorio");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("sigla_partido", 9).setDescription("Sigla  Partido");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("numero_recibo_eleitoral", 10).setDescription("N\u00famero recibo eleitoral");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("numero_documento", 11).setDescription("N\u00famero do documento");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("cpf_cnpj_doador", 12).setDescription("CPF/CNPJ do doador");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("nome_doador", 13).setDescription("Nome do doador");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("nome_doador_rfb", 14).setDescription("Nome do doador (Receita Federal)");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("sigla_ue_doador", 15).setDescription("Sigla UE doador");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("numero_partido_doador", 16).setDescription("N\u00famero partido doador");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("numero_candidato_doador", 17).setDescription("N\u00famero candidato doador");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("cod_setor_doador", 18).setDescription("Cod setor econ\u00f4mico do doador");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("setor_economico_doador", 19).setDescription("Setor econ\u00f4mico do doador");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("data_receita", 20).setDescription("Data da receita").setConverter(formato2DataFieldConverter).setType("TIMESTAMP");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("valor_receita", 21).setDescription("Valor receita").setConverter(floatConverter).setType("FLOAT64");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("tipo_receita", 22).setDescription("Tipo receita");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("fonte_recurso", 23).setDescription("Fonte recurso");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("especie_recurso", 24).setDescription("Esp\u00e9cie recurso");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("descricao_receita", 25).setDescription("Descri\u00e7\u00e3o da receita");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("cpf_cnpj_doador_originario", 26).setDescription("CPF/CNPJ do doador origin\u00e1rio");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("nome_doador_originario", 27).setDescription("Nome do doador origin\u00e1rio");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("tipo_doador_originario", 28).setDescription("Tipo doador origin\u00e1rio");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("setor_economico_doador_originario", 29).setDescription("Setor econ\u00f4mico do doador origin\u00e1rio");
		CONVERTER_RECEITAS_PARTIDOS.addForPosition("nome_doador_originario_rfb", 30).setDescription("Nome do doador origin\u00e1rio (Receita Federal)");

		CONVERTERS = new CSVBigQueryConverter[] { CONVERTER_DESPESAS_CANDIDATOS, CONVERTER_DESPESAS_PARTIDOS, CONVERTER_RECEITAS_CANDIDATOS, CONVERTER_RECEITAS_PARTIDOS };
	}

	public static interface Options extends PipelineOptions {

		@Description("Bucket for temp data")
		@Default.String("dataflow-temp-data")
		String getBucket();

		void setBucket(String bucket);

		@Description("Folder for temp data")
		@Default.String("")
		String getFolder();

		void setFolder(String folder);

		@Description("Dataflow project")
		@Default.String("arctic-cursor-171520")
		String getProject();

		void setProject(String project);

		@Description("Bigquery dataset")
		@Default.String("tse")
		String getDataset();

		void setDataset(String project);

	}

	public static void main(String[] args) {
		Options options = PipelineOptionsFactory.fromArgs(args).withValidation().as(Options.class);
		Pipeline p = Pipeline.create(options);

		String project = options.getProject();
		String dataset = options.getDataset();
		String folder = options.getFolder();
		String bucket = options.getBucket();
		List<Acl> acls = new ArrayList<Acl>(Arrays.asList(Acl.of(User.ofAllUsers(), Role.READER)));
		FileHandler.GCSFileHandler fileHandler = new FileHandler.GCSFileHandler(bucket, acls);

		PCollection<String> urls = p.apply(Create.of(URL_PRESTACAO_CONTAS_2016).withCoder(StringUtf8Coder.of()));
		PCollection<String> zips = urls.apply(ParDo.of(new URLDownloadDoFn(folder, fileHandler)).named("DownloadZIP"));
		PCollection<String> csvs = zips.apply(ParDo.of(new ZipExtractDoFn(folder, fileHandler, ".+brasil\\.txt$")).named("UnzipCSV"));
		PCollectionList<String> csvList = csvs.apply(Partition.of(6, new PrestacaoContasPartitionFn(ELEM_NAMES)));

		for (int index = 0; index < ELEM_NAMES.length; index++) {
			String elementName = ELEM_NAMES[index];
			csvs = csvList.get(index);
			PCollection<Line> lines = csvs.apply(ParDo.of(new LineReadDoFn(fileHandler, 1, "iso-8859-1")).named("ReadCSV_" + elementName));
			RegexFix fix = new RegexFix(";\"([ \\-[0-9]\\p{L}]*?)\"([ [0-9]\\p{L}]+)\"([ \\-[0-9]\\p{L}]*?)\"", ";\"$1'$2'$3\"");
			RegexFix fix2 = new RegexFix(";\"([ \\-\\.[0-9]\\p{L}]*?)\"([ \\-[0-9]\\.\\p{L}]+)\"", ";\"$1'$2\"");
			PCollection<Line> fixedLines = lines.apply(ParDo.of(new RegexFixDoFn(fix, fix2)).named("FixCSV_" + elementName));
			PCollection<CSVRecordLine> csv = fixedLines.apply(ParDo.of(new CSVParseDoFn(CSV_FORMAT)).named("ParseCSV_" + elementName));

			CSVBigQueryConverter converter = CONVERTERS[index];
			String tableSpec = project + ":" + dataset + "." + TABLES[index];

			PCollection<TableRow> rows = csv.apply(ParDo.of(new CSVRecordToTableRowDoFn(converter)).named("ConvertBigQuery_" + elementName));
			rows.apply(BigQueryIO.Write.named("WriteBigQuery_" + elementName).to(tableSpec).withSchema(converter.getTableFieldSchema())
					.withWriteDisposition(WriteDisposition.WRITE_TRUNCATE).withCreateDisposition(CreateDisposition.CREATE_IF_NEEDED).withoutValidation());
		}

		p.run();
	}

}

package br.gov.pf.labld.dataflow.fn;

import com.google.cloud.dataflow.sdk.transforms.DoFn;

public class RegexFixDoFn extends DoFn<Line, Line> {

	private static final long serialVersionUID = 4998515786917781016L;

	private final RegexFix[] fixes;

	public RegexFixDoFn(RegexFix... fixes) {
		super();
		this.fixes = fixes;
	}

	@Override
	public void processElement(DoFn<Line, Line>.ProcessContext ctx) throws Exception {
		Line line = ctx.element();
		String content = line.getContent();
		for (RegexFix fix : fixes) {
			content = fix.fixIfMatch(content);
		}
		line = new Line(line, content);
		ctx.output(line);
	}

}

package br.gov.pf.labld.dataflow.fn;

import java.io.Serializable;

public class Line implements Serializable {

	private static final long serialVersionUID = 6307972165573445871L;

	private final int number;
	private final String file;
	private final String content;

	public Line(int number, String file, String content) {
		super();
		this.number = number;
		this.file = file;
		this.content = content;
	}

	public Line(Line oLine, String content) {
		super();
		this.number = oLine.number;
		this.file = oLine.file;
		this.content = content;
	}

	public int getNumber() {
		return number;
	}

	public String getFile() {
		return file;
	}

	public String getContent() {
		return content;
	}

}

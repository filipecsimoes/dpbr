package br.gov.pf.labld.dataflow.util;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public interface CSVBigQueryFieldConverter extends Serializable {

	Object convert(String str);

	String getDefaultType();

	public static CSVBigQueryFieldConverter STRING_CONVERTER = new CSVBigQueryFieldConverter() {

		private static final long serialVersionUID = -4238371187746820561L;

		@Override
		public Object convert(String str) {
			if (str != null && !str.trim().isEmpty()) {
				return str.trim();
			} else {
				return null;
			}
		}

		@Override
		public String getDefaultType() {
			return "STRING";
		}

	};

	public static class LowerCaseStringConverter implements CSVBigQueryFieldConverter {

		private static final long serialVersionUID = -593817516159260124L;

		@Override
		public Object convert(String str) {
			if (str != null && !str.trim().isEmpty()) {
				return str.trim().toLowerCase();
			} else {
				return null;
			}
		}

		@Override
		public String getDefaultType() {
			return "STRING";
		}

	}

	public static class FloatConverter implements CSVBigQueryFieldConverter {

		private static final long serialVersionUID = -4238371187746820561L;

		private NumberFormat fmt;

		private final Locale locale;
		private final String format;

		public FloatConverter(Locale locale, String format) {
			super();
			this.locale = locale;
			this.format = format;
		}

		@Override
		public synchronized Object convert(String str) {
			if (str != null && !str.trim().isEmpty()) {
				try {
					Number num = getFmt().parse(str);
					return num.floatValue();
				} catch (ParseException e) {
					throw new RuntimeException(e);
				}
			} else {
				return null;
			}
		}

		@Override
		public String getDefaultType() {
			return "FLOAT64";
		}

		private NumberFormat getFmt() {
			if (fmt == null) {
				DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);
				fmt = new DecimalFormat(format, symbols);
			}
			return fmt;
		}
		
	};

	public static class ArrayConverter implements CSVBigQueryFieldConverter {

		private static final long serialVersionUID = 6315223079302107700L;

		private final String splitPatternStr;
		private Pattern splitPattern;

		public ArrayConverter(String splitPattern) {
			super();
			this.splitPatternStr = splitPattern;
		}

		@Override
		public Object convert(String str) {
			if (str != null && !str.isEmpty()) {
				return getSplitPattern().split(str);
			}
			return null;
		}

		@Override
		public String getDefaultType() {
			return "REPEATED";
		}

		private Pattern getSplitPattern() {
			if (splitPattern == null) {
				splitPattern = Pattern.compile(splitPatternStr);
			}
			return splitPattern;
		}

	}

	public static class TimestampConverter implements CSVBigQueryFieldConverter {

		private static final long serialVersionUID = -2465267261201277479L;

		private static final Locale DEFAULT_LOCALE = new Locale("en", "US");

		private static final Logger LOGGER = Logger.getLogger(TimestampConverter.class.getSimpleName());

		private final String inputFormat;
		private final String zoneId;
		private transient SimpleDateFormat inputFormatter;
		private transient SimpleDateFormat outputFormatter;

		private final boolean lenient;

		public TimestampConverter(String inputFormat, String zoneId) {
			super();
			this.inputFormat = inputFormat;
			this.zoneId = zoneId;
			this.lenient = false;
		}

		public TimestampConverter(String inputFormat, String zoneId, boolean lenient) {
			super();
			this.inputFormat = inputFormat;
			this.zoneId = zoneId;
			this.lenient = lenient;
		}

		@Override
		public synchronized Object convert(String str) {
			if (str != null && !str.trim().isEmpty()) {
				Date parsed;
				try {
					parsed = getInputDateTimeFormatter().parse(str.trim());
					return getOutputDateTimeFormatter().format(parsed);
				} catch (ParseException e) {
					if (lenient) {
						LOGGER.info("Error parsing " + str + " with format " + inputFormat);
						return null;
					} else {
						throw new RuntimeException(e);
					}
				}
			} else {
				return null;
			}
		}

		@Override
		public String getDefaultType() {
			return "TIMESTAMP";
		}

		private SimpleDateFormat getInputDateTimeFormatter() {
			if (inputFormatter == null) {
				inputFormatter = new SimpleDateFormat(inputFormat, DEFAULT_LOCALE);
				inputFormatter.setTimeZone(TimeZone.getTimeZone(ZoneId.of(zoneId)));
			}
			return inputFormatter;
		}

		private SimpleDateFormat getOutputDateTimeFormatter() {
			if (outputFormatter == null) {
				outputFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS z", DEFAULT_LOCALE);
			}
			return outputFormatter;
		}

	}

}

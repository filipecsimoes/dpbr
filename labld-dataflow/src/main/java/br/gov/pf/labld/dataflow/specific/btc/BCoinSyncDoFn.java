package br.gov.pf.labld.dataflow.specific.btc;

import java.net.MalformedURLException;
import java.net.URL;

import br.gov.pf.labld.dataflow.fn.JSONContentDoFn;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.cloud.dataflow.sdk.transforms.DoFn;

public class BCoinSyncDoFn extends JSONContentDoFn<Integer, Integer> {

	private static final long serialVersionUID = -1754547127946805714L;

	private String bcoinUrl;
	private Integer maxIndexInterval;

	public BCoinSyncDoFn(String bcoinUrl, Integer maxIndexInterval) {
		super(false);
		this.bcoinUrl = bcoinUrl;
		this.maxIndexInterval = maxIndexInterval;
	}

	@Override
	public void process(DoFn<Integer, Integer>.ProcessContext ctx, JsonNode jsonNode) throws Exception {
		int initialHeight = ctx.element();
		int currentHeight = jsonNode.get("chain").get("height").asInt();

		int interval = Math.min(currentHeight - initialHeight, maxIndexInterval);
		int finalHeight = initialHeight + interval;

		for (int height = initialHeight; height < finalHeight; height++) {
			ctx.output(height);
		}
	}

	@Override
	protected URL getURL(DoFn<Integer, Integer>.ProcessContext ctx) throws MalformedURLException {
		return new URL(bcoinUrl);
	}

}

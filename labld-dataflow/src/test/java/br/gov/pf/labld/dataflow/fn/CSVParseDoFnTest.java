package br.gov.pf.labld.dataflow.fn;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.testing.DataflowAssert;
import com.google.cloud.dataflow.sdk.testing.RunnableOnService;
import com.google.cloud.dataflow.sdk.testing.TestPipeline;
import com.google.cloud.dataflow.sdk.transforms.Create;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.transforms.SerializableFunction;
import com.google.cloud.dataflow.sdk.values.PCollection;

@RunWith(JUnit4.class)
public class CSVParseDoFnTest {

	private static final String RECORD = "\"21/05/2017\";\"03:44:24\";\"000095302437\";\"ANA MARIA DA SILVA FERREIRA\";\"PC DO B\";\"PARTIDO COMUNISTA DO BRASIL\";\"AC\";\"1082\";\"RODRIGUES ALVES\";\"4\";\"169\";\"20/07/2003\";\"SUB JUDICE\";\"OFICIAL\";\"18/04/2017\";\"\";\"\";\"\";\"\"";

	@Test
	@Category(RunnableOnService.class)
	public void testParseCSV() throws Exception {
		CSVFormat format = CSVFormat.EXCEL.withSkipHeaderRecord(true).withQuote('"').withDelimiter(';');

		Pipeline p = TestPipeline.create();

		Line line = new Line(0, "some", RECORD);
		PCollection<Line> input = p.apply(Create.of(line));
		PCollection<CSVRecordLine> output = input.apply(ParDo.of(new CSVParseDoFn(format)));

		DataflowAssert.that(output).satisfies(new AssertFunction());
		p.run();
	}

	private static class AssertFunction implements SerializableFunction<Iterable<CSVRecordLine>, Void> {

		private static final long serialVersionUID = -5160350311000486422L;

		@Override
		public Void apply(Iterable<CSVRecordLine> lines) {
			final int max = 1;
			int count = 0;
			for (CSVRecordLine line : lines) {
				count++;
				CSVRecord record = line.getContent();
				assertTrue(count <= max);
				String data = record.get(0);
				assertEquals("21/05/2017", data);
			}
			assertTrue(count == 1);
			return null;
		}
	};
}
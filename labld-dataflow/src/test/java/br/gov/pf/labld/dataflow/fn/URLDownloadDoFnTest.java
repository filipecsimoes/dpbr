package br.gov.pf.labld.dataflow.fn;

import static org.junit.Assert.assertTrue;

import java.net.URL;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import br.gov.pf.labld.dataflow.fn.FileHandler;
import br.gov.pf.labld.dataflow.fn.URLDownloadDoFn;

import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.coders.StringUtf8Coder;
import com.google.cloud.dataflow.sdk.testing.DataflowAssert;
import com.google.cloud.dataflow.sdk.testing.RunnableOnService;
import com.google.cloud.dataflow.sdk.testing.TestPipeline;
import com.google.cloud.dataflow.sdk.transforms.Create;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.transforms.SerializableFunction;
import com.google.cloud.dataflow.sdk.values.PCollection;
import com.google.common.io.Files;

@RunWith(JUnit4.class)
public class URLDownloadDoFnTest {

	private static final String FILE_PATH = "fil_sub_jud_pc_do_b_ac.csv";

	@Test
	@Category(RunnableOnService.class)
	public void testDownloadURL() throws Exception {
		URL url = this.getClass().getClassLoader().getResource(FILE_PATH);

		FileHandler downloader = new FileHandler.LocalFSHandler(Files.createTempDir());

		Pipeline p = TestPipeline.create();
		PCollection<String> input = p.apply(Create.of(url.toString()).withCoder(StringUtf8Coder.of()));
		PCollection<String> output = input.apply(ParDo.of(new URLDownloadDoFn("test", downloader)));

		DataflowAssert.that(output).satisfies(new AssertFunction());
		p.run();
	}

	private static class AssertFunction implements SerializableFunction<Iterable<String>, Void> {

		private static final long serialVersionUID = -5160350311000486422L;

		@Override
		public Void apply(Iterable<String> paths) {
			final int max = 1;
			int count = 0;
			for (String path : paths) {
				count++;
				assertTrue(count <= max);
				assertTrue(path.contains(FILE_PATH));
			}
			assertTrue(count == 1);
			return null;
		}
	};
}
package br.gov.pf.labld.dataflow.fn;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.apache.commons.csv.CSVFormat;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import br.gov.pf.labld.dataflow.util.CSVBigQueryConverter;
import br.gov.pf.labld.dataflow.util.CSVBigQueryField;
import br.gov.pf.labld.dataflow.util.CSVBigQueryFieldConverter;

import com.google.api.services.bigquery.model.TableRow;
import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.testing.DataflowAssert;
import com.google.cloud.dataflow.sdk.testing.RunnableOnService;
import com.google.cloud.dataflow.sdk.testing.TestPipeline;
import com.google.cloud.dataflow.sdk.transforms.Create;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.transforms.SerializableFunction;
import com.google.cloud.dataflow.sdk.values.PCollection;

@RunWith(JUnit4.class)
public class CSVRecordToTableRowDoFnTest {

	private static final String RECORD = "\"21/05/2017\";\"03:44:24\";\"000095302437\";\"ANA MARIA DA SILVA FERREIRA\";\"PC DO B\";\"PARTIDO COMUNISTA DO BRASIL\";\"AC\";\"1082\";\"RODRIGUES ALVES\";\"4\";\"169\";\"20/07/2003\";\"SUB JUDICE\";\"OFICIAL\";\"18/04/2017\";\"\";\"\";\"\";\"\"";

	@Test
	@Category(RunnableOnService.class)
	public void testCSVRecordToTableRow() throws Exception {
		CSVBigQueryConverter converter = new CSVBigQueryConverter();

		CSVBigQueryField nomeField = new CSVBigQueryField().setConverter(CSVBigQueryFieldConverter.STRING_CONVERTER).setName("nome").setType("STRING");
		converter.addForPosition(nomeField, 3);

		CSVBigQueryFieldConverter.TimestampConverter fieldConverter = new CSVBigQueryFieldConverter.TimestampConverter("dd/MM/yyyy", "America/Sao_Paulo");
		CSVBigQueryField dataField = new CSVBigQueryField().setConverter(fieldConverter).setName("data").setType("TIMESTAMP");
		converter.addForPosition(dataField, 0);

		CSVFormat format = CSVFormat.EXCEL.withSkipHeaderRecord(true).withQuote('"').withDelimiter(';');
		Pipeline p = TestPipeline.create();

		Line line = new Line(0, "some", RECORD);
		PCollection<Line> lineCollection = p.apply(Create.of(line));
		PCollection<CSVRecordLine> csv = lineCollection.apply(ParDo.of(new CSVParseDoFn(format)));
		PCollection<TableRow> row = csv.apply(ParDo.of(new CSVRecordToTableRowDoFn(converter)));

		DataflowAssert.that(row).satisfies(new AssertFunction());
		p.run();

	}

	private static class AssertFunction implements SerializableFunction<Iterable<TableRow>, Void> {

		private static final long serialVersionUID = -5160350311000486422L;

		@Override
		public Void apply(Iterable<TableRow> rows) {
			final int max = 1;
			int count = 0;
			for (TableRow row : rows) {
				count++;
				assertTrue(count <= max);
				assertEquals("2017-05-21T00:00:00.000000 BRT", row.get("data"));
				assertEquals("ANA MARIA DA SILVA FERREIRA", row.get("nome"));
			}
			assertTrue(count == 1);
			return null;
		}
	};

}

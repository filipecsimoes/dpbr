package br.gov.pf.labld.dataflow.specific.versalic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.net.URL;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import br.gov.pf.labld.dataflow.VersalicDataflow;
import br.gov.pf.labld.dataflow.util.JSONUtil;

import com.google.api.services.bigquery.model.TableRow;
import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.coders.StringUtf8Coder;
import com.google.cloud.dataflow.sdk.testing.DataflowAssert;
import com.google.cloud.dataflow.sdk.testing.RunnableOnService;
import com.google.cloud.dataflow.sdk.testing.TestPipeline;
import com.google.cloud.dataflow.sdk.transforms.Create;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.transforms.SerializableFunction;
import com.google.cloud.dataflow.sdk.values.PCollection;

@RunWith(JUnit4.class)
public class VersalicContentDoFnTest {

	@Test
	@Category(RunnableOnService.class)
	public void testParseProjects() throws Exception {
		String url = "http://api.salic.cultura.gov.br/v1/projetos/?limit=100&format=json";

		Pipeline p = TestPipeline.create();
		PCollection<String> input = p.apply(Create.of(url).withCoder(StringUtf8Coder.of()));
		PCollection<TableRow> output = input.apply(ParDo.of(new VersalicContentDoFn("projetos", JSONUtil.schemaAsMap(VersalicDataflow.PROJETOS_SCHEMA))));
		DataflowAssert.that(output).satisfies(new AssertFunction());
		p.run();
	}

	private static class AssertFunction implements SerializableFunction<Iterable<TableRow>, Void> {

		private static final long serialVersionUID = -5073419863889754081L;

		@Override
		public Void apply(Iterable<TableRow> rows) {
			final int max = 100;
			int count = 0;
			for (TableRow row : rows) {
				count++;
				assertTrue(count <= max);
				assertTrue(row.get("PRONAC") != null);
//				assertEquals("177138", row.get("PRONAC"));
			}
			return null;
		}

	}

}

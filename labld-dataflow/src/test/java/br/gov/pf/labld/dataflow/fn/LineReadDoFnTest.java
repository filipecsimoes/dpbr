package br.gov.pf.labld.dataflow.fn;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.nio.file.Paths;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.coders.StringUtf8Coder;
import com.google.cloud.dataflow.sdk.testing.DataflowAssert;
import com.google.cloud.dataflow.sdk.testing.RunnableOnService;
import com.google.cloud.dataflow.sdk.testing.TestPipeline;
import com.google.cloud.dataflow.sdk.transforms.Create;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.transforms.SerializableFunction;
import com.google.cloud.dataflow.sdk.values.PCollection;

@RunWith(JUnit4.class)
public class LineReadDoFnTest {

	private static final String FILE_PATH = "fil_sub_jud_pc_do_b_ac.csv";

	private static final String FILE_PATH_BIG = "filiados_dem_ac.csv";

	@Test
	@Category(RunnableOnService.class)
	public void testLineRead() throws Exception {
		File root = Paths.get(this.getClass().getClassLoader().getResource(FILE_PATH).toURI()).getParent().toFile();

		FileHandler downloader = new FileHandler.LocalFSHandler(root);

		Pipeline p = TestPipeline.create();
		PCollection<String> input = p.apply(Create.of(FILE_PATH).withCoder(StringUtf8Coder.of()));
		PCollection<Line> output = input.apply(ParDo.of(new LineReadDoFn(downloader, 1)));

		DataflowAssert.that(output).satisfies(new AssertFunction());
		p.run();
	}

	private static class AssertFunction implements SerializableFunction<Iterable<Line>, Void> {

		private static final long serialVersionUID = -5160350311000486422L;

		@Override
		public Void apply(Iterable<Line> lines) {
			final int max = 1;
			int count = 0;
			for (Line line : lines) {
				count++;
				assertTrue(count <= max);
				assertTrue(line.getContent().contains("ANA MARIA DA SILVA FERREIRA"));
				assertTrue(line.getFile().equals(FILE_PATH));
			}
			assertTrue(count == 1);
			return null;
		}
	};

}

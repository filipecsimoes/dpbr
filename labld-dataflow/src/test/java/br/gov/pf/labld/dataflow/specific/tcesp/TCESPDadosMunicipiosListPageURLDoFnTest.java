package br.gov.pf.labld.dataflow.specific.tcesp;

import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.coders.StringUtf8Coder;
import com.google.cloud.dataflow.sdk.testing.DataflowAssert;
import com.google.cloud.dataflow.sdk.testing.RunnableOnService;
import com.google.cloud.dataflow.sdk.testing.TestPipeline;
import com.google.cloud.dataflow.sdk.transforms.Create;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.transforms.SerializableFunction;
import com.google.cloud.dataflow.sdk.values.PCollection;

@RunWith(JUnit4.class)
public class TCESPDadosMunicipiosListPageURLDoFnTest {

	private static final String FILE_PATH = "portal_transparencia_municipal.html";

	@Test
	@Category(RunnableOnService.class)
	public void testDownloadURL() throws Exception {
		URL url = this.getClass().getClassLoader().getResource(FILE_PATH);

		Pipeline p = TestPipeline.create();
		PCollection<String> input = p.apply(Create.of(url.toString()).withCoder(StringUtf8Coder.of()));
		PCollection<String> output = input.apply(ParDo.of(new TCESPDespesasMunicipiosListPageURLDoFn("2017")));

		DataflowAssert.that(output).satisfies(new AssertFunction());
		p.run();
	}

	private static class AssertFunction implements SerializableFunction<Iterable<String>, Void> {

		private static final long serialVersionUID = -5160350311000486422L;

		private Pattern pattern = Pattern.compile("http:\\/\\/transparencia\\.tce\\.sp\\.gov\\.br\\/municipios-csv\\?ano_exercicio=[A-Za-z0-9]+&page=\\d+");

		@Override
		public Void apply(Iterable<String> urls) {
			final int max = 32;
			int count = 0;
			for (String url : urls) {
				count++;
				assertTrue(count <= max);
				Matcher matcher = pattern.matcher(url);
				assertTrue(matcher.matches());
			}
			return null;
		}
	};

}

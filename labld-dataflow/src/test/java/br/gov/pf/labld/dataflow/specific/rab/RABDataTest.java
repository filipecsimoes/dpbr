package br.gov.pf.labld.dataflow.specific.rab;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.nio.file.Paths;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import br.gov.pf.labld.dataflow.util.CSVBigQueryFieldConverter.TimestampConverter;

@RunWith(JUnit4.class)
public class RABDataTest {

	@Test
	public void testExtract() throws Exception {
		File file = Paths.get(RABDataTest.class.getClassLoader().getResource("rab.html").toURI()).normalize().toFile();
		Document doc = Jsoup.parse(file, "windows-1252");

		TimestampConverter converterSimple = new TimestampConverter("ddMMyy", "America/Sao_Paulo");
		TimestampConverter converterComplete = new TimestampConverter("dd/MM/yy", "America/Sao_Paulo");

		RABData rab = new RABData(doc, converterSimple, converterComplete);

		assertEquals("PPABV", rab.getPrefixo());
		assertEquals("EMBRAER S.A.", rab.getProprietario());
		assertEquals("07689002000189", rab.getCpfCnpjProprietario());
		assertEquals("EMBRAER S.A.", rab.getOperador());
		assertEquals("07689002000189", rab.getCpfCnpjOperador());
		assertEquals("EMBRAER", rab.getFabricante());
		assertEquals("EMB-505", rab.getModelo());
		assertEquals("50500045", rab.getNumeroSerie());
		assertEquals("E55P", rab.getTipoIcao());
		assertEquals("EPHN", rab.getTipoHabilitacaoPilotos());
		assertEquals("POUSO CONVECIONAL 2 MOTORES JATO/TURBOFAN", rab.getClasseAeronave());
		assertEquals("8150 - Kg", rab.getPesoMaximoDecolagem());
		assertEquals("15", rab.getNumeroMaximoPassageiros());
		assertEquals("IFR Noturno", rab.getTipoVooAutorizado());
		assertEquals("PRIVADA SERVICO AEREO PRIVADOS", rab.getCategoriaRegistro());
		assertEquals("19934", rab.getNumeroCertificados());
		assertEquals("Regular", rab.getSituacaoRab());
		assertTrue(rab.getDataCompraTransferencia().toString().startsWith("2015-12-09"));
		assertTrue(rab.getDataValidadeCa().toString().startsWith("2017-04-08"));
		assertTrue(rab.getDataValidadeIam().toString().startsWith("2017-03-09"));
		assertEquals("MATRICULA CANCELADA", rab.getSituacaoAeronavegabilidade());
		assertEquals("Nenhum", rab.getMotivos());

	}
}

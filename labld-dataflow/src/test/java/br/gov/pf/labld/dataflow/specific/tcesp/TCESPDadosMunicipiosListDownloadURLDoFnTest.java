package br.gov.pf.labld.dataflow.specific.tcesp;

import static org.junit.Assert.assertTrue;

import java.net.URL;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.coders.StringUtf8Coder;
import com.google.cloud.dataflow.sdk.testing.DataflowAssert;
import com.google.cloud.dataflow.sdk.testing.RunnableOnService;
import com.google.cloud.dataflow.sdk.testing.TestPipeline;
import com.google.cloud.dataflow.sdk.transforms.Create;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.transforms.SerializableFunction;
import com.google.cloud.dataflow.sdk.values.PCollection;

@RunWith(JUnit4.class)
public class TCESPDadosMunicipiosListDownloadURLDoFnTest {

	private static final String FILE_PATH = "portal_transparencia_municipal.html";

	@Test
	@Category(RunnableOnService.class)
	public void testDownloadURL() throws Exception {
		URL url = this.getClass().getClassLoader().getResource(FILE_PATH);

		Pipeline p = TestPipeline.create();
		PCollection<String> input = p.apply(Create.of(url.toString()).withCoder(StringUtf8Coder.of()));
		PCollection<String> output = input.apply(ParDo.of(new TCESPDespesasMunicipiosListDownloadURLDoFn()));

		DataflowAssert.that(output).satisfies(new AssertFunction());
		p.run();
	}

	private static class AssertFunction implements SerializableFunction<Iterable<String>, Void> {

		private static final long serialVersionUID = -5160350311000486422L;

		@Override
		public Void apply(Iterable<String> urls) {
			final int max = 20;
			int count = 0;
			for (String url : urls) {
				count++;
				assertTrue(count <= max);
				assertTrue(url.startsWith("http://transparencia.tce.sp.gov.br/sites/municipal/files/csv/despesas-"));
			}
			return null;
		}
	};

}

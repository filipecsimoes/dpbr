package br.gov.pf.labld.dataflow.fn;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.nio.file.Paths;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.coders.StringUtf8Coder;
import com.google.cloud.dataflow.sdk.testing.DataflowAssert;
import com.google.cloud.dataflow.sdk.testing.RunnableOnService;
import com.google.cloud.dataflow.sdk.testing.TestPipeline;
import com.google.cloud.dataflow.sdk.transforms.Create;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.transforms.SerializableFunction;
import com.google.cloud.dataflow.sdk.values.PCollection;

@RunWith(JUnit4.class)
public class ReadCKANPackageJsonDoFnTest {

	private static final String FILE_PATH = "filiados_partidos.json";

	@Test
	@Category(RunnableOnService.class)
	public void testReadCKANPackageJson() throws Exception {
		File root = Paths.get(this.getClass().getClassLoader().getResource(FILE_PATH).toURI()).getParent().toFile();

		FileHandler handler = new FileHandler.LocalFSHandler(root);

		Pipeline p = TestPipeline.create();
		PCollection<String> input = p.apply(Create.of(FILE_PATH).withCoder(StringUtf8Coder.of()));
		PCollection<String> output = input.apply(ParDo.of(new ReadCKANPackageJsonDoFn(handler)));

		DataflowAssert.that(output).satisfies(new AssertFunction());
		p.run();
	}

	private static class AssertFunction implements SerializableFunction<Iterable<String>, Void> {

		private static final long serialVersionUID = -5160350311000486422L;

		@Override
		public Void apply(Iterable<String> urls) {
			for (String url : urls) {
				assertTrue(url.startsWith("http://agencia.tse.jus.br/estatistica/sead/eleitorado/filiados/uf/"));
			}
			return null;
		}
	};
}

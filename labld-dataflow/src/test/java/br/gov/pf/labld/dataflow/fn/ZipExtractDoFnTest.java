package br.gov.pf.labld.dataflow.fn;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.coders.StringUtf8Coder;
import com.google.cloud.dataflow.sdk.testing.DataflowAssert;
import com.google.cloud.dataflow.sdk.testing.RunnableOnService;
import com.google.cloud.dataflow.sdk.testing.TestPipeline;
import com.google.cloud.dataflow.sdk.transforms.Create;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.transforms.SerializableFunction;
import com.google.cloud.dataflow.sdk.values.PCollection;

public class ZipExtractDoFnTest {

	private static final String FILE_PATH = "filiados_dem_ac.zip";

	@Test
	@Category(RunnableOnService.class)
	public void testZipExtract() throws Exception {
		File root = Paths.get(this.getClass().getClassLoader().getResource(FILE_PATH).toURI()).getParent().toFile();

		FileHandler downloader = new FileHandler.LocalFSHandler(root);

		Pipeline p = TestPipeline.create();
		PCollection<String> input = p.apply(Create.of(FILE_PATH).withCoder(StringUtf8Coder.of()));
		PCollection<String> output = input.apply(ParDo.of(new ZipExtractDoFn("test", downloader, ".+\\.csv$")));

		DataflowAssert.that(output).satisfies(new AssertFunction());
		p.run();
	}

	private static class AssertFunction implements SerializableFunction<Iterable<String>, Void> {

		private static final long serialVersionUID = -5160350311000486422L;

		@Override
		public Void apply(Iterable<String> paths) {
			final int max = 2;
			int count = 0;
			for (String path : paths) {
				count++;
				assertTrue(count <= max);
				assertTrue(path.contains(".csv"));
				try {
					assertTrue(Paths.get(new URL(path).toURI()).toFile().exists());
				} catch (MalformedURLException | URISyntaxException e) {
					throw new RuntimeException(e);
				}
			}
			assertTrue(count == 2);
			return null;
		}
	};
}

package br.gov.pf.labld.dataflow.specific.tcesp;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class IdentificadorDespesaTipoPessoaExtensionTest {

	@Test
	public void testExtendPJ() {
		IdentificadorDespesaTipoPessoaExtension extension = new IdentificadorDespesaTipoPessoaExtension();
		assertEquals("PESSOA JUR\u00cdDICA", extension.extend("CNPJ - PESSOA JUR\u00cdDICA - 61416244000144"));
	}

	@Test
	public void testExtendPF() {
		IdentificadorDespesaTipoPessoaExtension extension = new IdentificadorDespesaTipoPessoaExtension();
		assertEquals("PESSOA F\u00cdSICA", extension.extend("PESSOA F\u00cdSICA - 282158"));
	}

	@Test
	public void testExtendEspecial() {
		IdentificadorDespesaTipoPessoaExtension extension = new IdentificadorDespesaTipoPessoaExtension();
		assertEquals("IDENTIFICA\u00c7\u00c3O ESPECIAL", extension.extend("IDENTIFICA\u00c7\u00c3O ESPECIAL - SEM CPF/CNPJ - 17"));
	}

}

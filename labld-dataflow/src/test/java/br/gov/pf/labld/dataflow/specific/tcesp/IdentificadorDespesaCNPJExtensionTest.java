package br.gov.pf.labld.dataflow.specific.tcesp;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class IdentificadorDespesaCNPJExtensionTest {

	@Test
	public void testExtend() {
		IdentificadorDespesaCNPJExtension extension = new IdentificadorDespesaCNPJExtension();
		assertEquals("61416244000144", extension.extend("CNPJ - PESSOA JURÍDICA - 61416244000144"));
	}

}
